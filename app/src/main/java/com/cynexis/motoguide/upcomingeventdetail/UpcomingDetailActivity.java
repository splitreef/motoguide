package com.cynexis.motoguide.upcomingeventdetail;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.MediaAdapter;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.RaceEventDetailActivity;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.eventpracticedetailresponse.EquipmentType;
import com.cynexis.motoguide.network.response.eventpracticedetailresponse.EventDetailResponse;
import com.cynexis.motoguide.network.response.eventpracticedetailresponse.EventPracticeDetialData;
import com.cynexis.motoguide.network.response.eventpracticedetailresponse.UpcomingEvent;
import com.facebook.drawee.view.SimpleDraweeView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class UpcomingDetailActivity extends AppCompatActivity implements WebserviceStartEndEventTracker {

    MainApplication _mainApplication;
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_coverpic)
    ImageView ivCoverpic;
    @BindView(R.id.iv_logo)
    CircleImageView ivLogo;
    @BindView(R.id.loading_spinner)
    ProgressBar loadingSpinner;
    @BindView(R.id.tvcalendardate)
    TextView tvcalendardate;
    @BindView(R.id.tvenddate)
    TextView tvenddate;
    @BindView(R.id.llcalendar)
    LinearLayout llcalendar;
    @BindView(R.id.ivaddcalendar)
    ImageView ivaddcalendar;
    @BindView(R.id.lladdcalendar)
    LinearLayout lladdcalendar;
    @BindView(R.id.tveventname)
    TextView tveventname;
    @BindView(R.id.tveventfacility)
    TextView tveventfacility;
    @BindView(R.id.tveventaddres)
    TextView tveventaddres;
    @BindView(R.id.llweb)
    LinearLayout llweb;
    @BindView(R.id.llfb)
    LinearLayout llfb;
    @BindView(R.id.llcall)
    LinearLayout llcall;
    @BindView(R.id.llmsg)
    LinearLayout llmsg;
    @BindView(R.id.llmap)
    LinearLayout llmap;
    @BindView(R.id.tvsanctiontype)
    TextView tvsanctiontype;
    @BindView(R.id.tvdiscipline)
    TextView tvdiscipline;
    @BindView(R.id.llequipmentcontainer)
    LinearLayout llequipmentcontainer;
    @BindView(R.id.tvfactorycontigencytype)
    TextView tvfactorycontigencytype;
    @BindView(R.id.tvadditionalinfo)
    TextView tvadditionalinfo;
    TextView tvequipmentname;
    ImageView ivequipmenttype;
    Bundle _extra;
    Intent _intent;
    View _view;
    LayoutInflater inflater;
    String _latitude = "0.0", _longitude = "0.0", type = "";

    List<EventPracticeDetialData> _detailitem;
    List<EquipmentType> _equipment;
    ArrayList<String> _discipline,_factorycontigency, _sacnction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        _extra = this.getIntent().getExtras();
        _latitude = _extra.getString("LATT");
        _longitude = _extra.getString("LONGG");
        type = _extra.getString("TYPE");
        initializeActivity();
        bindControls();
    }
    private void initializeActivity() {
        setContentView(R.layout.activity_upcoming_detail);
        ButterKnife.bind(this);
        ivmenu.setVisibility(View.INVISIBLE);
    }
    private void bindControls() {


        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("id",_mainApplication.settingManager.getSetting(TypeFactory.SettingType.UPCOMINFEVENTID)));
            _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            _dataToPost.add(new DataEntity("Type", type));
            _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.EVENT_DETAIL, _dataToPost, UpcomingDetailActivity.this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(llmap);
        }
    }

    @OnClick({R.id.ivback, R.id.ivmenu, R.id.llweb, R.id.llfb, R.id.llcall, R.id.llmsg, R.id.llmap, R.id.lladdcalendar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.llweb:
                if (_detailitem != null && _detailitem.size() > 0) {
                    _intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_detailitem.get(0).getWebsiteURL()));
                    startActivity(_intent);
                }
                break;
            case R.id.llfb:
                if (_detailitem != null && _detailitem.size() > 0) {

                    /**
                     * open facebook link on website
                     * */
                  /*  _intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_detailitem.get(0).getFacebookPageURL()));
                    startActivity(_intent);
*/

                    /**
                     * open facebook link inside facebook app
                     */

                    Uri uri = Uri.parse(_detailitem.get(0).getFacebookPageURL());
                    try {

                        ApplicationInfo applicationInfo = this.getPackageManager().getApplicationInfo("com.facebook.katana", 0);
                        if (applicationInfo.enabled) {
                            // http://stackoverflow.com/a/24547437/1048340
                            uri = Uri.parse("fb://facewebmodal/f?href=" + _detailitem.get(0).getFacebookPageURL());
                        }
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                    _intent = new Intent(Intent.ACTION_VIEW, uri);
                    //startActivity(_intent);

                    if (_intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(_intent);
                    }
                }
                break;
            case R.id.llmsg:
                if (_detailitem != null && _detailitem.size() > 0) {
                    _intent = new Intent(Intent.ACTION_SEND);
                    _intent.setType("text/plain");
                    _intent.putExtra(Intent.EXTRA_EMAIL, _detailitem.get(0).getEmail());
                    _intent.putExtra(Intent.EXTRA_SUBJECT, "MotoGuide Contact");
                    startActivity(Intent.createChooser(_intent, "Send Email"));
                }
                break;
            case R.id.llcall:
                if (_detailitem != null && _detailitem.size() > 0) {
                    _intent = new Intent(Intent.ACTION_CALL);
                    _intent.setData(Uri.parse("tel:" + _detailitem.get(0).getPhone()));//change the number
                    startActivity(_intent);
                }
                break;
            case R.id.llmap:
                if (_detailitem != null && _detailitem.size() > 0) {
                    _intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + String.valueOf(_latitude) + "," + String.valueOf(_longitude) + "&daddr=" + _detailitem.get(0).getLatitude() + "," + _detailitem.get(0).getLatitude()));
                    startActivity(_intent);
                }
                break;
            case R.id.lladdcalendar:
                if (_detailitem.get(0).getAlreadyAdded() == 1) {
                    ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                    lladdcalendar.setClickable(false);
                } else {
                    ivaddcalendar.setImageResource(R.mipmap.addevent);
                    if (_mainApplication.networkManager.CheckNetworkConnection()) {
                        showProgressDialog();
                        List<DataEntity> _dataToPost = new ArrayList<>();
                        _dataToPost.add(new DataEntity("EventID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.UPCOMINFEVENTID)));
                        _dataToPost.add(new DataEntity("UserID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
                        _dataToPost.add(new DataEntity("Type", type));
                        _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));
                        AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.ADD_CALENDAR, _dataToPost, UpcomingDetailActivity.this);
                    } else {
                        _mainApplication.messageManager.noInternetConnectionMessage(llmap);
                    }
                }

                break;
        }
    }


    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.EVENT_DETAIL)) {
                EventDetailResponse _response = (EventDetailResponse) _object[0];
                if (_response.getStatus() == 1) {

                    if (_response.getData() != null && _response.getData().size() > 0) {
                        _detailitem = _response.getData();
                        if (_detailitem.get(0).getAlreadyAdded() == 1) {
                            ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                        } else {
                            ivaddcalendar.setImageResource(R.mipmap.addevent);
                        }
                        DecimalFormat df = new DecimalFormat("###.#");
                        tvenddate.setText("to " + CommonUtility.convertDateFormat(_detailitem.get(0).getEndDate(), "yyyy-MM-dd", "MM/dd"));
                        tveventname.setText(_detailitem.get(0).getEventName());
                        tveventaddres.setText(_detailitem.get(0).getCity() + ", " + _detailitem.get(0).getState() + " - " + df.format(Double.parseDouble(_detailitem.get(0).getDistance().replaceAll("[^\\d-]", "")))+" "+_detailitem.get(0).getDistance().replaceAll("[0-9]","").replace(".","").trim());
                        tvcalendardate.setText(CommonUtility.convertDateFormat(_detailitem.get(0).getStartDate(), "yyyy-MM-dd", "MMM") + "\n" + CommonUtility.convertDateFormat(_detailitem.get(0).getStartDate(), "yyyy-MM-dd", "d"));
                        tveventfacility.setText(_detailitem.get(0).getTrackFacility());
                        if (_detailitem.get(0).getCoverphoto() != null && _detailitem.get(0).getCoverphoto().length() > 0) {

                            Picasso.with(this)
                                    .load(_detailitem.get(0).getCoverphoto())
                                    .into(ivCoverpic, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            loadingSpinner.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });
                        }

                        if (_detailitem.get(0).getLogo() != null && _detailitem.get(0).getLogo().length() > 0) {
                            //  ivLogo.setImageURI(Uri.parse(_facilitydetailitem.get(0).getLogo()));
                            Picasso.with(this)
                                    .load(_detailitem.get(0).getLogo())
                                    .into(ivLogo, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            loadingSpinner.setVisibility(View.GONE);
                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });
                        }

                        if (_detailitem.get(0).getDisciplineType() != null && _detailitem.get(0).getDisciplineType().size() > 0) {
                            _discipline = new ArrayList<>();
                            for (int i = 0; i < _detailitem.get(0).getDisciplineType().size(); i++) {
                                _discipline.add(_detailitem.get(0).getDisciplineType().get(i));
                            }
                            tvdiscipline.setText(_discipline.toString().replace("[", "").replace("]", ""));
                        } else {
                            tvdiscipline.setText("NA");
                        }

                        if(_detailitem.get(0).getType().equals("Event"))
                        {
                          if (_detailitem.get(0).getSanctionType() != null && _detailitem.get(0).getSanctionType().size() > 0) {
                        _sacnction = new ArrayList<>();
                        for (int i = 0; i < _detailitem.get(0).getSanctionType().size(); i++) {
                            _sacnction.add(_detailitem.get(0).getSanctionType().get(i));
                        }
                            tvsanctiontype.setText(_sacnction.toString().replace("[", "").replace("]", ""));
                        } else {
                            tvsanctiontype.setText("Info");
                        }
                        if (_detailitem.get(0).getFactoryContingency() != null && _detailitem.get(0).getFactoryContingency().size() > 0) {
                            _factorycontigency = new ArrayList<>();
                            for (int i = 0; i < _detailitem.get(0).getFactoryContingency().size(); i++) {
                                _factorycontigency.add(_detailitem.get(0).getFactoryContingency().get(i));
                            }
                            tvfactorycontigencytype.setText(_factorycontigency.toString().replace("[", "").replace("]", ""));
                        } else {
                            tvfactorycontigencytype.setText("NA");
                        }
                        }
                        else
                        {
                            tvsanctiontype.setVisibility(View.GONE);
                        }

                        if (_detailitem.get(0).getAdditionalInfo().length() > 0) {
                            tvadditionalinfo.setText(_detailitem.get(0).getAdditionalInfo());
                        } else {
                            tvadditionalinfo.setText("No additional info available..");
                        }


                        if(_detailitem.get(0).getEquipmentType() != null &&  _detailitem.get(0).getEquipmentType().size() > 0)
                        {
                            _equipment =_detailitem.get(0).getEquipmentType();
                            if (llequipmentcontainer != null)
                            {
                                if (llequipmentcontainer.getChildCount() > 0) {
                                    llequipmentcontainer.removeAllViews();
                                }
                                for (int i = 0; i <_equipment.size(); i++)
                                {
                                    inflater = LayoutInflater.from(UpcomingDetailActivity.this);
                                    _view = inflater.inflate(R.layout.equipment_container, null);
                                    tvequipmentname = (TextView) _view.findViewById(R.id.tvequipmentname);
                                    ivequipmenttype = (ImageView) _view.findViewById(R.id.ivequipmenttype);
                                    tvequipmentname.setText(_equipment.get(i).getEquipmentName());
                                    if (_equipment.get(i).getEquipmentImage() != null && _equipment.get(i).getEquipmentImage().length() > 0) {
                                        //loadingSpinner.setVisibility(View.VISIBLE);
                                        Picasso.with(this)
                                                .load(_equipment.get(i).getEquipmentImage())
                                                .into(ivequipmenttype, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                        //loadingSpinner.setVisibility(View.GONE);
                                                    }

                                                    @Override
                                                    public void onError() {

                                                    }
                                                });
                                        //ivLogo.setImageURI(Uri.parse(_detailitem.get(0).getLogo()));
                                    } else {
                                        //ivLogo.setImageResource(R.mipmap.logomaskarea);
                                        ivequipmenttype.setVisibility(View.GONE);
                                    }
                                    // ivequipmenttype.setImageResource(_equipment.get(i).getEquipmentImage());
                                    llequipmentcontainer.addView(_view);
                                }
                            }
                            else {
                                if (llequipmentcontainer.getChildCount() > 0) {
                                    llequipmentcontainer.removeAllViews();

                                    TextView _textView = new TextView(this);
                                    _textView.setText("No Near By Event Found..");
                                    _textView.setGravity(Gravity.CENTER);
                                    lladdcalendar.addView(_textView);
                                }

                            }
                        }


                        else
                        {
                            _mainApplication.messageManager.DisplayToastMessage("No Upcoming Event");
                        }

                        if (_detailitem.get(0).getAlreadyAdded() == 1) {
                            ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                            lladdcalendar.setClickable(false);
                        } else {
                            ivaddcalendar.setImageResource(R.mipmap.addevent);
                        }

                    } else {
                        _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                    }

                }

            } else {
                JSONObject _response = (JSONObject) _object[0];
                try {
                    if (_response.getInt("Status") == 1) {
                        lladdcalendar.setClickable(false);
                        ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                        _mainApplication.messageManager.displayCustomToastWithLayout(_response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        } else {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
