package com.cynexis.motoguide;

import android.app.Application;

import com.bugsnag.android.Bugsnag;
import com.crashlytics.android.Crashlytics;
import com.cynexis.motoguide.Utility.TypefaceUtil;
import com.cynexis.motoguide.manager.DialogManager;
import com.cynexis.motoguide.manager.MessageManager;
import com.cynexis.motoguide.manager.NetworkManager;
import com.cynexis.motoguide.manager.SettingManager;
import io.fabric.sdk.android.Fabric;
//import com.facebook.drawee.backends.pipeline.Fresco;


/**
 * Created by Admin on 28-10-2016.
 */

public class MainApplication extends Application
{
    public DialogManager dialogManager;
    public MessageManager messageManager;
    public SettingManager settingManager;
    public NetworkManager networkManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
       // Fresco.initialize(this);
        Bugsnag.init(this);
        dialogManager =  new DialogManager(this);
        messageManager =  new MessageManager(this);
        settingManager =  new SettingManager(this);
        networkManager =  new NetworkManager(this,this);
        TypefaceUtil.overrideFont(getApplicationContext(), "fonts/Myriad_Pro.ttf", "fonts/Nimbus-Sans-D-OT-Bold-Condensed_32744.ttf"); // font from assets: "assets/fonts/Roboto-Regular.ttf
    }
}
