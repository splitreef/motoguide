package com.cynexis.motoguide.dateManager;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Build;
import android.util.Log;

import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.cynexis.motoguide.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DynamicControlsForDate {

	Context _context;
	View sourceButton,destinationButton;
	private int year, month, day;
	private View timeDisplay;
    public IDateConsumer _getDateConsumer;


	public DynamicControlsForDate(Context _ctx, View _sourceButton,View _destinationButton,IDateConsumer dateConsumer)
	{
		// TODO Auto-generated constructor stub
		this._context = _ctx;
		this.sourceButton = _sourceButton;
        _getDateConsumer = dateConsumer;
        destinationButton = _destinationButton;
	}

    public void invokeDatePicker() {

            if(timeDisplay != null) // Check for  timeDisplay Refremce for null, if the reference is not null then assign it null and create new instance
            {
                timeDisplay = null; // Setting null
                timeDisplay =  sourceButton; // Setting reference
            }
            else
            {
                timeDisplay =  sourceButton; // Setting reference
            }

            // Check for button text is blank or not blank

            if(sourceButton instanceof Button)
            {
                if(!(((Button)sourceButton).getText().toString().equals("START") || ((Button)sourceButton).getText().toString().equals("END")))
                {
                    SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                    try
                    {
                        Date date = formatter.parse(((Button)sourceButton).getText().toString());

                        day =	date.getDate();
                        year = 	date.getYear()+1900; // This method is depreciated and give the current year - 1900
                        month =	date.getMonth();
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
                else
                {
                    Calendar c = Calendar.getInstance();//To initialize with the current date
                    year = c.get(Calendar.YEAR);
                    month = c.get(Calendar.MONTH);
                    day = c.get(Calendar.DAY_OF_MONTH);
                }
            }
            else
            {

                if(sourceButton instanceof TextView)
                {
                    if(!(((TextView)sourceButton).getText().toString().equalsIgnoreCase("START") || ((TextView)sourceButton).getText().toString().equalsIgnoreCase("END")))
                    {
                        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                        try {
                            Date date = formatter.parse(((TextView)sourceButton).getText().toString());

                            day =	date.getDate();
                            year = 	date.getYear()+1900; // This method is depreciated and give the current year - 1900
                            month =	date.getMonth();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Calendar c = Calendar.getInstance();//To initialize with the current date
                        year = c.get(Calendar.YEAR);
                        month = c.get(Calendar.MONTH);
                        day = c.get(Calendar.DAY_OF_MONTH);
                    }
                }

                if (sourceButton instanceof Button)
                {
                    if(!((Button)sourceButton).getText().toString().equals("MM-dd-yyyy"))
                    {
                        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                        try {
                            Date date = formatter.parse(((TextView)sourceButton).getText().toString());

                            day =	date.getDate();
                            year = 	date.getYear()+1900; // This method is depreciated and give the current year - 1900
                            month =	date.getMonth();
                        } catch (ParseException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                    else
                    {
                        Calendar c = Calendar.getInstance();//To initialize with the current date
                        year = c.get(Calendar.YEAR);
                        month = c.get(Calendar.MONTH);
                        day = c.get(Calendar.DAY_OF_MONTH);
                    }
                }


            }



            DatePickerDialog dialog= new DatePickerDialog(_context, dateListener,year, month, day);

            if(Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN || (Build.VERSION.RELEASE).equals("4.1.1") )
            {
                // Setting POSITIVE BUTTON
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_POSITIVE) {
                            // Do Stuff
                            dialog.dismiss(); // Dismiss date picker dialog;
                        }
                    }
                });

                // Setting NEGATIVE BUTTON


                //  If NEGATIVE BUTTON is pressed simply dismiss the Date Picker And dont do any thing

                dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == DialogInterface.BUTTON_NEGATIVE) {
                            // Do Stuff
                            dialog.dismiss();
                        }
                    }
                });
            }

            // If Dialog is already showing
            if(dialog.isShowing())
            {
                // Don't do any thing
            }
            else
            {
                // Show dialog
                dialog.show();
            }
        }

	
	//////////////////////////////////////////// DATE AND TIME PICKER CONSTRUCTOR ///////////////////////////////////

	private DatePickerDialog.OnDateSetListener dateListener =
			new DatePickerDialog.OnDateSetListener() {

				@Override
				public void onDateSet(DatePicker view, int yr, int monthOfYear, int dayOfMonth) {

					year = yr;
					month = monthOfYear;
					day = dayOfMonth;
					 if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN)
                	 {
                         updateDateAndTimeOnButtonText();
                	 }

				}
		};

		private void updateDateAndTimeOnButtonText() {
			String _valueOfDateTime = null;
            _valueOfDateTime = new StringBuilder().append(checkDigit(month + 1)).append('-').append(checkDigit(day)).append('-').append(year).toString();

            if (sourceButton instanceof Button)
            {
                ((Button) timeDisplay).setText(_valueOfDateTime);
                ((Button)sourceButton).setTextColor(_context.getResources().getColor(R.color.colorPrimaryDark));
                ((Button)sourceButton).setTypeface(null, Typeface.BOLD);
                if(_getDateConsumer != null)
                {

                    if(destinationButton != null || ((Button) destinationButton).getText().equals("END")) {
                        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                        String _endDate = null;
                        try {
                            Date date = formatter.parse(_valueOfDateTime);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            calendar.add(Calendar.DAY_OF_YEAR, 30);
                            Date newDate = calendar.getTime();
                            int days = newDate.getDate();
                            int years = newDate.getYear() + 1900; // This method is depreciated and give the current year - 1900
                            int months = newDate.getMonth();

                            _endDate = new StringBuilder().append(checkDigit(months + 1)).append('-').append(checkDigit(days)).append('-').append(years).toString();
                            //Log.i("Anish New Date", _endDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        _getDateConsumer.getDate(_valueOfDateTime, _endDate);
                    }
                    else
                    {
                        _getDateConsumer.getDate(_valueOfDateTime,null);
                    }
                }
                else
                {
                    _getDateConsumer.getDate(_valueOfDateTime,null);
                }
            }
            else
            {
                ((TextView) timeDisplay).setText(_valueOfDateTime);
                ((TextView)sourceButton).setTextColor(_context.getResources().getColor(R.color.colorPrimaryDark));
                ((TextView)sourceButton).setTypeface(null, Typeface.BOLD);

                if(destinationButton != null)

                {

                    if(((TextView) destinationButton).getText().toString().trim().equals("END"))
                    {
                        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
                        String _endDate = null;
                        try {
                            Date date = formatter.parse(_valueOfDateTime);
                            Calendar calendar = Calendar.getInstance();
                            calendar.setTime(date);
                            calendar.add(Calendar.DAY_OF_YEAR, 30);
                            Date newDate = calendar.getTime();
                            int days = newDate.getDate();
                            int years = newDate.getYear() + 1900; // This method is depreciated and give the current year - 1900
                            int months = newDate.getMonth();

                            _endDate = new StringBuilder().append(checkDigit(months + 1)).append('-').append(checkDigit(days)).append('-').append(years).toString();
                            //Log.i("Anish New Date", _endDate);
                            ((TextView)destinationButton).setTextColor(_context.getResources().getColor(R.color.colorPrimaryDark));
                            ((TextView)destinationButton).setTypeface(null, Typeface.BOLD);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                        _getDateConsumer.getDate(_valueOfDateTime, _endDate);
                    }
                    else
                    {
                        _getDateConsumer.getDate(_valueOfDateTime,null);
                    }
                }
                else
                {
                    _getDateConsumer.getDate(_valueOfDateTime,null);
                }
            }
		}


		/*
		 *  checkDigit is used for date and month for adding 0 for single digit
		 * 
		 */
		public String checkDigit(int number)
		{
			return number <= 9 ? "0" + number : String.valueOf(number);
		}
}