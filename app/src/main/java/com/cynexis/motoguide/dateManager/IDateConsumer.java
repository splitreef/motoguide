package com.cynexis.motoguide.dateManager;

/**
 * Created by Anish on 30/05/17.
 */

public interface IDateConsumer {

    public void getDate(String startDate, String endDate);
}
