package com.cynexis.motoguide.Utility;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.cynexis.motoguide.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anish on 13/06/17.
 */

public class CommonSpinnerAdapter extends ArrayAdapter<String> {

    LayoutInflater flater;
    List<String> _dataItem;

    public CommonSpinnerAdapter(Activity context, int textviewId, List<String> list){
        super(context,textviewId, list);
        flater = context.getLayoutInflater();
        _dataItem = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowview = flater.inflate(R.layout.common_spinner_adapter,null,true);

        TextView txtTitle = (TextView) rowview.findViewById(R.id.tvspinnerdata);
        txtTitle.setText(_dataItem.get(position));

        return rowview;
    }
}
