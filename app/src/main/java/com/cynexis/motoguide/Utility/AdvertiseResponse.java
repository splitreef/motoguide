package com.cynexis.motoguide.Utility;

import android.bluetooth.le.AdvertiseData;
import android.content.Intent;

import com.cynexis.motoguide.network.response.searchresponse.SearchData;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by  on 10-01-2020.
 */
public class AdvertiseResponse implements Serializable
{
   /* {
        "Status": 1,
            "message": "Data found",
            "url": "http://work.splitreef.com/client/development/motoguide/uploads/advertisements/1081118681-396X192.jpg"
    }*/
    @SerializedName("Status")
    @Expose
    private Integer status;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("data")
    @Expose
    private List<AdvertisementData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AdvertisementData> getData() {
        return data;
    }

    public void setData(List<AdvertisementData> data) {
        this.data = data;
    }
}
