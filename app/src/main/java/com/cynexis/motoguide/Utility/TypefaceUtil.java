package com.cynexis.motoguide.Utility;

import android.content.Context;
import android.graphics.Typeface;

import java.lang.reflect.Field;

//import android.util.Log;

/**
 * Created by Admin on 24-08-2016.
 */
public class TypefaceUtil {

    /**
     * Using reflection to override default typeface
     * NOTICE: DO NOT FORGET TO SET TYPEFACE FOR APP THEME AS DEFAULT TYPEFACE WHICH WILL BE OVERRIDDEN
     * @param context to work with assets
     * @param fontNameForEditText Edit text font monospace will be overrided.
     * @param customFontFileNameInAssets file name of the font from assets
     */
    public static void overrideFont(Context context, String fontNameForEditText, String customFontFileNameInAssets) {
        try
        {
            final Typeface customFontTypeface = Typeface.createFromAsset(context.getAssets(), customFontFileNameInAssets);
            final Field defaultFontTypefaceField = Typeface.class.getDeclaredField("SANS_SERIF");
            defaultFontTypefaceField.setAccessible(true);
            defaultFontTypefaceField.set(null, customFontTypeface);


            final Typeface customFontTypefaceForEditText = Typeface.createFromAsset(context.getAssets(), fontNameForEditText);
            final Field defaultFontTypefaceFieldForEditText = Typeface.class.getDeclaredField("MONOSPACE");
            defaultFontTypefaceFieldForEditText.setAccessible(true);
            defaultFontTypefaceFieldForEditText.set(null, customFontTypefaceForEditText);

            //Log.i("Anish ","Set");

        } catch (Exception e) {
            System.out.print(e.toString());
            //Log.i("Anish",e.toString());
          //  Log.i("Can not set custom font ",  customFontFileNameInAssets + " instead of + defaultFontNameToOverride);
        }
    }
}
