package com.cynexis.motoguide.Utility;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by  on 14-01-2020.
 */
public class AdvertisementData implements Serializable {
    @SerializedName("url")
    @Expose
    private String url;

    @SerializedName("website_url")
    @Expose
    private String website_url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebsite_url() {
        return website_url;
    }

    public void setWebsite_url(String website_url) {
        this.website_url = website_url;
    }
}
