/*
 * Used as class for common utility functions
 */

package com.cynexis.motoguide.Utility;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AlertDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * @author Anish
 *
 */

/**
 * @author Anish
 *
 */
@SuppressLint({ "SimpleDateFormat", "DefaultLocale" })
public class CommonUtility {

	public static String _INTERNET_CONNECTION = "Internet connection is too slow or may be not working.";
	public static String IMAGE_DIRECTORY_NAME = "PEOPLEPEDIA";
	public static String ERROR_OCCURED = "Fail to get response, please try again.";







	// shows alert message
	public static void showAlert(final Activity context, final String title,
                                 final String message) {
        AlertDialog.Builder alertDialogBuilder  = new AlertDialog.Builder(context);
        //alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setTitle("MotoGuide");
        alertDialogBuilder.setMessage(message);
		alertDialogBuilder.setCancelable(false);
        // set dialog message
        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	public static void showAlert(final Context context, final String title,
                                 final String message) {
		AlertDialog.Builder alertDialogBuilder  = new AlertDialog.Builder(context);
		//alertDialogBuilder.setTitle(title);
		alertDialogBuilder.setTitle("MotoGuide");
		alertDialogBuilder.setMessage(message);
		// set dialog message
		alertDialogBuilder.setCancelable(false);
		alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


	public static long getDifferenceDays(String startDate, String endDate) {

		//HH converts hour in 24 hours format (0-23), day calculation
				SimpleDateFormat format = new SimpleDateFormat("yy/MM/dd");

				Date _dateStart = null;
				Date _dateEnd = null;

				try {
					_dateStart = format.parse(startDate);
					_dateEnd = format.parse(endDate);
				} catch (ParseException e) {
					e.printStackTrace();
				}

				long diff = _dateEnd.getTime() - _dateStart.getTime();
				long diffDays = diff / (24 * 60 * 60 * 1000);

				System.out.print(diffDays + " days, ");
				//Log.i("Difference In Days", String.valueOf(diffDays));
			return diffDays;
	}

	public static void hideKeyboard(final Activity context) {
		View view = context.getCurrentFocus();
		if (view != null) {
			InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		}
	}


/// SHOW ALERT AND GO BACK
	public static void showAlertAndGoBack(final Activity context, final String title,
                                          final String message) {
		context.runOnUiThread(new Runnable() {

			@Override
			public void run() {
				new AlertDialog.Builder(context)
						//.setTitle(title)
						.setTitle("MotoGuide")
						.setMessage(message)
						.setCancelable(false)
						.setPositiveButton("Ok",
								new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog,
														int which) {
										context.finish();
									}
								}).show();
			}
		});
	}


	public static boolean isNetworkAvailable(Context context)
	{
		ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
		if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnectedOrConnecting())
		{
			return true;
		}
		return false;
	}

	// converts date string from its current source format to required target
	// format
	// "yyyy-MM-dd HH:mm:ss" to "MM/dd/yyyy hh:mm a"
	// "MM/dd/yy HH:mm:ss" to "MM/dd/yyyy hh:mm a"
	public static String convertDateFormat(String dateStr, String sourceFormat,
                                           String targetFormat) {

		if (dateStr.equals("")) {
			return "";
		}
		//Log.d("date", dateStr + "---" + sourceFormat + "---" + targetFormat);
		// "yyyy-MM-dd'T'HH:mm:ss.SSS"
		SimpleDateFormat form = new SimpleDateFormat(sourceFormat);
		Date date = null;
		try {
			date = form.parse(dateStr);
		} catch (Exception e) {
			e.printStackTrace();
		}
		SimpleDateFormat postFormater = new SimpleDateFormat(targetFormat);
		String newDateStr = postFormater.format(date);
		//Log.d("Date", newDateStr);
		return newDateStr;
	}

	// returns current date using format "yyyy-MM-dd HH:mm:ss"
	public static String getCurrentDateTime(){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dateFormat.format(new Date());
	}

    public static String getCurrentDateTimeUS(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd h:mm a", Locale.US);
        //Log.i("Anish",dateFormat.format(new Date()));
        return dateFormat.format(new Date());
    }

}



