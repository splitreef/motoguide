package com.cynexis.motoguide;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  on 30-12-2019.
 */
public class FacilityEventPagerAdapter extends PagerAdapter {
    Context context;
    List<String> listImages = new ArrayList<>();
    LayoutInflater layoutInflater;


    public FacilityEventPagerAdapter(Context context, List<String> listImages) {
        this.context = context;
        this.listImages = listImages;
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return listImages.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = layoutInflater.inflate(R.layout.item_image_pager_, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);
        //imageView.setImageResource(images[position]);

       /* if (listImages.get(position) != null && !listImages.get(position).equalsIgnoreCase("")) {
            Picasso.with(context)
                    .load(listImages.get(position))
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            Log.d("gggg","");
                           // loadingSpinner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            imageView.setImageResource(R.mipmap.dashboard_two);
        }*/

       /* Glide.with(context)
                .load(listImages.get(position))
                .into(imageView);
*/

        Picasso.with(context)
                .load(listImages.get(position))
                .into(imageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        Log.d("gggg","");
                        // loadingSpinner.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });

        container.addView(itemView);

        //listening to image click
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show();
            }
        });

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}
