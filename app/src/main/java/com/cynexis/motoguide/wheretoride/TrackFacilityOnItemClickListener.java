package com.cynexis.motoguide.wheretoride;

/**
 * Created by  on 02-01-2020.
 */
public interface TrackFacilityOnItemClickListener {
    public  void trackFacilityItemClick(int position);
}
