package com.cynexis.motoguide.wheretoride;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.network.response.wheretorideresponse.WheretoRideData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 02-06-2017.
 */

public class TrackFacilityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context _ctx;
    List<WheretoRideData> _tracklist;
    private static LayoutInflater inflaterone = null;
    private TrackFacilityOnItemClickListener trackFacilityOnItemClickListener;
    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    private boolean showLoader;
    
    public TrackFacilityAdapter(Context _ctx, List<WheretoRideData> _tracklist, TrackFacilityOnItemClickListener trackFacilityOnItemClickListener) {
        this._ctx = _ctx;
        this._tracklist = _tracklist;
        inflaterone = (LayoutInflater) _ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.trackFacilityOnItemClickListener = trackFacilityOnItemClickListener;
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_track, parent, false);
                return new TrackFacilityAdapter.ViewHolderOne(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
                return new TrackFacilityAdapter.FooterLoader(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == _tracklist.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }



   @Override
   public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
       if (viewHolder instanceof TrackFacilityAdapter.FooterLoader) {
           TrackFacilityAdapter.FooterLoader loaderViewHolder = (TrackFacilityAdapter.FooterLoader) viewHolder;
           if (showLoader) {
               loaderViewHolder.loadmore_progress.setVisibility(View.VISIBLE);
           } else {
               loaderViewHolder.loadmore_progress.setVisibility(View.GONE);
           }
           return;
       }
       TrackFacilityAdapter.ViewHolderOne holder = ((TrackFacilityAdapter.ViewHolderOne) viewHolder);

       // DecimalFormat df = new DecimalFormat("###.#");
        //holder.tvadress.setText(_tracklist.get(position).getCity()+", "+_tracklist.get(position).getState()+" - "+df.format(Double.parseDouble(_tracklist.get(position).getDistance().replaceAll("[^\\d-]", "")))+" "+_tracklist.get(position).getDistance().replaceAll("[0-9]","").replace(".","").trim());
        holder.tvadress.setText(_tracklist.get(position).getCity() + ", " + _tracklist.get(position).getState() + " - " + _tracklist.get(position).getDistance());
        holder.tveventname.setText(_tracklist.get(position).getFacilityName());
        if (position == 0) {
            holder.rlroot.setBackgroundResource(R.color.white);
        }
        if (position == 1) {
            holder.rlroot.setBackgroundResource(R.color.listcolor);
        }

        if (position > 1) {
            if (position % 2 == 0) {
                holder.rlroot.setBackgroundResource(R.color.white);
            } else {
                holder.rlroot.setBackgroundResource(R.color.listcolor);
            }
        }
        holder.rlroot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                trackFacilityOnItemClickListener.trackFacilityItemClick(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return _tracklist.size();
    }


    static class ViewHolderOne extends RecyclerView.ViewHolder {
        @BindView(R.id.tvadress)
        public TextView tvadress;
        @BindView(R.id.tveventname)
        public TextView tveventname;
        @BindView(R.id.rlroot)
        public RelativeLayout rlroot;

        public ViewHolderOne(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected class FooterLoader extends RecyclerView.ViewHolder {
        @BindView(R.id.loadmore_progress)
        public ProgressBar loadmore_progress;

        public FooterLoader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}


