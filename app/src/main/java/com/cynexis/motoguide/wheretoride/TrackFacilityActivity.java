package com.cynexis.motoguide.wheretoride;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.DashBoardActivity;
import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.FacilityEventDetailActivity;
import com.cynexis.motoguide.Utility.CommonSpinnerAdapter;
import com.cynexis.motoguide.filters.FilterTrackFacilityActivity;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.filters.properties.FilterSerializableForTrackFacilityClass;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.mycalendar.MyCalendarActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.wheretorideresponse.WheretoRideData;
import com.cynexis.motoguide.network.response.wheretorideresponse.WheretoRideResponse;
import com.cynexis.motoguide.searchallraces.SearchActivity;
import com.cynexis.motoguide.startingactivities.permission.GetLatitudeAndLongitude;
import com.cynexis.motoguide.startingactivities.permission.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class TrackFacilityActivity extends FragmentActivity implements OnMapReadyCallback, GetLatitudeAndLongitude,
        WebserviceStartEndEventTracker, GoogleMap.OnMarkerClickListener, TrackFacilityOnItemClickListener {

    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.spinnermiles)
    AppCompatSpinner spinnermiles;
    @BindView(R.id.btnmorefilter)
    Button btnmorefilter;

    /* @BindView(android.R.id.list)
     ListView list;*/
    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    private LinearLayoutManager linearLayoutManager;

    @BindView(R.id.ivscrollupscrolldown)
    ImageView ivscrollupscrolldown;
    @BindView(R.id.dragView)
    RelativeLayout dragView;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.activity_track_facility)
    RelativeLayout activityTrackFacility;
    @BindView(R.id.bottomnavigation)
    BottomNavigationView bottomnavigation;
    FilterSerializableForTrackFacilityClass _arrayOfTrackFaciltiy;


    private GoogleMap mMap;
    private TrackFacilityAdapter trackFacilityAdapter;
    Intent _intent;
    SlideMenueImplimentationClass _slidingMenu;
    LocationService _locationService;
    public static final int PERMISSION_REQUEST_CODE = 120;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    String _latitude = "0.0", _longitude = "0.0";
    MainApplication _mainApplication;
    List<WheretoRideData> _rideitem = new ArrayList<>();
    SupportMapFragment mapFragment;
    boolean isfirsttimeWebAPICalled = false;
    private int offset = 0;
    private Boolean isApiCalling = false;
    private int dataSize = 0;
    @BindView(R.id.tv_swipe_up_for_more_info)
    TextView tv_swipe_up_for_more_info;
    @BindView(R.id.tv_show_all)
    TextView tv_show_all;
    private Boolean isShowAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        initalizeActivity();
        checkPermissionForMarshmallow();
        bindControls();

    }

    private void initalizeActivity() {
        setContentView(R.layout.activity_track_facility);
        ButterKnife.bind(this);
        // dragView.setVisibility(View.VISIBLE);

        // Spinner array list with custom adapter...
        List<String> _arrayOfMiles = Arrays.asList(getResources().getStringArray(R.array.array_miles));
        CommonSpinnerAdapter _spinneradapter = new CommonSpinnerAdapter(this, android.R.id.text1, _arrayOfMiles);
        _spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnermiles.setAdapter(_spinneradapter);
        spinnermiles.setSelection(1);
        isfirsttimeWebAPICalled = false;

        linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_view.getContext(),
                linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.item_divider));
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView

        trackFacilityAdapter = new TrackFacilityAdapter(this, _rideitem, this);
        recycler_view.setAdapter(trackFacilityAdapter);

        slidingLayout.setScrollableView(recycler_view);

        RecyclerView.OnScrollListener mScrollListener = new
                RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                        if (!isShowAll) {
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                if (!isApiCalling) {
                                    if (_arrayOfTrackFaciltiy != null && _arrayOfTrackFaciltiy.getMiles() != null) {
                                        trackFacilityAdapter.showLoading(true);
                                        trackFacilityAdapter.notifyDataSetChanged();
                                        offset = offset + 10;
                                        callWebApiAfterFilterApply(offset);
                                    } else {
                                        trackFacilityAdapter.showLoading(true);
                                        trackFacilityAdapter.notifyDataSetChanged();
                                        offset = offset + 10;
                                        callWebAPIs(offset, false);
                                    }

                                }

                            }
                        }
                    }
                };
        recycler_view.addOnScrollListener(mScrollListener);


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @OnItemSelected(R.id.spinnermiles)
    public void spinnerItemSelected(Spinner spinner, int position) {
        //if (spinner.getId() == R.id.spinnermiles) {
        if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
            if (_arrayOfTrackFaciltiy != null) {
                _arrayOfTrackFaciltiy.setMiles(null);
            }
            _rideitem.clear();
            offset = 0;
            isShowAll = false;
            // tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
            callWebAPIs(offset, true);
        }
        //}
    }

    private void bindControls() {

        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();

        slidingLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {

            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                if (newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    ivscrollupscrolldown.setImageResource(R.mipmap.sliderdown);
                } else {
                    ivscrollupscrolldown.setImageResource(R.mipmap.sliderup);
                }
            }
        });

        bottomnavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        // Write code to perform some actions.

                        switch (item.getItemId()) {
                            case R.id.action_search:
                                // Log.i("Anish","1");
                                _intent = new Intent(TrackFacilityActivity.this, SearchActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_home:
                                //Log.i("Anish","2");
                                _intent = new Intent(TrackFacilityActivity.this, DashBoardActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_calendar:
                                //Log.i("Anish","3");
                                _intent = new Intent(TrackFacilityActivity.this, MyCalendarActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;
                        }

                        return false;
                    }
                });


        recycler_view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        _rideitem.clear();
        isShowAll = false;
        //tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
        offset = 0;
        if (intent.getExtras().getBoolean("IS_TRACK_FACILITY")) {
            _arrayOfTrackFaciltiy = (FilterSerializableForTrackFacilityClass) intent.getExtras().getSerializable("FILTER");
        }
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            if (_arrayOfTrackFaciltiy.getMiles() != null) {
                _dataToPost.add(new DataEntity("distance", _arrayOfTrackFaciltiy.getMiles().replace("Miles", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }

            if (_arrayOfTrackFaciltiy.getState() == null || _arrayOfTrackFaciltiy.getState().trim().equals("STATE")) {
                _dataToPost.add(new DataEntity("state", ""));
            } else {
                _dataToPost.add(new DataEntity("state", _arrayOfTrackFaciltiy.getState().trim()));
            }

            if (_arrayOfTrackFaciltiy.getDiscipline() != null && _arrayOfTrackFaciltiy.getDiscipline().size() > 0) {
                _dataToPost.add(new DataEntity("disciplinename", _arrayOfTrackFaciltiy.getDiscipline().toString().trim().replace("[", "").replace("]", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("disciplinename", ""));
            }
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("type", "facility"));
            _dataToPost.add(new DataEntity("show_all", "" + "No"));

            //
            if (mMap != null) {
                mMap.clear();
            }

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.TRACK_FACILITY, _dataToPost, this);
            _mainApplication.messageManager.DisplayToastMessageAtCenter("Filter have been applied");

        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(ivback);
        }
    }

    private void checkPermissionForMarshmallow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);
                }

            } else {
                if (checkGooglePlayServices()) {
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            /*
             * google play services is missing or update is required
             *  return code could be
             * SUCCESS,
             * SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
             * SERVICE_DISABLED, SERVICE_INVALID.
             */
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        if (_rideitem != null && _rideitem.size() > 0) {
            for (int i = 0; i < _rideitem.size(); i++) {
                LatLng myLocation = new LatLng(Double.valueOf(_latitude), Double.valueOf(_longitude));
                Marker _plottedMarkerObject = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(_rideitem.get(i).getLatitude()), Double.valueOf(_rideitem.get(i).getLongitude()))).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker)));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 8));
                _plottedMarkerObject.setTag(i);
                mMap.setOnMarkerClickListener(this);
            }
           /* Random rand = new Random();
            if(_rideitem.size() == 1)
            {
                LatLng myLocation = new LatLng((Double.valueOf(_rideitem.get(0).getLatitude())), Double.valueOf(_rideitem.get(0).getLongitude()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 8));
                mMap.setOnMarkerClickListener(this);
            }
            else
            {
                int  n = rand.nextInt(_rideitem.size()-1);
                //_raceEventsList.size()-1 is the maximum and the 1 is our minimum
                LatLng myLocation = new LatLng((Double.valueOf(_rideitem.get(n).getLatitude())), Double.valueOf(_rideitem.get(n).getLongitude()));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 8));
                mMap.setOnMarkerClickListener(this);

            }*/

            if (_latitude.trim().length() != 0 && !_latitude.trim().equals("0.0")) {
                LatLng myLocation = new LatLng(Double.valueOf(_latitude), Double.valueOf(_longitude));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10));
            } else {
                _mainApplication.messageManager.DisplayToastMessage("Current location not found.");
            }

        } else {
            if (_latitude.trim().length() != 0 && !_latitude.trim().equals("0.0")) {
                LatLng myLocation = new LatLng(Double.valueOf(_latitude), Double.valueOf(_longitude));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10));
            } else {
                _mainApplication.messageManager.DisplayToastMessage("Current location not found.");
            }
        }
    }


    @OnClick({R.id.ivback, R.id.ivmenu, R.id.btnmorefilter, R.id.tv_show_all})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else {
                    finish();
                }
                break;
            case R.id.ivmenu:
                _slidingMenu.ShowMenueOnActionBarClick();
                break;
            case R.id.btnmorefilter:
                if (!isApiCalling) {
                    _intent = new Intent(this, FilterTrackFacilityActivity.class);
                    _intent.putExtra("POSITION", spinnermiles.getSelectedItemPosition());
                    startActivity(_intent);
                }
                break;
            case R.id.tv_show_all:
                if (!isApiCalling) {
                    if (!isShowAll) {
                        isShowAll = true;
                        //tv_show_all.setBackgroundResource(R.drawable.bg_oval_red);
                        // tv_show_all.setAlpha(.5f);
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        _rideitem.clear();
                        apicallShowAllData(0);
                    } else {
                        isShowAll = false;
                        // tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
                        //tv_show_all.setAlpha(1f);
                        tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        offset = 0;
                        _rideitem.clear();
                        callWebAPIs(offset, true);
                    }
                }
                break;
        }
    }

    private void apicallShowAllData(int offset) {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            //isApiCalling = true;
//            if (progressFlag) {
            showProgressDialog();
//            }
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            // _dataToPost.add(new DataEntity("distance", spinnermiles.getSelectedItem().toString().replace("Miles","").trim()));
            if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                _dataToPost.add(new DataEntity("distance", spinnermiles.getSelectedItem().toString().replace("Miles", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }
            if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME) != null) {
                _dataToPost.add(new DataEntity("state", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME)));
            } else {
                _dataToPost.add(new DataEntity("state", ""));
            }
            _dataToPost.add(new DataEntity("disciplinename", ""));
            _dataToPost.add(new DataEntity("type", "facility"));

            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("show_all", "Yes"));


            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.TRACK_FACILITY, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(recycler_view);
        }
    }

    @Override
    public void getLatLong(String locationBY, String latitude, String longitude) {
        _latitude = latitude;
        _longitude = longitude;
        // Log.i("tanvi", _latitude + " " + _longitude);
        isfirsttimeWebAPICalled = true;
        _rideitem.clear();
        offset = 0;
        isShowAll = false;
        //tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
        callWebAPIs(offset, true);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted) {
                        Snackbar.make(recycler_view, "Permission Granted, Thanks.", Snackbar.LENGTH_LONG).show();
                        if (_locationService == null) {
                            _locationService = new LocationService(this, this);
                            _locationService.makeConnectionIfRequired();
                        }
                    } else {
                        Snackbar.make(recycler_view, "Permission Denied. Please allow and try loading this screen.", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        if (!isShowAll) {
            handalResponseWithPagination(_object);
        } else {
            handalResponseShowAll(_object);
        }

    }

    private void handalResponseWithPagination(Object[] _object) {
        isApiCalling = false;
        trackFacilityAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            WheretoRideResponse _response = (WheretoRideResponse) _object[0];
            if (_response != null) {
                if (_response.getStatus() == 1) {
                   /* if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED))
                    {
                        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED); //to close
                    }*/

                    if (_response.getData() != null && _response.getData().size() > 0) {
                        dataSize = _response.getData().size();
                        if (dataSize >= 10) {
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        } else {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        }
                        _rideitem.addAll(_response.getData());
                        trackFacilityAdapter.notifyDataSetChanged();

                      /*  _rideitem = _response.getData();
                        trackFacilityAdapter = new TrackFacilityAdapter(this, _rideitem, this);
                        recycler_view.setAdapter(trackFacilityAdapter);*/

                        if (mapFragment == null) {
                            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                            mapFragment.getMapAsync(this);
                        } else {
                            mapFragment.getMapAsync(TrackFacilityActivity.this);
                        }
                    }

                } else {
                    if (offset == 0) {
                        // if (_rideitem != null && _rideitem.size() > 0) {
                        if (_rideitem != null) {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            _rideitem.clear();
                            trackFacilityAdapter = new TrackFacilityAdapter(this, _rideitem, this);
                            recycler_view.setAdapter(trackFacilityAdapter);
                            trackFacilityAdapter.notifyDataSetChanged();

                            if (mMap != null) {
                                mMap.clear();
                            }
                        }
                        _mainApplication.messageManager.DisplayToastMessageAtCenter(_response.getMessage());
                    } else {
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        trackFacilityAdapter.showLoading(false);
                        trackFacilityAdapter.notifyDataSetChanged();
                    }
                }
            } else {
                tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
            }
        } else {
            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    private void handalResponseShowAll(Object[] _object) {
        // isApiCalling = false;
        // trackFacilityAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            WheretoRideResponse _response = (WheretoRideResponse) _object[0];
            if (_response != null) {
                if (_response.getStatus() == 1) {


                    if (_response.getData() != null && _response.getData().size() > 0) {

                        _rideitem.addAll(_response.getData());
                        trackFacilityAdapter.notifyDataSetChanged();



                        if (mapFragment == null) {
                            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                            mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                            mapFragment.getMapAsync(this);
                        } else {
                            mapFragment.getMapAsync(TrackFacilityActivity.this);
                        }
                    }

                } else {
                    // if (offset == 0) {
                    // if (_rideitem != null && _rideitem.size() > 0) {
                    if (_rideitem != null) {
                        //tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        _rideitem.clear();
                        trackFacilityAdapter = new TrackFacilityAdapter(this, _rideitem, this);
                        recycler_view.setAdapter(trackFacilityAdapter);
                        trackFacilityAdapter.notifyDataSetChanged();

                        if (mMap != null) {
                            mMap.clear();
                        }
                    }
                    _mainApplication.messageManager.DisplayToastMessageAtCenter(_response.getMessage());

                }
            } else {
                //tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
            }
        } else {
            // tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        // dragView.setVisibility(View.INVISIBLE);
        // Create the Snackbar
        final Snackbar snackbar = Snackbar.make(ivback, "", Snackbar.LENGTH_INDEFINITE);
        // Get the Snackbar's layout view

        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        layout.setBackgroundResource(R.mipmap.transparent);
        layout.setPadding(0, 0, 0, 0);//set padding to 0
        // Hide the text
        //TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        TextView textView = (TextView) layout.findViewById(R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        LayoutInflater inflater = LayoutInflater.from(this);
        // Inflate our custom view
        View snackView = inflater.inflate(R.layout.snackbar_custom, null);
        // Configure the view
        ImageView ivclose = (ImageView) snackView.findViewById(R.id.ivclose);
        TextView tveventname = (TextView) snackView.findViewById(R.id.tveventname);
        TextView tv_direction = (TextView) snackView.findViewById(R.id.tv_direction);
        LinearLayout lldirection = (LinearLayout) snackView.findViewById(R.id.lldirection);
        LinearLayout lladdressraceevent = (LinearLayout) snackView.findViewById(R.id.lladdressraceevent);
        LinearLayout lladdressfacility = (LinearLayout) snackView.findViewById(R.id.lladdressfacility);
        TextView tvadresss = (TextView) snackView.findViewById(R.id.tvadresss);
        if (_rideitem != null) {
            lladdressraceevent.setVisibility(View.GONE);
            lladdressfacility.setVisibility(View.VISIBLE);
            tveventname.setText(_rideitem.get(Integer.valueOf(marker.getTag().toString())).getFacilityName());
            if (_rideitem.get(Integer.valueOf(marker.getTag().toString())).getTime().length() > 0) {
                tv_direction.setText(_rideitem.get(Integer.valueOf(marker.getTag().toString())).getTime());
            } else {
                tv_direction.setText("0h drive");
            }

            tvadresss.setText(_rideitem.get(Integer.valueOf(marker.getTag().toString())).getAddress() + " " + _rideitem.get(Integer.valueOf(marker.getTag().toString())).getCity() + "\n" +
                    " " + _rideitem.get(Integer.valueOf(marker.getTag().toString())).getState());
        }

        lldirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_rideitem != null && _rideitem.size() > 0) {
                    _intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + String.valueOf(_latitude) + "," + String.valueOf(_longitude) + "&daddr=" + _rideitem.get(Integer.valueOf(marker.getTag().toString())).getLatitude() + "," + _rideitem.get(Integer.valueOf(marker.getTag().toString())).getLongitude()));
                    startActivity(_intent);
                }
            }
        });
        tveventname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _intent = new Intent(TrackFacilityActivity.this, FacilityEventDetailActivity.class);
                _intent.putExtra("LAT", _latitude);
                _intent.putExtra("LONG", _longitude);
                _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _rideitem.get(Integer.valueOf(marker.getTag().toString())).getID());
                startActivity(_intent);
            }
        });
        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dragView.setVisibility(View.VISIBLE);
                snackbar.dismiss();
            }
        });
        // Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
        // Show the Snackbar
        snackbar.show();
        return false;
    }

    public void callWebApiAfterFilterApply(int offset) {
        // _rideitem.clear();

        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            // showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            if (_arrayOfTrackFaciltiy.getMiles() != null) {
                _dataToPost.add(new DataEntity("distance", _arrayOfTrackFaciltiy.getMiles().replace("Miles", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }

            if (_arrayOfTrackFaciltiy.getState() == null || _arrayOfTrackFaciltiy.getState().trim().equals("STATE")) {
                _dataToPost.add(new DataEntity("state", ""));
            } else {
                _dataToPost.add(new DataEntity("state", _arrayOfTrackFaciltiy.getState().trim()));
            }

            if (_arrayOfTrackFaciltiy.getDiscipline() != null && _arrayOfTrackFaciltiy.getDiscipline().size() > 0) {
                _dataToPost.add(new DataEntity("disciplinename", _arrayOfTrackFaciltiy.getDiscipline().toString().trim().replace("[", "").replace("]", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("disciplinename", ""));
            }
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("type", "facility"));
            _dataToPost.add(new DataEntity("show_all", "" + "No"));

            //
            if (mMap != null) {
                mMap.clear();
            }

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.TRACK_FACILITY, _dataToPost, this);
            _mainApplication.messageManager.DisplayToastMessageAtCenter("Filter have been applied");

        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(ivback);
        }
    }


    public void callWebAPIs(int offset, boolean progressFlag) {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            isApiCalling = true;
            if (progressFlag) {
                showProgressDialog();
            }
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            // _dataToPost.add(new DataEntity("distance", spinnermiles.getSelectedItem().toString().replace("Miles","").trim()));
            if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                _dataToPost.add(new DataEntity("distance", spinnermiles.getSelectedItem().toString().replace("Miles", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }
            if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME) != null) {
                _dataToPost.add(new DataEntity("state", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME)));
            } else {
                _dataToPost.add(new DataEntity("state", ""));
            }
            _dataToPost.add(new DataEntity("disciplinename", ""));
            _dataToPost.add(new DataEntity("type", "facility"));

            _dataToPost.add(new DataEntity("offset", "" + offset));

            _dataToPost.add(new DataEntity("show_all", "" + "No"));

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.TRACK_FACILITY, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(recycler_view);
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void trackFacilityItemClick(int position) {
        if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED); //to close
        }
        _intent = new Intent(TrackFacilityActivity.this, FacilityEventDetailActivity.class);
        _intent.putExtra("LAT", _latitude);
        _intent.putExtra("LONG", _longitude);
        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _rideitem.get(position).getID());
        startActivity(_intent);
    }
}
