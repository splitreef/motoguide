package com.cynexis.motoguide;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.bumptech.glide.Glide;
import com.cynexis.motoguide.Utility.AdvertiseResponse;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.mycalendar.MyCalendarActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.nearbyracesresponse.NearbyEventResponse;
import com.cynexis.motoguide.organizepracticedays.OrganizedPracticeDaysActivity;
import com.cynexis.motoguide.searchallraces.SearchActivity;
import com.cynexis.motoguide.searchallraces.SearchAllRacesActivity;
import com.cynexis.motoguide.searchallraces.SearchAllRacesAdapter;
import com.cynexis.motoguide.wheretoride.TrackFacilityActivity;
import com.google.android.gms.maps.SupportMapFragment;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashBoardActivity extends AppCompatActivity implements WebserviceStartEndEventTracker {


    @BindView(R.id.ivserarch)
    ImageButton ivserarch;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    Intent _intent;
    @BindView(R.id.rlsearchallraces)
    RelativeLayout rlsearchallraces;
    @BindView(R.id.rlmycalendar)
    RelativeLayout rlmycalendar;
    @BindView(R.id.rltrackfacility)
    RelativeLayout rltrackfacility;
    @BindView(R.id.rlorganizedpractiseday)
    RelativeLayout rlorganizedpractiseday;
    @BindView(R.id.ivcalendar)
    ImageView ivcalendar;

    @BindView(R.id.imv_advertise)
    RoundedImageView imv_advertise;
    @BindView(R.id.imf_advertiseClose)
    ImageView imf_advertiseClose;
    @BindView(R.id.rel_advertise)
    RelativeLayout rel_advertise;


    SlideMenueImplimentationClass _slidingMenu;
    MainApplication _mainApplication;
    private Boolean isImageAvailable = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        initializeActivity();
        bindControls();
    }


    private void initializeActivity() {
        setContentView(R.layout.activity_dash_board);
        ButterKnife.bind(this);
    }

    private void bindControls() {
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();
        callApiToGetAdvertiseImage();
    }

    @OnClick({R.id.ivserarch, R.id.ivmenu})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivserarch:
                _intent = new Intent(this, SearchActivity.class);
                startActivity(_intent);
                break;
            case R.id.ivmenu:
                _slidingMenu.ShowMenueOnActionBarClick();
                break;

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isImageAvailable) {
            rel_advertise.setVisibility(View.VISIBLE);
        } else {
            rel_advertise.setVisibility(View.GONE);
        }

    }

    @OnClick({R.id.rlsearchallraces, R.id.rlmycalendar, R.id.rltrackfacility, R.id.rlorganizedpractiseday, R.id.imf_advertiseClose})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.rlsearchallraces:
                _intent = new Intent(this, SearchAllRacesActivity.class);
                startActivity(_intent);
                break;
            case R.id.rltrackfacility:
                _intent = new Intent(this, TrackFacilityActivity.class);
                startActivity(_intent);
                break;
            case R.id.rlorganizedpractiseday:
                _intent = new Intent(this, OrganizedPracticeDaysActivity.class);
                startActivity(_intent);
                break;
            case R.id.rlmycalendar:
                _intent = new Intent(this, MyCalendarActivity.class);
                startActivity(_intent);
                break;
            case R.id.imf_advertiseClose:
                rel_advertise.setVisibility(View.GONE);
                break;


        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    private void callApiToGetAdvertiseImage() {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("", ""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.ADVERTISEMENT, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(rel_advertise);
        }
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.ADVERTISEMENT)) {
                AdvertiseResponse _response = (AdvertiseResponse) _object[0];
                if (_response != null) {
                    if (_response.getStatus() == 1) {
                        isImageAvailable = true;
                        rel_advertise.setVisibility(View.VISIBLE);
                        Glide.with(this)
                                .load(_response.getData().get(0).getUrl())
                                .into(imv_advertise);
                    } else {
                        rel_advertise.setVisibility(View.GONE);
                        isImageAvailable = false;

                        //_mainApplication.messageManager.DisplayToastMessage("" + _response.getMessage());

                    }
                }
            } else {
                //Log.i("tanvi", String.valueOf(_object[0]));
            }
        } else {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }


    @Override
    public Context getActivityContext() {
        return this;
    }

}
