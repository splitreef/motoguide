package com.cynexis.motoguide.lib;

/**
 * Created by Anish on 3/1/2016.
 */
public class SetterGetterForLeftToRightAdapter {

    String title;
    int iconId;

    public SetterGetterForLeftToRightAdapter(String _title, int _iconId)
    {
        this.title = _title;
        this.iconId = _iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }
}
