package com.cynexis.motoguide.lib;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.cynexis.motoguide.R;

import java.util.List;

/**
 * Created by Anish on 3/1/2016.
 */

public class SlidingMenuAdapter extends BaseAdapter {

    private Activity activity;
    private List<DataEntity> _items;
    private LayoutInflater inflater;

    public SlidingMenuAdapter(Activity activity, List<DataEntity> drawerItem)
    {
        this.activity = activity;
        this._items = drawerItem;
    }



    @Override
    public int getCount() {
        return _items.size();
    }

    @Override
    public Object getItem(int position) {
        return _items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (inflater == null)
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
            convertView = inflater.inflate(R.layout.left_to_right_adapter, null);

        if(_items.get(position).getName().trim().length() != 0)
        {
            TextView tvtitle = (TextView) convertView.findViewById(R.id.tvtitle);
            tvtitle.setText(_items.get(position).getName());
            ImageView _image = (ImageView) convertView.findViewById(R.id.ivshow);
            _image.setImageResource(Integer.valueOf(_items.get(position).getValue()));
        }

        return convertView;
    }
}
