
package com.cynexis.motoguide.lib;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;


import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.settingactivities.ProfileSettingActivity;
import com.cynexis.motoguide.settingactivities.AccountInfoActivity;
import com.cynexis.motoguide.startingactivities.LoginActivity;

import java.util.ArrayList;


public class SlideMenueImplimentationClass {

	Activity _activity;
	public SlidingMenu menu;
	ListView _listView;
	boolean _touchModeMargine;
    Intent _intent;
	MainApplication _mainApplication;
	
	public SlideMenueImplimentationClass(Activity _act, boolean _enableTouchModeMargine) {
		// TODO Auto-generated constructor stub
		this._activity = _act;
		this._touchModeMargine = _enableTouchModeMargine;
        _mainApplication = ((MainApplication) _act.getApplicationContext());
	}
	
	public void AttachSlideMenueToActivity() {
		// TODO Auto-generated method stub
			menu = new SlidingMenu(this._activity);
			if(this._touchModeMargine)
			{
				menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
			}
			else
			{
				menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
			}

			menu.setMode(SlidingMenu.RIGHT); // Open from right
			menu.setBehindOffset(130);
			menu.setFadeDegree(0.35f);
			menu.attachToActivity(_activity, SlidingMenu.RIGHT,true);
			menu.setMenu(R.layout.left_menue_new);
			_listView = (ListView) menu.findViewById(R.id.left_drawer);
			ArrayList<DataEntity> _listForExpandableListView = new ArrayList<DataEntity>();
/*
			_listForExpandableListView.add(new DataEntity("Account Information",String.valueOf(R.mipmap.settings))); // 0
*/
/*
		    _listForExpandableListView.add(new DataEntity("Profile Settings",String.valueOf(R.mipmap.userprofile))); // 1
*/
			_listForExpandableListView.add(new DataEntity("Contact Us",String.valueOf(R.mipmap.contactus))); // 2
			_listForExpandableListView.add(new DataEntity("more information",String.valueOf(R.mipmap.moreinfo))); // 3
/*
			_listForExpandableListView.add(new DataEntity("Logout",String.valueOf(R.mipmap.logout))); // 4
*/
			SlidingMenuAdapter _adapter =  new SlidingMenuAdapter(_activity,_listForExpandableListView);
			_listView.setAdapter(_adapter);
			_listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

					/*if(position == 0)
					{
						_intent = new Intent(_activity,AccountInfoActivity.class);
						_activity.startActivity(_intent);
                        menu.toggle();
					}*/

					/*if(position == 1)
					{
						_intent = new Intent(_activity,ProfileSettingActivity.class);
						_activity.startActivity(_intent);
						menu.toggle();
					}*/

					if(position == 0)
					{
						/*_intent = new Intent(_activity,RaceEventDetailActivity.class);
						_activity.startActivity(_intent);*/
                        _intent = new Intent(Intent.ACTION_SEND);
                        _intent.setType("text/plain");
                        _intent.putExtra(Intent.EXTRA_EMAIL,"admin@motoguideapp.com");
                        _intent.putExtra(Intent.EXTRA_SUBJECT, "MotoGuide Contact");
                        //_intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");
                        _activity.startActivity(Intent.createChooser(_intent, "Send Email"));
                        menu.toggle();
					}

					if(position == 1)
					{
                        String url = "https://www.motoguideapp.com/";  _intent = new Intent(Intent.ACTION_VIEW); _intent.setData(Uri.parse(url)); _activity.startActivity(_intent);
                        menu.toggle();
					}

					/*if(position == 3)
					{
						AlertDialog alertDialog = new AlertDialog.Builder(_activity).create();

						alertDialog.setTitle("Logout");

						alertDialog.setMessage("Are you sure you want to logout.");

						alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();
								ActivityCompat.finishAffinity(_activity);
								_intent =  new Intent(_activity,LoginActivity.class);
								_mainApplication.settingManager.setSetting(TypeFactory.SettingType.LOGIN_SUCCESS,"");
								_activity.startActivity(_intent);
								_activity.finish();
							} });


						alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "No", new DialogInterface.OnClickListener() {

							public void onClick(DialogInterface dialog, int id) {
								dialog.dismiss();

							}});
						alertDialog.show();
					}*/

				}
			});


	}
	
	public void ShowMenueOnActionBarClick()
	{
		if(menu != null)
		{
			menu.showMenu();
		}
	}

	public void updateMessageCount(String _messageCount)
	{
        ArrayList<DataEntity> _listForExpandableListView = new ArrayList<DataEntity>();
        _listForExpandableListView.add(new DataEntity("MY PEOPLEPEDIA PAGE")); // 0
        _listForExpandableListView.add(new DataEntity("ACCOUNT SETTINGS")); // 1
        _listForExpandableListView.add(new DataEntity("ADDITIONAL PROFILE INFO")); // 2
        _listForExpandableListView.add(new DataEntity("INVITE FRIENDS")); // 3
        _listForExpandableListView.add(new DataEntity("EDIT PROFILE")); // 4
        _listForExpandableListView.add(new DataEntity("EDIT ENTRIES")); // 5
        _listForExpandableListView.add(new DataEntity("MESSAGE",_messageCount)); // 6
        _listForExpandableListView.add(new DataEntity("LOGOUT")); // 7
        SlidingMenuAdapter _adapter =  new SlidingMenuAdapter(_activity,_listForExpandableListView);
        _listView.setAdapter(_adapter);
        _adapter.notifyDataSetChanged();
	}
}
