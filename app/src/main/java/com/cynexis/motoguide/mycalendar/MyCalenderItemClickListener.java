package com.cynexis.motoguide.mycalendar;

/**
 * Created by  on 30-12-2019.
 */
public interface MyCalenderItemClickListener {
    public void calenderItemClick(String arraytype,String tvid,String tvtypefordelete);
    public void deleteCalenderItem(String tvid,String tvtypefordelete);
}
