package com.cynexis.motoguide.mycalendar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.alexandrius.accordionswipelayout.library.SwipeLayout;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.organizepracticedays.OrganisedPractiseAdapter;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 13-06-2017.
 */

public class MyCalendarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {

    Context _ctx;
    List<SetterGetterForMyCalendar> _eventBinder;
    List<SetterGetterForMyCalendar> _eventBinderTemporary;
    LayoutInflater inflaterone = null;
    MyCalenderItemClickListener myCalenderItemClickListener;
    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    private boolean showLoader;

    public MyCalendarAdapter(Context _ctx, List<SetterGetterForMyCalendar> eventBinder, MyCalenderItemClickListener myCalenderItemClickListener) {
        this._ctx = _ctx;
        this._eventBinder = eventBinder;
        this._eventBinderTemporary = this._eventBinder;
        inflaterone = (LayoutInflater) _ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.myCalenderItemClickListener = myCalenderItemClickListener;
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }


    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {

            @SuppressWarnings("unchecked")
            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                _eventBinderTemporary = (ArrayList<SetterGetterForMyCalendar>) results.values;
                notifyDataSetChanged();
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<SetterGetterForMyCalendar> FilteredList = new ArrayList<SetterGetterForMyCalendar>();
                if (constraint == null || constraint.length() == 0) {
                    // No filter implemented we return all the list
                    results.values = _eventBinder;
                    results.count = _eventBinder.size();
                    _eventBinderTemporary = _eventBinder;
                } else {
                    for (int i = 0; i < _eventBinder.size(); i++) {
                        String data = _eventBinder.get(i).getEventName() + "" + _eventBinder.get(i).getCity() + "" + _eventBinder.get(i).getState() + "" + _eventBinder.get(i).getZip() + "" + _eventBinder.get(i).getSanctionType() + "" + _eventBinder.get(i).getDisciplineType()
                                + "" + _eventBinder.get(i).getEquipmentType() + "" + _eventBinder.get(i).getFactoryContingency() + "" + _eventBinder.get(i).getType() + "" + _eventBinder.get(i).getArrayType() + "" + _eventBinder.get(i).getDistance() + "" + _eventBinder.get(i).getStartdate() + "" + _eventBinder.get(i).getEnddate() + "" + _eventBinder.get(i).getTrackFacility();
                        if (data.toLowerCase().contains(constraint.toString())) {

                            if (_eventBinder.get(i).getArrayType().equals("EVENT")) {
                                FilteredList.add(new SetterGetterForMyCalendar(_eventBinder.get(i).getId(), _eventBinder.get(i).getEventName(), _eventBinder.get(i).getCity(), _eventBinder.get(i).getState(), _eventBinder.get(i).getZip(), _eventBinder.get(i).getSanctionType(), _eventBinder.get(i).getDisciplineType()
                                        , _eventBinder.get(i).getEquipmentType(), _eventBinder.get(i).getFactoryContingency(), _eventBinder.get(i).getType(), _eventBinder.get(i).getArrayType(), _eventBinder.get(i).getDistance(), _eventBinder.get(i).getEnddate(), _eventBinder.get(i).getEnddate(), _eventBinder.get(i).getTrackFacility()));
                            } else {
                                FilteredList.add(new SetterGetterForMyCalendar(_eventBinder.get(i).getId(), _eventBinder.get(i).getEventName(), _eventBinder.get(i).getCity(), _eventBinder.get(i).getState(), _eventBinder.get(i).getZip(), _eventBinder.get(i).getSanctionType(), _eventBinder.get(i).getDisciplineType()
                                        , _eventBinder.get(i).getEquipmentType(), _eventBinder.get(i).getFactoryContingency(), _eventBinder.get(i).getType(), "FACILITY", _eventBinder.get(i).getDistance(), _eventBinder.get(i).getStartdate(), _eventBinder.get(i).getEnddate(), ""));
                            }

                            //FilteredList.add(data);
                        }
                    }
                    results.values = FilteredList;
                    results.count = FilteredList.size();
                }
                return results;
            }
        };
        return filter;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itemswipe_custom_mycalendar, parent, false);
                return new MyCalendarAdapter.ViewHolderOne(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
                return new MyCalendarAdapter.FooterLoader(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == _eventBinderTemporary.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }



    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof MyCalendarAdapter.FooterLoader) {
            MyCalendarAdapter.FooterLoader loaderViewHolder = (MyCalendarAdapter.FooterLoader) viewHolder;
            if (showLoader) {
                loaderViewHolder.loadmore_progress.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.loadmore_progress.setVisibility(View.GONE);
            }
            return;
        }
        final MyCalendarAdapter.ViewHolderOne holder = ((MyCalendarAdapter.ViewHolderOne) viewHolder);

        if (_eventBinderTemporary.get(position).getArrayType().equalsIgnoreCase("EVENT")) {
            holder.rlfacilitycontainer.setVisibility(View.GONE);
            holder.rleventcontainer.setVisibility(View.VISIBLE);
            //DecimalFormat df = new DecimalFormat("###.#");
            holder.tvadress.setText(_eventBinderTemporary.get(position).getCity() + ", " + _eventBinderTemporary.get(position).getState() + " - " + _eventBinderTemporary.get(position).getDistance());
            holder.tvracename.setText(_eventBinderTemporary.get(position).getEventName());
            holder.tvtype.setText(_eventBinderTemporary.get(position).getTrackFacility());
            holder.tvenddate.setText("to " + CommonUtility.convertDateFormat(_eventBinderTemporary.get(position).getEnddate(), "yyyy-MM-dd", "MM/dd"));
            holder.tvcalendardate.setText(CommonUtility.convertDateFormat(_eventBinderTemporary.get(position).getStartdate(), "yyyy-MM-dd", "MMM") + "\n" + CommonUtility.convertDateFormat(_eventBinderTemporary.get(position).getStartdate(), "yyyy-MM-dd", "d"));
            if (_eventBinderTemporary.get(position).getType().equalsIgnoreCase("practiceday")) {
                //holder.ivflag.setImageResource(R.drawable.ic_openpracticeday_new);
                holder.ivflag.setVisibility(View.GONE);
                holder.ivflag_prctice.setVisibility(View.VISIBLE);

            } else if (_eventBinderTemporary.get(position).getType().equalsIgnoreCase("Event")) {
                // holder.ivflag.setImageResource(R.drawable.ic_searchallevents_new);
                holder.ivflag.setVisibility(View.VISIBLE);
                holder.ivflag_prctice.setVisibility(View.GONE);
            }
        } else {
            holder.rlfacilitycontainer.setVisibility(View.VISIBLE);
            holder.rleventcontainer.setVisibility(View.GONE);
            //DecimalFormat df = new DecimalFormat("###.#");
            //holder.tvadresss.setText(_eventBinderTemporary.get(position).getCity()+", "+_eventBinderTemporary.get(position).getState()+" - "+df.format(Double.parseDouble(_eventBinderTemporary.get(position).getDistance()))+" mi");
            holder.tvadresss.setText(_eventBinderTemporary.get(position).getCity() + ", " + _eventBinderTemporary.get(position).getState() + " - " + _eventBinderTemporary.get(position).getDistance());
            holder.tveventname.setText(_eventBinderTemporary.get(position).getEventName());

            if (_eventBinderTemporary.get(position).getType().equalsIgnoreCase("OpenRidingAreas")) {
                holder.ivright.setVisibility(View.GONE);
                holder.ivright_cwatch.setVisibility(View.VISIBLE);
            } else {
                holder.ivright.setVisibility(View.VISIBLE);
                holder.ivright_cwatch.setVisibility(View.GONE);
            }
        }
        holder.tvtypefordelete.setText(_eventBinderTemporary.get(position).getType());
        holder.tvarraytype.setText(_eventBinderTemporary.get(position).getArrayType());
        holder.tvid.setText(_eventBinderTemporary.get(position).getId());

        if (position == 0) {
            holder.llroot.setBackgroundResource(R.color.white);
        }
        if (position == 1) {
            holder.llroot.setBackgroundResource(R.color.listcolor);
        }

        if (position > 1) {
            if (position % 2 == 0) {
                holder.llroot.setBackgroundResource(R.color.white);
            } else {
                holder.llroot.setBackgroundResource(R.color.listcolor);
            }
        }

        holder.swipeLayout.setOnSwipeItemClickListener(new SwipeLayout.OnSwipeItemClickListener() {
            @Override
            public void onSwipeItemClick(boolean b, int i) {
                // Toast.makeText(_ctx, "", Toast.LENGTH_SHORT).show();
                // moviesList.remove(position);
                // notifyDataSetChanged();
                myCalenderItemClickListener.deleteCalenderItem(holder.tvid.getText().toString(),
                        holder.tvtypefordelete.getText().toString());

            }
        });
        holder.swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myCalenderItemClickListener.calenderItemClick(holder.tvarraytype.getText().toString(),
                        holder.tvid.getText().toString(), holder.tvtypefordelete.getText().toString());

            }
        });
    }

    @Override
    public int getItemCount() {
        return _eventBinderTemporary.size();
    }

    class ViewHolderOne extends RecyclerView.ViewHolder {
        @BindView(R.id.tvcalendardate)
        public TextView tvcalendardate;
        @BindView(R.id.tvid)
        public TextView tvid;
        @BindView(R.id.tvarraytype)
        public TextView tvarraytype;
        @BindView(R.id.tvadress)
        public TextView tvadress;
        @BindView(R.id.tvtype)
        public TextView tvtype;
        @BindView(R.id.tvracename)
        public TextView tvracename;
        @BindView(R.id.tveventname)
        public TextView tveventname;
        @BindView(R.id.tvenddate)
        public TextView tvenddate;
        @BindView(R.id.tvtypefordelete)
        public TextView tvtypefordelete;

        @BindView(R.id.ivflag)
        public ImageView ivflag;


        @BindView(R.id.ivflag_prctice)
        public ImageView ivflag_prctice;


        @BindView(R.id.tvadresss)
        public TextView tvadresss;
        @BindView(R.id.ivright)
        public ImageView ivright;
        @BindView(R.id.llroot)
        public LinearLayout llroot;
        @BindView(R.id.rlfacilitycontainer)
        public RelativeLayout rlfacilitycontainer;
        @BindView(R.id.rleventcontainer)
        public RelativeLayout rleventcontainer;

        @BindView(R.id.ivright_cwatch)
        public ImageView ivright_cwatch;

        @BindView(R.id.swipe_layout)
        public SwipeLayout swipeLayout;


        public ViewHolderOne(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected class FooterLoader extends RecyclerView.ViewHolder {
        @BindView(R.id.loadmore_progress)
        public ProgressBar loadmore_progress;

        public FooterLoader(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}

