package com.cynexis.motoguide.mycalendar;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;/*
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;*/
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.RaceEventDetailActivity;
import com.cynexis.motoguide.FacilityEventDetailActivity;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.mycalendarresponse.Event;
import com.cynexis.motoguide.network.response.mycalendarresponse.Facility;
import com.cynexis.motoguide.network.response.mycalendarresponse.MycalendarResponse;
import com.cynexis.motoguide.startingactivities.permission.GetLatitudeAndLongitude;
import com.cynexis.motoguide.startingactivities.permission.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MyCalendarActivity extends AppCompatActivity implements WebserviceStartEndEventTracker, GetLatitudeAndLongitude, MyCalenderItemClickListener {

    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SlideMenueImplimentationClass _slidingMenu;
    MainApplication _mainApplication;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    public static final int PERMISSION_REQUEST_CODE = 120;
    String _latitude = "", _longitude = "";
    LocationService _locationService;
    /*  @BindView(R.id.lvmycalendar)
      ListView lvmycalendar;*/
    MyCalendarAdapter myCalendarAdapter;

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    private LinearLayoutManager linearLayoutManager;

    List<SetterGetterForMyCalendar> _setterGetter = new ArrayList<>();
    String _id = "";
    @BindView(R.id.etsearckeyword)
    EditText etsearckeyword;
    Intent intent;
    private int offset = 0;
    private Boolean isApiCalling = false;
    private int dataSize = 0;
    @BindView(R.id.tv_swipe_up_for_more_info)
    TextView tv_swipe_up_for_more_info;
    @BindView(R.id.tv_show_all)
    TextView tv_show_all;
    private Boolean isShowAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        initializeActivity();
        checkPermissionForMarshmallow();
        bindControls();
    }

    private void initializeActivity() {
        setContentView(R.layout.activity_my_calendar);
        ButterKnife.bind(this);
    }

    private void bindControls() {
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();

        etsearckeyword.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text

                if (myCalendarAdapter != null) {
                    if (cs.toString().trim().length() > 0) {
                        myCalendarAdapter.getFilter().filter(cs.toString().trim());
                    } else {
                        myCalendarAdapter.getFilter().filter("");

                        //myCalendarAdapter.notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        etsearckeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

                    }
                    return true;
                }
                return false;
            }
        });

        linearLayoutManager = new LinearLayoutManager(this);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_view.getContext(),
                linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.item_divider));
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView

        myCalendarAdapter = new MyCalendarAdapter(this, _setterGetter, this);
        recycler_view.setAdapter(myCalendarAdapter);

        RecyclerView.OnScrollListener mScrollListener = new
                RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                        if (!isShowAll) {
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                if (!isApiCalling) {
                                    myCalendarAdapter.showLoading(true);
                                    myCalendarAdapter.notifyDataSetChanged();
                                    offset = offset + 10;
                                    apiCalltoGetCalenderData(offset, false);
                                }



                            }
                        }
                    }
                };
        recycler_view.addOnScrollListener(mScrollListener);
        recycler_view.setNestedScrollingEnabled(false);
    }

    private void checkPermissionForMarshmallow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);
                }

            } else {
                if (checkGooglePlayServices()) {
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            /*
             * google play services is missing or update is required
             *  return code could be
             * SUCCESS,
             * SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
             * SERVICE_DISABLED, SERVICE_INVALID.
             */
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;

    }


    @OnClick({R.id.ivback, R.id.ivmenu, R.id.tv_show_all})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.ivmenu:
                _slidingMenu.ShowMenueOnActionBarClick();
                break;
            case R.id.tv_show_all:
                if (!isApiCalling) {
                    if (!isShowAll) {
                        etsearckeyword.setText("");
                        etsearckeyword.clearFocus();
                        isShowAll = true;
                        // tv_show_all.setBackgroundResource(R.drawable.bg_oval_red);
                        // tv_show_all.setAlpha(.5f);
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        _setterGetter.clear();
                        apicallShowAllData(0);
                    } else {
                        etsearckeyword.setText("");
                        etsearckeyword.clearFocus();
                        isShowAll = false;
                        //tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
                        //tv_show_all.setAlpha(1f);
                        tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        offset = 0;
                        _setterGetter.clear();
                        apiCalltoGetCalenderData(0, true);
                    }
                }
                break;

        }
    }


    private void apicallShowAllData(int offset) {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            //isApiCalling = true;
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            _dataToPost.add(new DataEntity("userid",
                    _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("show_all", "Yes"));
            _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.MY_CALENDAR, _dataToPost, this);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            if (imm != null) {
                //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

            }
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(ivback);
        }


    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        if (!isShowAll) {
            handalResponseWithPagination(_object);
        } else {

            handalResponseShowAll(_object);
        }
    }

    private void handalResponseWithPagination(Object... _object) {
        isApiCalling = false;
        myCalendarAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.DELETE_EVENT)) {
                try {
                    if (_object[0] != null) {
                        JSONObject _getObject = new JSONObject(_object[0].toString());
                        if (_getObject.getInt("Status") == 1) {
                            _mainApplication.messageManager.DisplayToastMessageAtCenter(_getObject.getString("message"));
                            for (int i = 0; i < _setterGetter.size(); i++) {
                                if (_setterGetter.get(i).getId().equals(_id)) {
                                    _setterGetter.remove(i);
                                }
                            }
                            etsearckeyword.setText("");
                            myCalendarAdapter = new MyCalendarAdapter(this, _setterGetter, this);
                            recycler_view.setAdapter(myCalendarAdapter);
                        } else {
                            _mainApplication.messageManager.DisplayToastMessageAtCenter(_getObject.getString("message"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                MycalendarResponse _response = (MycalendarResponse) _object[0];
                if (_response != null) {
                    if (_response.getStatus() == 1) {

                        //_setterGetter = new ArrayList<>();
                        /////////// EVENT
                        if (_response.getEvent() != null && _response.getEvent().size() > 0) {
                            for (Event _data : _response.getEvent()) {
                                _setterGetter.add(new SetterGetterForMyCalendar(_data.getID(), _data.getEventName(), _data.getCity(), _data.getState(), _data.getZip(), _data.getSanctionType(), _data.getDisciplineType()
                                        , _data.getEquipmentType(), _data.getFactoryContingency(), _data.getType(), "EVENT", _data.getDistance(), _data.getStartDate(), _data.getEndDate(), _data.getTrackfacility()));
                            }
                        }

                        if (_response.getFacility() != null && _response.getFacility().size() > 0) {
                            for (Facility _data : _response.getFacility()) {
                                _setterGetter.add(new SetterGetterForMyCalendar(_data.getID(), _data.getEventName(), _data.getCity(), _data.getState(), _data.getZip(), _data.getSanctionType(), _data.getDisciplineType()
                                        , _data.getEquipmentType(), _data.getFactoryContingency(), _data.getType(), "FACILITY", _data.getDistance(), _data.getStartDate(), _data.getEndDate(), ""));
                            }
                        }
                        dataSize = _setterGetter.size();
                        // tv_show_all.setVisibility(View.VISIBLE);
                        if (dataSize >= 10) {
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        } else {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        }
                        myCalendarAdapter.notifyDataSetChanged();
                           /* myCalendarAdapter = new MyCalendarAdapter(this, _setterGetter, this);
                            recycler_view.setAdapter(myCalendarAdapter);*/
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                                INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

                        }



                    } else {
                        if (offset == 0) {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            // tv_show_all.setVisibility(View.GONE);

                            myCalendarAdapter.notifyDataSetChanged();
                            _mainApplication.messageManager.DisplayToastMessageAtCenter(_response.getMessage());
                        } else {
                            //  tv_show_all.setVisibility(View.VISIBLE);
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            myCalendarAdapter.showLoading(false);
                            myCalendarAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    //tv_show_all.setVisibility(View.GONE);
                    tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                    _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
                }
            }
        } else {
            //tv_show_all.setVisibility(View.GONE);
            _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
        }
    }

    private void handalResponseShowAll(Object... _object) {
        //isApiCalling = false;
        // myCalendarAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.DELETE_EVENT)) {
                try {
                    if (_object[0] != null) {
                        JSONObject _getObject = new JSONObject(_object[0].toString());
                        if (_getObject.getInt("Status") == 1) {
                            _mainApplication.messageManager.DisplayToastMessageAtCenter(_getObject.getString("message"));
                            for (int i = 0; i < _setterGetter.size(); i++) {
                                if (_setterGetter.get(i).getId().equals(_id)) {
                                    _setterGetter.remove(i);
                                }
                            }
                            etsearckeyword.setText("");
                            myCalendarAdapter = new MyCalendarAdapter(this, _setterGetter, this);
                            recycler_view.setAdapter(myCalendarAdapter);
                        } else {
                            _mainApplication.messageManager.DisplayToastMessageAtCenter(_getObject.getString("message"));
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                MycalendarResponse _response = (MycalendarResponse) _object[0];
                if (_response != null) {
                    if (_response.getStatus() == 1) {

                        //_setterGetter = new ArrayList<>();
                        /////////// EVENT
                        if (_response.getEvent() != null && _response.getEvent().size() > 0) {
                            for (Event _data : _response.getEvent()) {
                                _setterGetter.add(new SetterGetterForMyCalendar(_data.getID(), _data.getEventName(), _data.getCity(), _data.getState(), _data.getZip(), _data.getSanctionType(), _data.getDisciplineType()
                                        , _data.getEquipmentType(), _data.getFactoryContingency(), _data.getType(), "EVENT", _data.getDistance(), _data.getStartDate(), _data.getEndDate(), _data.getTrackfacility()));
                            }
                        }

                        if (_response.getFacility() != null && _response.getFacility().size() > 0) {
                            for (Facility _data : _response.getFacility()) {
                                _setterGetter.add(new SetterGetterForMyCalendar(_data.getID(), _data.getEventName(), _data.getCity(), _data.getState(), _data.getZip(), _data.getSanctionType(), _data.getDisciplineType()
                                        , _data.getEquipmentType(), _data.getFactoryContingency(), _data.getType(), "FACILITY", _data.getDistance(), _data.getStartDate(), _data.getEndDate(), ""));
                            }
                        }
                        //dataSize = _setterGetter.size();
                        /*if (dataSize >= 10) {
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        } else {
                            tv_swipe_up_for_more_info.setVisibility(View.GONE);
                        }*/
                        myCalendarAdapter.notifyDataSetChanged();
                       /* myCalendarAdapter = new MyCalendarAdapter(this, _setterGetter, this);
                        recycler_view.setAdapter(myCalendarAdapter);*/
                        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                                INPUT_METHOD_SERVICE);
                        if (imm != null) {
                            //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

                        }




                    } else {
                        myCalendarAdapter.notifyDataSetChanged();
                        _mainApplication.messageManager.DisplayToastMessageAtCenter(_response.getMessage());


                    }
                } else {
                    //tv_swipe_up_for_more_info.setVisibility(View.GONE);
                    _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
                }
            }
        } else {
            _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
            //tv_swipe_up_for_more_info.setVisibility(View.GONE);
        }

    }


    @Override
    public void getLatLong(String locationBY, String latitude, String longitude) {
        if (latitude.equals("0.0")) {
            _latitude = "";
            _longitude = "";
        } else {
            _latitude = latitude;
            _longitude = longitude;
        }
        _setterGetter.clear();
        offset = 0;
        isShowAll = false;
        apiCalltoGetCalenderData(offset, true);
    }

    private void apiCalltoGetCalenderData(int offset, boolean progressFlag) {
        // Log.i("tanvi", _latitude + " " + _longitude);
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            isApiCalling = true;
            if (progressFlag) {
                showProgressDialog();
            }
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("show_all", "" + "No"));
            _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.MY_CALENDAR, _dataToPost, this);
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                    INPUT_METHOD_SERVICE);
            if (imm != null) {
                //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

            }
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(ivback);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted) {
                        if (_locationService == null) {
                            _locationService = new LocationService(this, this);
                            _locationService.makeConnectionIfRequired();
                        }
                    } else {
                        Snackbar.make(recycler_view, "Permission Denied. Please allow and try loading this screen.", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        if (imm != null) {
            //imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
            imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);

        }
        return true;
    }

    public void moveToDetailPage(int id) {
        // Log.i("tanvi",_setterGetter.get(id).getId()+ _setterGetter.get(id).getArrayType());
        if (_setterGetter.get(id).getArrayType().equals("EVENT")) {
            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _setterGetter.get(id).getId());
            intent = new Intent(this, RaceEventDetailActivity.class);
            intent.putExtra("LAT", _latitude);
            intent.putExtra("LONG", _longitude);
            startActivity(intent);
        } else {
            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _setterGetter.get(id).getId());
            intent = new Intent(this, FacilityEventDetailActivity.class);
            intent.putExtra("LAT", _latitude);
            intent.putExtra("LONG", _longitude);
            startActivity(intent);
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void calenderItemClick(String arraytype, String tvid, String tvtypefordelete) {

//        if(((TextView) view.findViewById(R.id.tvarraytype)).getText().toString().equals("EVENT"))
        if (arraytype.equalsIgnoreCase("EVENT")) {
            //_mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID,((TextView) view.findViewById(R.id.tvid)).getText().toString());
            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, tvid);
            intent = new Intent(MyCalendarActivity.this, RaceEventDetailActivity.class);
            intent.putExtra("LAT", _latitude);
            intent.putExtra("LONG", _longitude);
            // intent.putExtra("TYPE",((TextView) view.findViewById(R.id.tvtypefordelete)).getText().toString());
            intent.putExtra("TYPE", tvtypefordelete);
            startActivity(intent);
        } else {
            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, tvid);
            intent = new Intent(MyCalendarActivity.this, FacilityEventDetailActivity.class);
            intent.putExtra("LAT", _latitude);
            intent.putExtra("LONG", _longitude);
            // intent.putExtra("TYPE",((TextView) view.findViewById(R.id.tvtypefordelete)).getText().toString());
            intent.putExtra("TYPE", tvtypefordelete);
            startActivity(intent);
        }
    }

    @Override
    public void deleteCalenderItem(String tvid, String tvtypefordelete) {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _id = tvid;
            _dataToPost.add(new DataEntity("EventID", tvid));
            _dataToPost.add(new DataEntity("UserID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            _dataToPost.add(new DataEntity("Type", tvtypefordelete));
            _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.DELETE_EVENT, _dataToPost, MyCalendarActivity.this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(ivback);
        }
    }
}


