package com.cynexis.motoguide.mycalendar;

/**
 * Created by Anish on 16/06/17.
 */

public class SetterGetterForMyCalendar {

    String id,eventName,city,state,zip,sanctionType,disciplineType,equipmentType,factoryContingency,type,arrayType,distance,startdate,enddate,trackFacility;

    public SetterGetterForMyCalendar(String id, String eventName, String city, String state, String zip, String sanctionType, String disciplineType, String equipmentType, String factoryContingency, String type, String arrayType, String distance, String startdate,String enddate,String trackFacility)
    {
        this.id = id;
        this.eventName = eventName;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.sanctionType = sanctionType;
        this.disciplineType = disciplineType;
        this.equipmentType = equipmentType;
        this.factoryContingency = factoryContingency;
        this.type = type;
        this.arrayType = arrayType;
        this.distance = distance;
        this.startdate = startdate;
        this.enddate = enddate;
        this.trackFacility = trackFacility;
    }

    public String getTrackFacility() {
        return trackFacility;
    }

    public void setTrackFacility(String trackFacility) {
        this.trackFacility = trackFacility;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getSanctionType() {
        return sanctionType;
    }

    public void setSanctionType(String sanctionType) {
        this.sanctionType = sanctionType;
    }

    public String getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(String disciplineType) {
        this.disciplineType = disciplineType;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getFactoryContingency() {
        return factoryContingency;
    }

    public void setFactoryContingency(String factoryContingency) {
        this.factoryContingency = factoryContingency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getArrayType() {
        return arrayType;
    }

    public void setArrayType(String arrayType) {
        this.arrayType = arrayType;
    }
}
