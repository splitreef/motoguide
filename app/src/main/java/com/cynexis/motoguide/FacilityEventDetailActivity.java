package com.cynexis.motoguide;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.facilitydataresponse.EquipmentType;
import com.cynexis.motoguide.network.response.facilitydataresponse.FacilityDetailData;
import com.cynexis.motoguide.network.response.facilitydataresponse.FacilityDetailResponse;
import com.cynexis.motoguide.network.response.facilitydataresponse.Image;
import com.cynexis.motoguide.network.response.facilitydataresponse.UpcomingEvent;
import com.cynexis.motoguide.network.response.facilitydataresponse.Video;
import com.cynexis.motoguide.upcomingeventdetail.UpcomingDetailActivity;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class FacilityEventDetailActivity extends AppCompatActivity implements WebserviceStartEndEventTracker, MyRecyclerViewAdapter.ItemClickListener {


    Intent _intent;
    MainApplication _mainApplication;
    Bundle _extra;
    String _latitude = "0.0", _longitude = "0.0";
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ivaddcalendar)
    ImageView ivaddcalendar;
    @BindView(R.id.lladdcalendar)
    LinearLayout lladdcalendar;
    @BindView(R.id.tveventname)
    TextView tveventname;
    /*@BindView(R.id.tveventfacility)
    TextView tveventfacility;*/
    @BindView(R.id.tveventaddres)
    TextView tveventaddres;
    @BindView(R.id.tvsanctiontype)
    TextView tvsanctiontype;
    @BindView(R.id.tvdiscipline)
    TextView tvdiscipline;
    @BindView(R.id.tvadditionalinfo)
    TextView tvadditionalinfo;
    SlideMenueImplimentationClass _slidingMenu;
    List<FacilityDetailData> _facilitydetailitem;
    @BindView(R.id.iv_coverpic)
    ImageView ivCoverpic;
    @BindView(R.id.iv_logo)
    CircleImageView ivLogo;
    LayoutInflater inflater;

    @BindView(R.id.llfb)
    LinearLayout llfb;
    @BindView(R.id.llcall)
    LinearLayout llcall;
    @BindView(R.id.llmsg)
    LinearLayout llmsg;
    @BindView(R.id.llmap)
    LinearLayout llmap;
    @BindView(R.id.llmedia)
    LinearLayout llmedia;
    @BindView(R.id.llweb)
    LinearLayout llweb;
    TextView tvid, tvtype;
    ImageView ivflag;
    TextView tvracename;
    TextView tvupcomingcalendardate;
    RelativeLayout rlroot;
    @BindView(R.id.llupcomingevent)
    LinearLayout llupcomingevent;
    @BindView(R.id.loading_spinner)
    ProgressBar loadingSpinner;
    @BindView(R.id.activity_race_event_detail_one)
    LinearLayout activityRaceEventDetailOne;
    @BindView(R.id.recycleView)
    RecyclerView recycleView;
    @BindView(R.id.childscrollview)
    NestedScrollView childscrollview;
    @BindView(R.id.parentscrollview)
    NestedScrollView parentscrollview;
    @BindView(R.id.llequipmentcontainer)
    LinearLayout llequipmentcontainer;
    View _view;
    String type = "";
    @BindView(R.id.tvnomedia)
    TextView tvnomedia;
    TextView tvequipmentname;
    ImageView ivequipmenttype;
    ArrayList<String> _discipline;
    List<EquipmentType> _equipmentlist;
    List<UpcomingEvent> _upcomingevent;

    ArrayList<HashMap<String, String>> _imagePaths;

    @BindView(R.id.viewPager)
    public ViewPager viewPager;
    @BindView(R.id.tabDots)
    public TabLayout tabLayout;
    private FacilityEventPagerAdapter facilityEventPagerAdapter;
    private List<String> listImages = new ArrayList<>();
    private String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        _extra = this.getIntent().getExtras();
        _latitude = _extra.getString("LAT");
        _longitude = _extra.getString("LONG");
        from = _extra.getString("from");

        initializeActivity();
        bindControls();

    }

    private void initializeActivity() {
        setContentView(R.layout.activity_race_event_detail_one);
        ButterKnife.bind(this);
        ivmenu.setVisibility(View.INVISIBLE);
        recycleView.setNestedScrollingEnabled(false);
        enabelingDesiablingParentAndChildScrooll();

        tabLayout.setupWithViewPager(viewPager, true);
        facilityEventPagerAdapter = new FacilityEventPagerAdapter(this, listImages);
        viewPager.setAdapter(facilityEventPagerAdapter);
    }

    private void bindControls() {
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();

        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("id", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.EVENTID)));
            _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.FACILITY_DETAIL, _dataToPost, FacilityEventDetailActivity.this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(toolbar);
        }
    }

    @OnClick({R.id.ivback, R.id.ivmenu, R.id.llweb, R.id.llfb, R.id.llcall, R.id.llmsg, R.id.llmap, R.id.lladdcalendar})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.ivmenu:
                _slidingMenu.ShowMenueOnActionBarClick();
                break;
            case R.id.llweb:
                if (_facilitydetailitem.get(0).getWebsiteURL() != null && _facilitydetailitem.get(0).getWebsiteURL().length() > 0) {
                    _intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_facilitydetailitem.get(0).getWebsiteURL()));
                    startActivity(_intent);
                } else {
                    _mainApplication.messageManager.DisplayToastMessage("Not Valid Url");
                }
                break;
            case R.id.llfb:
                if (_facilitydetailitem.get(0).getFacebookPageURL() != null && _facilitydetailitem.get(0).getFacebookPageURL().length() > 0) {
                    /**
                     * open facebook link on website
                     * */

                  /*  _intent = new Intent(Intent.ACTION_VIEW, Uri.parse(_facilitydetailitem.get(0).getFacebookPageURL()));
                    startActivity(_intent);
*/
                    /**
                     * open facebook link inside facebook app
                     */

                    Uri uri = Uri.parse(_facilitydetailitem.get(0).getFacebookPageURL());
                    try {

                        ApplicationInfo applicationInfo = this.getPackageManager().getApplicationInfo("com.facebook.katana", 0);
                        if (applicationInfo.enabled) {
                            // http://stackoverflow.com/a/24547437/1048340
                            uri = Uri.parse("fb://facewebmodal/f?href=" + _facilitydetailitem.get(0).getFacebookPageURL());
                        }
                    } catch (Exception ignored) {
                        ignored.printStackTrace();
                    }
                    _intent = new Intent(Intent.ACTION_VIEW, uri);
                    //startActivity(_intent);

                    if (_intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(_intent);
                    }

                } else {
                    // llfb.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                    _mainApplication.messageManager.DisplayToastMessage("Not Valid Url");
                }
                break;
            case R.id.llmsg:
                if (_facilitydetailitem.get(0).getEmail() != null && _facilitydetailitem.get(0).getEmail().length() > 0) {
                    _intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                            "mailto", _facilitydetailitem.get(0).getEmail(), null));
                    _intent.putExtra(Intent.EXTRA_SUBJECT, "MotoGuide Contact");
                    _intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(Intent.createChooser(_intent, "Select Email Sending App"));
                   /* _intent = new Intent(Intent.ACTION_SEND);
                    _intent.setType("text/plain");
                    _intent.putExtra(Intent.EXTRA_EMAIL, _facilitydetailitem.get(0).getEmail());
                    _intent.putExtra(Intent.EXTRA_SUBJECT, "MotoGuide Contact");
                    startActivity(Intent.createChooser(_intent, "Send Email"));*/
                } else {
                    llmsg.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                }
                break;
            case R.id.llcall:
                if (_facilitydetailitem.get(0).getPhone() != null && _facilitydetailitem.get(0).getPhone().length() > 0) {
                    if (isPermissionGranted()) {
                        _intent = new Intent(Intent.ACTION_CALL);
                        _intent.setData(Uri.parse("tel:" + _facilitydetailitem.get(0).getPhone()));//change the number
                        startActivity(_intent);
                    }

                } else {
                    llcall.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                }
                break;
            case R.id.llmap:
                if (_facilitydetailitem.get(0).getLatitude() != null && _facilitydetailitem.get(0).getLatitude().length() > 0) {
                    _intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + String.valueOf(_latitude) + "," + String.valueOf(_longitude) + "&daddr=" + _facilitydetailitem.get(0).getLatitude() + "," + _facilitydetailitem.get(0).getLongitude()));
                    startActivity(_intent);
                } else {
                    llmap.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                }
                break;
            case R.id.lladdcalendar:
                if (_facilitydetailitem.get(0).getAlreadyAdded() == 1) {
                    ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                    lladdcalendar.setClickable(false);
                } else {
                    ivaddcalendar.setImageResource(R.mipmap.addevent);
                    if (_mainApplication.networkManager.CheckNetworkConnection()) {
                        showProgressDialog();
                        List<DataEntity> _dataToPost = new ArrayList<>();
                        _dataToPost.add(new DataEntity("EventID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.EVENTID)));
                        _dataToPost.add(new DataEntity("UserID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
                        _dataToPost.add(new DataEntity("DeviceID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.DEVICE_ID)));
                        if (from != null && from.equalsIgnoreCase("practice_day")) {
                            _dataToPost.add(new DataEntity("Type", "OpenRidingAreas"));

                        } else {
                            _dataToPost.add(new DataEntity("Type", "Facility"));
                        }
                        AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.ADD_CALENDAR, _dataToPost, FacilityEventDetailActivity.this);
                    } else {
                        _mainApplication.messageManager.noInternetConnectionMessage(toolbar);
                    }
                }

                break;
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (_object[0] instanceof FacilityDetailResponse) {
                if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.FACILITY_DETAIL)) {
                    FacilityDetailResponse _response = (FacilityDetailResponse) _object[0];
                    if (_response.getStatus() == 1) {

                        if (_response.getData() != null && _response.getData().size() > 0) {
                            _facilitydetailitem = _response.getData();
                        }

                        _imagePaths = new ArrayList<>();

                        if (_response.getMedia() != null) {

                            if (_response.getMedia().getImage() != null && _response.getMedia().getImage().size() > 0) {
                                for (Image _imagePath :
                                        _response.getMedia().getImage()) {

                                    HashMap<String, String> _data = new HashMap<String, String>();
                                    _data.put("ITEM", _imagePath.getFilepath());
                                    _data.put("TYPE", "IMAGE");
                                    _imagePaths.add(_data);
                                }

                            }

                            if (_response.getMedia().getVideo() != null && _response.getMedia().getVideo().size() > 0) {
                                for (Video _videoPath :
                                        _response.getMedia().getVideo()) {

                                    HashMap<String, String> _data = new HashMap<String, String>();
                                    _data.put("ITEM", _videoPath.getVideourl());
                                    _data.put("TYPE", "VIDEO");
                                    _imagePaths.add(_data);
                                }
                            }
                            if (_imagePaths.size() == 0) {
                                tvnomedia.setVisibility(View.VISIBLE);

                            }

                            recycleView.setLayoutManager(new GridLayoutManager(this, 3));
                            MyRecyclerViewAdapter adapter = new MyRecyclerViewAdapter(this, _imagePaths);
                            adapter.setClickListener(this);
                            recycleView.setAdapter(adapter);
                        }

                        if (_response.getData() != null && _response.getData().size() > 0) {
                            _facilitydetailitem = _response.getData();
                            if (_facilitydetailitem.get(0).getAlreadyAdded() == 1) {
                                ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                            } else {
                                ivaddcalendar.setImageResource(R.mipmap.addevent);
                            }

                          /*  if (_facilitydetailitem.get(0).getCoverphoto() != null && _facilitydetailitem.get(0).getCoverphoto().length() > 0) {
                                loadingSpinner.setVisibility(View.VISIBLE);
                                Picasso.with(this)
                                        .load(_facilitydetailitem.get(0).getCoverphoto())
                                        .into(ivCoverpic, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                loadingSpinner.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                            } else {
                                ivCoverpic.setImageResource(R.mipmap.dashboard_two);
                            }*/

                            if (_facilitydetailitem.get(0).getCoverphoto() != null && _facilitydetailitem.get(0).getCoverphoto().size() > 0) {
                                listImages.clear();
                                ivCoverpic.setVisibility(View.GONE);
                                viewPager.setVisibility(View.VISIBLE);
                                tabLayout.setVisibility(View.VISIBLE);
                                for (int i = 0; i < _facilitydetailitem.get(0).getCoverphoto().size(); i++) {
                                    listImages.add(_facilitydetailitem.get(0).getCoverphoto().get(i));
                                }
                                // loadingSpinner.setVisibility(View.VISIBLE);
//                                tabLayout.setupWithViewPager(viewPager, true);
//                                facilityEventPagerAdapter = new FacilityEventPagerAdapter(this, listImages);
//                                viewPager.setAdapter(facilityEventPagerAdapter);
                                facilityEventPagerAdapter.notifyDataSetChanged();
                            } else {
                                ivCoverpic.setVisibility(View.VISIBLE);
                                viewPager.setVisibility(View.INVISIBLE);
                                tabLayout.setVisibility(View.INVISIBLE);
                                ivCoverpic.setImageResource(R.mipmap.dashboard_two);
                            }


                            if (_facilitydetailitem.get(0).getLogo() != null && _facilitydetailitem.get(0).getLogo().length() > 0) {
                                //  ivLogo.setImageURI(Uri.parse(_facilitydetailitem.get(0).getLogo()));
                                loadingSpinner.setVisibility(View.VISIBLE);
                                Picasso.with(this)
                                        .load(_facilitydetailitem.get(0).getLogo())
                                        .into(ivLogo, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                loadingSpinner.setVisibility(View.GONE);
                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                            } else {
                                ivLogo.setVisibility(View.GONE);
                            }
                            DecimalFormat df = new DecimalFormat("###.#");
                            tveventname.setText(_facilitydetailitem.get(0).getFacilityName());
                            //tveventaddres.setText(_facilitydetailitem.get(0).getCity() + ", " + _facilitydetailitem.get(0).getState() + " - " + df.format(Double.parseDouble(_facilitydetailitem.get(0).getDistance().replaceAll("[^\\d-]", "")))+" "+_facilitydetailitem.get(0).getDistance().replaceAll("[0-9]","").replace(".","").trim());
                            tveventaddres.setText(_facilitydetailitem.get(0).getCity() + ", " + _facilitydetailitem.get(0).getState() + " - " + _facilitydetailitem.get(0).getDistance());
                            if (_facilitydetailitem.get(0).getAdditionalInfo() != null && _facilitydetailitem.get(0).getAdditionalInfo().length() > 0) {
                                tvadditionalinfo.setText(_facilitydetailitem.get(0).getAdditionalInfo());
                            } else {
                                tvadditionalinfo.setText("NA");
                            }


                            if (_response.getUpcomingEvent() != null && _response.getUpcomingEvent().size() > 0) {
                                _upcomingevent = _response.getUpcomingEvent();
                                if (llupcomingevent != null) {
                                    if (llupcomingevent.getChildCount() > 0) {
                                        llupcomingevent.removeAllViews();
                                    }
                                    for (int i = 0; i < _upcomingevent.size(); i++) {
                                        inflater = LayoutInflater.from(FacilityEventDetailActivity.this);
                                        _view = inflater.inflate(R.layout.custom_upcoming_event, null);
                                        tvracename = (TextView) _view.findViewById(R.id.tvracename);
                                        tvid = (TextView) _view.findViewById(R.id.tvid);
                                        ivflag = (ImageView) _view.findViewById(R.id.ivflag);
                                        tvtype = (TextView) _view.findViewById(R.id.tvtype);
                                        rlroot = (RelativeLayout) _view.findViewById(R.id.rlroot);
                                        tvupcomingcalendardate = (TextView) _view.findViewById(R.id.tvupcomingcalendardate);

                                        if (i == 1) {
                                            llupcomingevent.setBackgroundColor(getResources().getColor(android.R.color.white));
                                        }

                                        if (i > 1) {
                                            if (i % 2 != 0) {
                                                llupcomingevent.setBackgroundColor(getResources().getColor(R.color.listcolor));
                                            }
                                        }
                                        rlroot.setTag(_upcomingevent.get(i).getID());
                                        tvracename.setText(_upcomingevent.get(i).getEventName());
                                        tvtype.setText(_upcomingevent.get(i).getType());
                                        tvid.setText(_upcomingevent.get(i).getID());
                                        tvupcomingcalendardate.setText(CommonUtility.convertDateFormat(_upcomingevent.get(i).getStartDate(), "yyyy-MM-dd", "MMM") + "\n" + CommonUtility.convertDateFormat(_upcomingevent.get(i).getStartDate(), "yyyy-MM-dd", "d"));
                                        if (_upcomingevent.get(i).getType().equals("Event")) {
                                            ivflag.setImageResource(R.mipmap.race_adapter_flag);
                                        } else if (_upcomingevent.get(i).getType().equals("practiceday")) {
                                            ivflag.setImageResource(R.mipmap.listorganized);
                                        }
                                        llupcomingevent.addView(_view);
                                        rlroot.setOnClickListener(getOnClickListnerUpcomingDetail(rlroot));

                                    }

                                } else {
                                    if (llupcomingevent.getChildCount() > 0) {
                                        llupcomingevent.removeAllViews();

                                        TextView _textView = new TextView(this);
                                        _textView.setText("No Near By Event Found..");
                                        _textView.setGravity(Gravity.CENTER);
                                        llupcomingevent.addView(_textView);
                                    }

                                }

                            } else {
                                TextView _textView = new TextView(this);
                                _textView.setText("No Near By Event Found..");
                                _textView.setGravity(Gravity.CENTER);
                                llupcomingevent.addView(_textView);
                            }

                            if (_facilitydetailitem.get(0).getEquipmentType() != null && _facilitydetailitem.get(0).getEquipmentType().size() > 0) {
                                _equipmentlist = _facilitydetailitem.get(0).getEquipmentType();
                                if (llequipmentcontainer != null) {
                                    if (llequipmentcontainer.getChildCount() > 0) {
                                        llequipmentcontainer.removeAllViews();
                                    }
                                    for (int i = 0; i < _equipmentlist.size(); i++) {
                                        inflater = LayoutInflater.from(FacilityEventDetailActivity.this);
                                        _view = inflater.inflate(R.layout.equipment_container, null);
                                        tvequipmentname = (TextView) _view.findViewById(R.id.tvequipmentname);
                                        ivequipmenttype = (ImageView) _view.findViewById(R.id.ivequipmenttype);
                                        tvequipmentname.setText(_equipmentlist.get(i).getEquipmentName());
                                        if (_equipmentlist.get(i).getEquipmentImage() != null && _equipmentlist.get(i).getEquipmentImage().length() > 0) {
                                            //loadingSpinner.setVisibility(View.VISIBLE);
                                            Picasso.with(this)
                                                    .load(_equipmentlist.get(i).getEquipmentImage())
                                                    .into(ivequipmenttype, new Callback() {
                                                        @Override
                                                        public void onSuccess() {
                                                            //loadingSpinner.setVisibility(View.GONE);
                                                        }

                                                        @Override
                                                        public void onError() {

                                                        }
                                                    });
                                        } else {
                                            ivequipmenttype.setVisibility(View.GONE);
                                        }
                                        llequipmentcontainer.addView(_view);
                                    }
                                } else {
                                    if (llequipmentcontainer.getChildCount() > 0) {
                                        llequipmentcontainer.removeAllViews();

                                        TextView _textView = new TextView(this);
                                        _textView.setText("No Equipment Found..");
                                        _textView.setGravity(Gravity.CENTER);
                                        llequipmentcontainer.addView(_textView);
                                    }

                                }
                            } else {
                                _mainApplication.messageManager.DisplayToastMessage("No Equipment");
                            }

                            if (_facilitydetailitem.get(0).getDisciplineType() != null && _facilitydetailitem.get(0).getDisciplineType().size() > 0) {
                                _discipline = new ArrayList<>();
                                for (int i = 0; i < _facilitydetailitem.get(0).getDisciplineType().size(); i++) {
                                    _discipline.add(_facilitydetailitem.get(0).getDisciplineType().get(i));
                                }
                                tvdiscipline.setText(_discipline.toString().replace("[", "").replace("]", ""));
                            } else {
                                tvdiscipline.setText("NA");
                            }

                            if (_facilitydetailitem.get(0).getWebsiteURL() != null && _facilitydetailitem.get(0).getWebsiteURL().length() > 0) {
                                llweb.setBackgroundResource(R.drawable.rounded_cornerbutton);
                            } else {
                                llweb.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                            }
                            if (_facilitydetailitem.get(0).getFacebookPageURL() != null && _facilitydetailitem.get(0).getFacebookPageURL().length() > 0) {
                                llfb.setBackgroundResource(R.drawable.rounded_cornerbutton);
                            } else {
                                llfb.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                            }
                            if (_facilitydetailitem.get(0).getPhone() != null && _facilitydetailitem.get(0).getPhone().length() > 0) {
                                llcall.setBackgroundResource(R.drawable.rounded_cornerbutton);
                            } else {
                                llcall.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                            }
                            if (_facilitydetailitem.get(0).getEmail() != null && _facilitydetailitem.get(0).getEmail().length() > 0) {
                                llmsg.setBackgroundResource(R.drawable.rounded_cornerbutton);
                            } else {
                                llmsg.setBackgroundResource(R.drawable.rounded_cornerbuttongray);
                            }
                            if (_facilitydetailitem.get(0).getAlreadyAdded() == 1) {
                                ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                                lladdcalendar.setClickable(false);
                            } else {
                                ivaddcalendar.setImageResource(R.mipmap.addevent);
                            }

                        } else {
                            _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                        }

                    }

                }
            } else {
                JSONObject _response = (JSONObject) _object[0];
                try {
                    if (_response.getInt("Status") == 1) {
                        lladdcalendar.setClickable(false);
                        ivaddcalendar.setImageResource(R.mipmap.approvedevent);
                        _mainApplication.messageManager.displayCustomToastWithLayout(_response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    @Override
    public void onItemClick(View view, int position) {

        //Log.i("Anish", _imagePaths.get(position).get("TYPE"));
        //Log.i("Anish", _imagePaths.get(position).get("ITEM"));
        _imagePaths.get(position).get("TYPE");
        _imagePaths.get(position).get("ITEM");

        if (_imagePaths.get(position).get("TYPE").equals("VIDEO")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + getYoutubeVideoIdFromUrl(_imagePaths.get(position).get("ITEM"))));
            intent.putExtra("VIDEO_ID", getYoutubeVideoIdFromUrl(_imagePaths.get(position).get("ITEM")));
            startActivity(intent);
        } else {
            Intent _sendImage = new Intent(this, ImageViewFullViewActivity.class);
            _sendImage.putExtra("IMAGE", _imagePaths.get(position).get("ITEM"));
            startActivity(_sendImage);
        }
    }

    public static String getYoutubeVideoIdFromUrl(String inUrl) {
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1);
        }
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(inUrl);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }

    @SuppressLint("ClickableViewAccessibility")
    public void enabelingDesiablingParentAndChildScrooll() {
        parentscrollview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                childscrollview.getParent().requestDisallowInterceptTouchEvent(false);
                //  We will have to follow above for all scrollable contents
                return false;
            }
        });


        childscrollview.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View p_v, MotionEvent p_event) {
                // this will disallow the touch request for parent scroll on touch of child view
                p_v.getParent().requestDisallowInterceptTouchEvent(true);
                return false;
            }
        });
    }

    View.OnClickListener getOnClickListnerUpcomingDetail(final View layout) {
        return new View.OnClickListener() {
            public void onClick(View v) {

                // Log.i("tanvi", layout.getTag().toString());
                _mainApplication.settingManager.setSetting(TypeFactory.SettingType.UPCOMINFEVENTID, layout.getTag().toString());
                _intent = new Intent(FacilityEventDetailActivity.this, UpcomingDetailActivity.class);
                _intent.putExtra("LATT", _latitude);
                _intent.putExtra("LONGG", _longitude);
                _intent.putExtra("TYPE", type);
                startActivity(_intent);
            }
        };
    }

    public boolean isPermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED) {
                // Log.v("TAG", "Permission is granted");
                return true;
            } else {

                //Log.v("TAG", "Permission is revoked");
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            //Log.v("TAG", "Permission is granted");
            return true;
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {

            case 1: {

                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Toast.makeText(getApplicationContext(), "Permission granted", Toast.LENGTH_SHORT).show();
                    _intent = new Intent(Intent.ACTION_CALL);
                    _intent.setData(Uri.parse("tel:" + _facilitydetailitem.get(0).getPhone()));//change the number
                    startActivity(_intent);
                } else {
                    //Toast.makeText(getApplicationContext(), "Permission denied", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
