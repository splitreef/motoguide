package com.cynexis.motoguide;

import android.content.Intent;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.settingactivities.EquipmentTypeActivity;
import com.cynexis.motoguide.settingactivities.FactoryContingency;
import com.cynexis.motoguide.startingactivities.LoginChooserActivity;

public class SplashScreenActivity extends AppCompatActivity {

    MainApplication _mainApplication;
    private String unique_device_id = "00";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = ((MainApplication) getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        initalizeActivity();

    }

    private void initalizeActivity() {
        CountDownTimer _countDownTimer = new CountDownTimer(3000, 10) {
            @Override
            public void onTick(long millisUntilFinished) {
            }

            @Override
            public void onFinish() {

                /*if(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.LOGIN_SUCCESS) != null && _mainApplication.settingManager.getSetting(TypeFactory.SettingType.LOGIN_SUCCESS).equals("YES"))
                {
                    Intent intent = new Intent(SplashScreenActivity.this,DashBoardActivity.class);
                    startActivity(intent);
                    finish();
                }
                else
                {
                    Intent intent = new Intent(SplashScreenActivity.this,LoginChooserActivity.class);
                    startActivity(intent);
                    finish();
                }*/
                try {
                    unique_device_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                _mainApplication.settingManager.setSetting(TypeFactory.SettingType.USER_ID, "0");
                _mainApplication.settingManager.setSetting(TypeFactory.SettingType.DEVICE_ID, unique_device_id);

                Intent intent = new Intent(SplashScreenActivity.this, DashBoardActivity.class);
                startActivity(intent);
                finish();

            }

        };
        _countDownTimer.start();
    }
}
