package com.cynexis.motoguide.manager;

import android.content.Context;
import android.os.Process;


import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {
    private final Context myContext;
    private final Class<?> myActivityClass;

    public ExceptionHandler(Context context, Class<?> c) {
        myContext = context;
        myActivityClass = c;
    }

    public void uncaughtException(Thread thread, Throwable exception) {
        StringWriter stackTrace = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTrace));
        System.err.println(stackTrace);

       // CommonUtility.showAlert(myContext,"",stackTrace.toString());
        //Intent intent = new Intent(myContext, myActivityClass);
        //intent.addFlags( Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK );
        //intent.putExtra("SUCCESS", stackTrace.toString());
        //myContext.startActivity(intent);

        Process.killProcess(Process.myPid());
        System.exit(0);
    }
}