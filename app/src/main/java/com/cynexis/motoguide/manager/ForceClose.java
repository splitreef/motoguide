package com.cynexis.motoguide.manager;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;


public class ForceClose extends AppCompatActivity {
    /** Called when the activity is first created. */
   TextView txt;
   private String SUCCESS = null;
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle _extra = this.getIntent().getExtras();
        if(_extra != null)
		{
			SUCCESS = _extra.getString("SUCCESS");
		}
        Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(this, ForceClose.class));

        TextView txt = new TextView(this);
        txt.setText(SUCCESS);
        // Your mechanism is ready now.. In this activity from anywhere if you get force close error it will be redirected to the CrashActivity.
    }
}