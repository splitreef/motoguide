package com.cynexis.motoguide.manager;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.cynexis.motoguide.MainApplication;


public class NetworkManager{
	
	private Context context;
	ConnectivityManager cManager;
	MainApplication _mainapplication;

	public NetworkManager(Context _context, MainApplication mainapplication)
	{
		this.context = _context;
		cManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		_mainapplication = mainapplication;
	}
	
	@SuppressWarnings("static-access")
	public Boolean CheckNetworkConnection()
	{
		NetworkInfo activeNetworkInfo = cManager.getActiveNetworkInfo();
		if (activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnectedOrConnecting())
		{
			return true;
		}
		return false;

	}
	
}
