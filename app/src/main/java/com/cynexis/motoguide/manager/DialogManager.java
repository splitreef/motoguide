package com.cynexis.motoguide.manager;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.ContextThemeWrapper;

import androidx.appcompat.app.AlertDialog;

import com.cynexis.motoguide.R;


public class DialogManager {
	public Context context;
	private ProgressDialog progressDialog;
	ProgressDialog _dialog;

	//AlertDialog alertDialog;
	public DialogManager(Context _context)
	{		
		this.context = _context;
		//alertDialog = new AlertDialog.Builder(this.context).create();
	}
	
	@SuppressWarnings("deprecation")
	public void DisplayAlertDialog(Context ctx , String _title, String _message)
	{

		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, R.style.myDialog));
		if(_title.length() ==0)
		{
			alertDialogBuilder.setTitle("MotoGuide");
		}
		else
		{
			alertDialogBuilder.setTitle(_title);
		}
		alertDialogBuilder.setMessage(_message);
		// set dialog message
		alertDialogBuilder.setCancelable(false);
		alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		});
		alertDialogBuilder.show();
	}
	
	public void displayProgressDialog(Context _ctx , String _message)
	{
		if(_dialog == null)
		{
			_dialog = new ProgressDialog(_ctx);
			_dialog.setIndeterminate(true);
			_dialog.setCancelable(false);
			_dialog.setMessage(_message);
			if(!_dialog.isShowing())
			{
				_dialog.show();
			}
		}

	}
	
	public void dismissProgressDialog()
	{
		if(_dialog != null)
		{
			if(_dialog.isShowing())
			{
				_dialog.dismiss();
				_dialog = null;
			}

		}
	}
	
}
