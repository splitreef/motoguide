package com.cynexis.motoguide.manager;

import android.content.Context;
import android.content.SharedPreferences;

import com.cynexis.motoguide.network.TypeFactory;

public class SettingManager {
		
	private static final String PREFS_NAME = "PREF_MOTOGUIDE";
	private Context context;
	
	public SettingManager(Context _context)
	{
		this.context = _context;
	}
	
	public String getSetting(TypeFactory.SettingType _settingType) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
		String _value = null;
		_value = settings.getString(_settingType.toString(), _value);
		return _value;
	}
	
	public void setSetting(TypeFactory.SettingType _settingType, String _value) {
		SharedPreferences settings = context.getSharedPreferences(PREFS_NAME, 0);
	    SharedPreferences.Editor editor = settings.edit();
	    editor.putString(_settingType.toString(), _value);
	    editor.commit();
	}
	
}
