package com.cynexis.motoguide.manager;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
//import android.support.design.widget.Snackbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.google.android.material.snackbar.Snackbar;


public class MessageManager {
	
	private Context context;
	
	public MessageManager(Context _context)
	{		
		this.context = _context;		
	}
	
	public void DisplayToastMessageAtCenter(String _message)
	{
		Toast toast = Toast.makeText(context, _message, Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}

	public void DisplayToastMessage(String _message)
	{
		Toast.makeText(context, _message, Toast.LENGTH_LONG).show();
	}

	public void noInternetConnectionMessage(View _view)
	{
		Snackbar.make(_view, CommonUtility._INTERNET_CONNECTION, Snackbar.LENGTH_LONG).setAction("Setting", new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(Settings.ACTION_SETTINGS);
				intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				context.startActivity(intent);
			}
		}).show();
	}


	public void displayCustomToastWithLayout(String _message)
	{
		//Creating the LayoutInflater instance
		LayoutInflater li = LayoutInflater.from(context);
		//Getting the View object as defined in the customtoast.xml file
		View layout = li.inflate(R.layout.custom_toast, null);
		TextView _text =  (TextView) layout.findViewById(R.id.tvtoasttext);
		_text.setText(_message);
		//Creating the Toast object
		Toast toast = new Toast(context);
		toast.setDuration(Toast.LENGTH_LONG);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.setView(layout);//setting the view of custom toast layout
		toast.show();
	}

}
