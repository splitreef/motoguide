package com.cynexis.motoguide.startingactivities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cynexis.motoguide.DashBoardActivity;
import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.login.LoginResponse;
import com.cynexis.motoguide.startingactivities.permission.GetLatitudeAndLongitude;
import com.cynexis.motoguide.startingactivities.permission.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.material.snackbar.Snackbar;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements WebserviceStartEndEventTracker, GetLatitudeAndLongitude {

    @BindView(R.id.etemail)
    EditText etemail;
    @BindView(R.id.etpassword)
    ShowHidePasswordEditText etpassword;
    @BindView(R.id.btnlogin)
    Button btnlogin;
    @BindView(R.id.tvforgotpassword)
    TextView tvforgotpassword;
    @BindView(R.id.cbrememberme)
    CheckBox cbrememberme;
    @BindView(R.id.btnlinkcontact)
    TextView btnlinkcontact;
    Intent _intent;
    LocationService _locationService;
    public static final int PERMISSION_REQUEST_CODE = 120;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    MainApplication _mainApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        checkPermissionForMarshmallow();
        etpassword.setTransformationMethod(new PasswordTransformationMethod());

        etemail.setText(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.REMEMBER_EMAIL));
        etpassword.setText(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.REMEMBER_PASSWORD));
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.REMEMBER_EMAIL) != null && _mainApplication.settingManager.getSetting(TypeFactory.SettingType.REMEMBER_EMAIL).length() > 0) {
            cbrememberme.setChecked(true);
        } else {
            cbrememberme.setChecked(false);
        }
    }

    @OnClick({R.id.btnlogin, R.id.tvforgotpassword, R.id.btnlinkcontact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnlogin:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                }
                if (checkPermission()) {
                    if (_mainApplication.networkManager.CheckNetworkConnection()) {

                        if (etemail.getText().toString().trim().equals("")) {
                            _mainApplication.messageManager.DisplayToastMessage("Please enter email");
                            etemail.requestFocus();
                            return;
                        } else {
                            if (!Patterns.EMAIL_ADDRESS.matcher(etemail.getText().toString()).matches()) {
                                //scrollView.scrollTo(0, etemail.getBottom());
                                _mainApplication.messageManager.DisplayToastMessage("Please enter valid email");
                                etemail.requestFocus();
                                return;
                            }
                        }
                        if (etpassword.getText().toString().trim().equals("")) {
                            //scrollView.scrollTo(0, etpassword.getBottom());
                            _mainApplication.messageManager.DisplayToastMessage("Please enter password");
                            etpassword.requestFocus();
                            return;
                        }
                        showProgressDialog();
                        List<DataEntity> _dataToPost = new ArrayList<>();
                        _dataToPost.add(new DataEntity("Email", etemail.getText().toString()));
                        _dataToPost.add(new DataEntity("Password", etpassword.getText().toString()));
                        AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.LOGIN, _dataToPost, this);
                    } else {
                        _mainApplication.messageManager.noInternetConnectionMessage(btnlogin);
                    }
                } else {
                    requestPermission();
                }
                break;
            case R.id.tvforgotpassword:
                _intent = new Intent(this, ForgotPasswordActivity.class);
                startActivity(_intent);
                break;
            case R.id.btnlinkcontact:
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", " admin@motoguideapp.com", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Need Help");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, "admin@motoguideapp.com"));
                break;
        }
    }

    private void checkPermissionForMarshmallow() {
       /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);
                }
            } else {
                if (checkGooglePlayServices()) {
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }*/

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);
                }

            } else {
                if (checkGooglePlayServices()) {
                    // isButtonPressedToRequestForLocation = false;
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted) {
                        Snackbar.make(btnlogin, "Permission Granted, Thanks.", Snackbar.LENGTH_LONG).show();
                        if (_locationService == null) {
                            _locationService = new LocationService(this, this);
                            _locationService.makeConnectionIfRequired();
                        }

                        if (etemail.getText().toString().trim().equals("")) {
                            _mainApplication.messageManager.DisplayToastMessage("Please enter email");
                            etemail.requestFocus();
                            return;
                        } else {
                            if (!Patterns.EMAIL_ADDRESS.matcher(etemail.getText().toString()).matches()) {
                                //scrollView.scrollTo(0, etemail.getBottom());
                                _mainApplication.messageManager.DisplayToastMessage("Please enter valid email");
                                etemail.requestFocus();
                                return;
                            }
                        }
                        if (etpassword.getText().toString().trim().equals("")) {
                            //scrollView.scrollTo(0, etpassword.getBottom());
                            _mainApplication.messageManager.DisplayToastMessage("Please enter password");
                            etpassword.requestFocus();
                            return;
                        }
                        showProgressDialog();
                        List<DataEntity> _dataToPost = new ArrayList<>();
                        _dataToPost.add(new DataEntity("Email", etemail.getText().toString()));
                        _dataToPost.add(new DataEntity("Password", etpassword.getText().toString()));
                        AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.LOGIN, _dataToPost, this);
                    } else {
                        Snackbar.make(btnlogin, "Permission Denied. Please allow.", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        Log.d("dataMayuri",""+ "hide_progress _dilaog"+_object[0]);

        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            LoginResponse _dataRespnse = (LoginResponse) _object[0];
            if (_dataRespnse != null) {
                if (_dataRespnse.getStatus() == 1) {
                    Log.d("dataMayuri",""+ _dataRespnse.getStatus());
                    Log.d("dataMayuri",""+ _dataRespnse.getData());
                    Log.d("dataMayuri",""+ _dataRespnse.getData().getFirstName());
                   /* Log.d("dataMayuri",""+ _dataRespnse.getData().getID());*/
                    if (cbrememberme.isChecked()) {
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.REMEMBER_EMAIL, etemail.getText().toString().trim());
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.REMEMBER_PASSWORD, etpassword.getText().toString());
                    } else {
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.REMEMBER_EMAIL, "");
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.REMEMBER_PASSWORD, "");
                    }
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EMAIL, etemail.getText().toString().trim());
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.PASSWORD, etpassword.getText().toString());
                    _mainApplication.messageManager.DisplayToastMessageAtCenter(_dataRespnse.getMessage());
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.USER_ID, _dataRespnse.getData().getID());
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.FIRSTNAME, _dataRespnse.getData().getFirstName());
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.LASTNAME, _dataRespnse.getData().getLastName());
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.LOGIN_SUCCESS, "YES");
                    ActivityCompat.finishAffinity(this);
                    _intent = new Intent(this, DashBoardActivity.class);
                    startActivity(_intent);
                    finish();
                } else {
                    _mainApplication.messageManager.DisplayToastMessage(_dataRespnse.getMessage());
                }

            }
        } else {
            _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
        }
    }

    @Override
    public void getLatLong(String locationBY, String latitude, String longitude) {

    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
