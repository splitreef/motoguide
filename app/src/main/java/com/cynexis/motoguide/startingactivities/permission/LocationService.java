package com.cynexis.motoguide.startingactivities.permission;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


/**
 * Created by Admin on 14-09-2016.
 */
public class LocationService implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

   /* Activity activity;
    private GoogleApiClient mGoogleApiClient;
    GetLatitudeAndLongitude _callBackInterface;
    private LocationRequest mLocationRequest;
    private Location mLocation;

    public LocationService(Activity _act, GetLatitudeAndLongitude callBackInterface) {
        this.activity = _act;
        _callBackInterface = callBackInterface;
        buildGoogleApiClient();
    }

    @Override
    public void onConnected(Bundle bundle) {

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation != null)
        {
           // Log.i("anish lat",String.valueOf(mLocation.getLatitude()));
           // Log.i("anish lomg",String.valueOf(mLocation.getLongitude()));
            _callBackInterface.getLatLong("Main", String.valueOf(mLocation.getLatitude()), String.valueOf(mLocation.getLongitude()));
        }
        else
        {
            LocationManagerConsumer locationManagerCheck = new LocationManagerConsumer(this.activity);
            Location location = null;

            if(locationManagerCheck.isLocationServiceAvailable())
            {

                if (locationManagerCheck.getProviderType() == 1)
                {
                    location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                else if(locationManagerCheck.getProviderType() == 2)
                {
                    location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }

                if(location != null)
                {
                    _callBackInterface.getLatLong("FROM", String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()));
                }
                else
                {
                    _callBackInterface.getLatLong("FROM", String.valueOf(0.0), String.valueOf(0.0));
                }
            }
            else
            {
                locationManagerCheck .createLocationServiceError(this.activity);
            }
        }
    }

    *//**
     * Creating google api client object
     * *//*
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this.activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    *//*@Override
    public void onLocationChanged(Location location) {
        _callBackInterface.getLatLong("Change",String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
       // Log.i("anish lat location changed",String.valueOf(location.getLatitude()));
       // Log.i("anish lomg location changed",String.valueOf(location.getLongitude()));
    }
*//*
    public void connectClient()
    {
        mGoogleApiClient.connect();
    }

    public void disconnectClient()
    {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void makeConnectionIfRequired()
    {
        if (!mGoogleApiClient.isConnecting() &&
                !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            *//*mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLocation != null)
            {
                // Log.i("anish lat",String.valueOf(mLocation.getLatitude()));
                // Log.i("anish lomg",String.valueOf(mLocation.getLongitude()));
                _callBackInterface.getLatLong("Main",String.valueOf(mLocation.getLatitude()),String.valueOf(mLocation.getLongitude()));
            }
            else
            {
                LocationManagerConsumer locationManagerCheck = new LocationManagerConsumer(this.activity);
                Location location = null;

                if(locationManagerCheck.isLocationServiceAvailable()){

                    if (locationManagerCheck.getProviderType() == 1)
                    {
                        location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                    else if(locationManagerCheck.getProviderType() == 2)
                    {
                        location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }else{
                    locationManagerCheck .createLocationServiceError(this.activity);
                }
                if(location != null)
                {
                    _callBackInterface.getLatLong("FROM",String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
                }
                else
                {
                    _callBackInterface.getLatLong("FROM","0.0","0.0");
                }
            }*//*
        }
    }


    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }*/

    Activity activity;
    private GoogleApiClient mGoogleApiClient;
    GetLatitudeAndLongitude _callBackInterface;
    private LocationRequest mLocationRequest;
    private Location mLocation;

    public LocationService(Activity _act,GetLatitudeAndLongitude callBackInterface) {
        this.activity = _act;
        _callBackInterface = callBackInterface;
        buildGoogleApiClient();
    }

    @Override
    public void onConnected(Bundle bundle) {

        /*mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(5000);
        mLocationRequest.setFastestInterval(3000);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);*/
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLocation != null)
        {
            // Log.i("anish lat",String.valueOf(mLocation.getLatitude()));
            // Log.i("anish lomg",String.valueOf(mLocation.getLongitude()));
            _callBackInterface.getLatLong("Main",String.valueOf(mLocation.getLatitude()),String.valueOf(mLocation.getLongitude()));
        }
        else
        {
            LocationManagerConsumer locationManagerCheck = new LocationManagerConsumer(this.activity);
            Location location = null;

            if(locationManagerCheck.isLocationServiceAvailable())
            {

                if (locationManagerCheck.getProviderType() == 1)
                {
                    location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                }
                else if(locationManagerCheck.getProviderType() == 2)
                {
                    location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }

                if(location != null)
                {
                    _callBackInterface.getLatLong("FROM",String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
                }
                else
                {
                    _callBackInterface.getLatLong("FROM",String.valueOf(0.0),String.valueOf(0.0));
                }
            }
            else
            {
                locationManagerCheck .createLocationServiceError(this.activity);
            }
        }
    }

    /**
     * Creating google api client object
     * */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this.activity)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
        if (!mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.connect();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    /*@Override
    public void onLocationChanged(Location location) {
        _callBackInterface.getLatLong("Change",String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
       // Log.i("anish lat location changed",String.valueOf(location.getLatitude()));
       // Log.i("anish lomg location changed",String.valueOf(location.getLongitude()));
    }
*/
    public void connectClient()
    {
        mGoogleApiClient.connect();
    }

    public void disconnectClient()
    {
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void makeConnectionIfRequired()
    {
        if (!mGoogleApiClient.isConnecting() &&
                !mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
            /*mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLocation != null)
            {
                // Log.i("anish lat",String.valueOf(mLocation.getLatitude()));
                // Log.i("anish lomg",String.valueOf(mLocation.getLongitude()));
                _callBackInterface.getLatLong("Main",String.valueOf(mLocation.getLatitude()),String.valueOf(mLocation.getLongitude()));
            }
            else
            {
                LocationManagerConsumer locationManagerCheck = new LocationManagerConsumer(this.activity);
                Location location = null;

                if(locationManagerCheck.isLocationServiceAvailable()){

                    if (locationManagerCheck.getProviderType() == 1)
                    {
                        location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                    else if(locationManagerCheck.getProviderType() == 2)
                    {
                        location = locationManagerCheck.locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                }else{
                    locationManagerCheck .createLocationServiceError(this.activity);
                }
                if(location != null)
                {
                    _callBackInterface.getLatLong("FROM",String.valueOf(location.getLatitude()),String.valueOf(location.getLongitude()));
                }
                else
                {
                    _callBackInterface.getLatLong("FROM","0.0","0.0");
                }
            }*/
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.i("Anish1",provider);
    }

    @Override
    public void onProviderEnabled(String provider) {

        Log.i("Anish2",provider);

    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.i("Anishh3",provider);
    }

}
