package com.cynexis.motoguide.startingactivities.permission;

/**
 * Created by Admin on 14-09-2016.
 */
public interface GetLatitudeAndLongitude {

    public void getLatLong(String locationBY, String latitude, String longitude);
}
