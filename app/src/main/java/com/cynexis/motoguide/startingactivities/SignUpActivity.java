package com.cynexis.motoguide.startingactivities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.startingactivities.permission.GetLatitudeAndLongitude;
import com.cynexis.motoguide.startingactivities.permission.LocationService;
import com.cynexis.motoguide.settingactivities.TypeChooserActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.material.snackbar.Snackbar;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements WebserviceStartEndEventTracker,GetLatitudeAndLongitude {

    @BindView(R.id.etfirstname)
    EditText etfirstname;
    @BindView(R.id.etlastname)
    EditText etlastname;
    @BindView(R.id.etaddress)
    EditText etaddress;
    @BindView(R.id.etcity)
    EditText etcity;
    @BindView(R.id.spstate)
    Spinner spstate;
    @BindView(R.id.etzip)
    EditText etzip;
    @BindView(R.id.etemail)
    EditText etemail;
    @BindView(R.id.etphone)
    EditText etphone;
    @BindView(R.id.etpassword)
    ShowHidePasswordEditText etpassword;
    @BindView(R.id.btnregister)
    Button btnregister;

    ArrayList<String> _Stateitem,_StateID;
    MainApplication _mainApplication;
    String _latitude = "0.0",_longitude = "0.0";
    LocationService _locationService;
    public static final int PERMISSION_REQUEST_CODE = 120;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    Intent _intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        etpassword.setTransformationMethod(new PasswordTransformationMethod());
        checkPermissionForMarshmallow();
        bindControls();
    }

    private void bindControls() {
        if(_mainApplication.networkManager.CheckNetworkConnection())
        {
            showProgressDialog();
            List<DataEntity> _dataToPost =  new ArrayList<>();
            _dataToPost.add(new DataEntity("",""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.STATE,_dataToPost,this);
        }
        else
        {
            _mainApplication.messageManager.noInternetConnectionMessage(btnregister);
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(getCurrentFocus().getWindowToken() != null)
        {
            imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
        return true;
    }

    private void checkPermissionForMarshmallow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);

                }

            } else {
                if (checkGooglePlayServices()) {
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }
    }
    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }
    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }
    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            /*
            * google play services is missing or update is required
			*  return code could be
			* SUCCESS,
			* SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
			* SERVICE_DISABLED, SERVICE_INVALID.
			*/
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;

    }

    @OnClick(R.id.btnregister)
    public void onViewClicked() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if(_mainApplication.networkManager.CheckNetworkConnection())
        {
            if (etfirstname.getText().toString().trim().equals(""))
           {
                etfirstname.setError("Please enter first name");
                etfirstname.requestFocus();
                return;
           }
            if (etlastname.getText().toString().trim().equals(""))
            {

                etlastname.setError("Please enter last name");
                etlastname.requestFocus();
                return;
            }
            if (etaddress.getText().toString().trim().equals(""))
            {
                etaddress.requestFocus();
                etaddress.setError("Please enter address");
                return;
            }
            if (etcity.getText().toString().trim().equals(""))
            {
                etcity.setError("Please enter city");
                etcity.requestFocus();
                return;
            }
            if(spstate.getSelectedItemPosition()==0)
            {
                _mainApplication.messageManager.DisplayToastMessage("Please enter State");
                return;
            }
            if (etzip.getText().toString().trim().equals(""))
            {
                etzip.setError("Please enter zip");
                etzip.requestFocus();
                return;
            }
            if(etzip.getText().toString().trim().length() < 5)
            {
                etzip.setError("Please enter valid zip");
                etzip.requestFocus();
                return;
            }
            if (etemail.getText().toString().trim().equals(""))
            {
                etemail.setError("Please enter email");
                etemail.requestFocus();
                return;
            }

            if (!Patterns.EMAIL_ADDRESS.matcher(etemail.getText().toString()).matches())
            {
                etemail.requestFocus();
                etemail.setError("Please enter valid email id");
                return;
            }
           /* if (etphone.getText().toString().trim().equals(""))
            {
                etphone.setError("Please enter Phone");
                etphone.requestFocus();
                return;
            }*/
            if(etphone.getText().toString().trim().length() >0)
            {
                if(etphone.getText().toString().trim().length() < 10)
                {
                    etphone.setError("Please enter valid Phone number");
                    etphone.requestFocus();
                    return;
                }
            }

            if (etpassword.getText().toString().trim().equals("")) {
                etpassword.setError("Please enter password");
                etpassword.requestFocus();
                return;
            }
                //showProgressDialog();
                List<DataEntity> _dataToPost =  new ArrayList<>();
                _dataToPost.add(new DataEntity("FirstName",etfirstname.getText().toString()));
                _dataToPost.add(new DataEntity("LastName",etlastname.getText().toString()));
                _dataToPost.add(new DataEntity("Phone",etphone.getText().toString()));
                _dataToPost.add(new DataEntity("Email",etemail.getText().toString()));
                _dataToPost.add(new DataEntity("Password",etpassword.getText().toString()));
                _dataToPost.add(new DataEntity("Address",etaddress.getText().toString()));
                _dataToPost.add(new DataEntity("State",_Stateitem.get(spstate.getSelectedItemPosition())));
                _dataToPost.add(new DataEntity("City",etcity.getText().toString()));
                _dataToPost.add(new DataEntity("ZIP",etzip.getText().toString()));
                _dataToPost.add(new DataEntity("Latitude",_latitude));
                _dataToPost.add(new DataEntity("Longitude",_longitude));


                  _intent = new Intent(SignUpActivity.this,TypeChooserActivity.class);
                  _intent.putExtra("FirstName",etfirstname.getText().toString());
                  _intent.putExtra("LastName",etlastname.getText().toString());
                  _intent.putExtra("Phone",etphone.getText().toString());
                  _intent.putExtra("Email",etemail.getText().toString());
                  _intent.putExtra("Password",etpassword.getText().toString());
                  _intent.putExtra("Address",etaddress.getText().toString());
                  _intent.putExtra("State",_Stateitem.get(spstate.getSelectedItemPosition()));
                  _intent.putExtra("City",etcity.getText().toString());
                  _intent.putExtra("Zip",etzip.getText().toString());
                  _intent.putExtra("Latitude",_latitude);
                  _intent.putExtra("Longitude",_longitude);
                  startActivity(_intent);
        }
        else
        {
            _mainApplication.messageManager.noInternetConnectionMessage(btnregister);
        }

    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this,"Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        JSONObject _data = (JSONObject) _object[0];
        //Log.i("tanvi", String.valueOf(_data));
        if(AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.STATE))
        {
            if(_data != null)
            {
                try {
                    if(_data.getInt("Status") == 1)
                    {
                        _Stateitem = new ArrayList<>();
                        _Stateitem.add("STATE");
                        _StateID =  new ArrayList<>();
                        _StateID.add("");
                        for (int i = 0; i < _data.getJSONArray("data").length(); i++)
                        {
                            _StateID.add(i+1,_data.getJSONArray("data").getJSONObject(i).getString("state_id"));
                            _Stateitem.add(i+1,_data.getJSONArray("data").getJSONObject(i).getString("state_name"));
                        }
                        ArrayAdapter<String> spinnerArrayFOrState = new ArrayAdapter<String>(SignUpActivity.this, android.R.layout.simple_spinner_item, _Stateitem); //selected item will look like a spinner set from XML
                        spinnerArrayFOrState.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spstate.setAdapter(spinnerArrayFOrState);

                    }
                    else
                    {
                        _mainApplication.messageManager.DisplayToastMessage(_data.getString("data"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else
            {

                _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
            }

        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted)
                    {
                        Snackbar.make(btnregister, "Permission Granted, Thanks.", Snackbar.LENGTH_LONG).show();
                        if(_locationService == null)
                        {
                            _locationService = new LocationService(this,this);
                            _locationService.makeConnectionIfRequired();
                        }
                    }
                    else
                    {
                        Snackbar.make(btnregister, "Permission Denied. Please allow.", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }



    @Override
    public void getLatLong(String locationBY, String latitude, String longitude) {
        _latitude = latitude;
        _longitude = longitude;
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
