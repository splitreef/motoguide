
package com.cynexis.motoguide.startingactivities.permission;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EquipmentAndDisceplineSerializableClass implements Serializable
{

    @SerializedName("discipline")
    @Expose
    private List<Integer> discipline = null;
    @SerializedName("equipment")
    @Expose
    private List<Integer> equipment = null;
    private final static long serialVersionUID = -6502305456075185539L;

    public List<Integer> getDiscipline() {
        return discipline;
    }

    public void setDiscipline(List<Integer> discipline) {
        this.discipline = discipline;
    }

    public List<Integer> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<Integer> equipment) {
        this.equipment = equipment;
    }

}
