package com.cynexis.motoguide.startingactivities;

import android.content.Context;
import android.os.Bundle;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends AppCompatActivity implements WebserviceStartEndEventTracker {

    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etemail)
    EditText etemail;
    @BindView(R.id.btnsendemail)
    Button btnsendemail;
    MainApplication _mainApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        initalizeActivity();
    }

    private void initalizeActivity() {
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        ivmenu.setVisibility(View.GONE);
    }

    @OnClick({R.id.ivback, R.id.btnsendemail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.btnsendemail:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if (_mainApplication.networkManager.CheckNetworkConnection()) {
                    if (etemail.getText().toString().trim().equals("")) {
                        _mainApplication.messageManager.DisplayToastMessage("Please enter email");
                        etemail.requestFocus();
                        return;
                    } else {
                        if (!Patterns.EMAIL_ADDRESS.matcher(etemail.getText().toString()).matches()) {
                            _mainApplication.messageManager.DisplayToastMessage("Please enter valid email");
                            etemail.requestFocus();
                            return;
                        }
                    }
                    showProgressDialog();
                    List<DataEntity> _dataToPost = new ArrayList<>();
                    _dataToPost.add(new DataEntity("Email", etemail.getText().toString()));
                    AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.FORGOT_PASSWORD, _dataToPost, this);
                } else {
                    _mainApplication.messageManager.noInternetConnectionMessage(btnsendemail);
                }
                break;
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();

        ForgotPasswordResponse _response = (ForgotPasswordResponse) _object[0];
        _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
        if (_response.getStatus() == 1) {
            finish();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
