package com.cynexis.motoguide.startingactivities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.settingactivities.TypeChooserActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class LoginChooserActivity extends AppCompatActivity {


    @BindView(R.id.btnlogin)
    Button btnlogin;
    @BindView(R.id.btnregister)
    Button btnregister;
    @BindView(R.id.btnlinkcontact)
    TextView btnlinkcontact;
    Intent _intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_chooser);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.btnlogin, R.id.btnregister, R.id.btnlinkcontact})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnlogin:
                _intent = new Intent(LoginChooserActivity.this,LoginActivity.class);
                startActivity(_intent);
                break;
            case R.id.btnregister:
                _intent = new Intent(LoginChooserActivity.this,SignUpActivity.class);
                startActivity(_intent);
                break;
            case R.id.btnlinkcontact:
                Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto"," admin@motoguideapp.com", null));
                intent.putExtra(Intent.EXTRA_SUBJECT, "Need Help");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(Intent.createChooser(intent, "admin@motoguideapp.com"));
                break;
        }
    }
}
