package com.cynexis.motoguide.network.response.factorycontingencyresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Admin on 01-06-2017.
 */

public class FactoryConigencyData implements Serializable {
    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("FactoryName")
    @Expose
    private String factoryName;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getFactoryName() {
        return factoryName;
    }

    public void setFactoryName(String factoryName) {
        this.factoryName = factoryName;
    }
}
