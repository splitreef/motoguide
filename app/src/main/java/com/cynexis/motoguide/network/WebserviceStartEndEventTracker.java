package com.cynexis.motoguide.network;

/**
 * Created by Admin on 02-09-2016.
 */
public interface WebserviceStartEndEventTracker extends GettingActivityInstance{

    public void showProgressDialog();
    public void hideProgressDialog(Object... _object);
}
