
package com.cynexis.motoguide.network.response.searchresponse;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchResponse implements Parcelable
{

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<SearchData> data = null;
    public final static Creator<SearchResponse> CREATOR = new Creator<SearchResponse>() {


        @SuppressWarnings({
            "unchecked"
        })
        public SearchResponse createFromParcel(Parcel in) {
            SearchResponse instance = new SearchResponse();
            instance.status = ((Integer) in.readValue((Integer.class.getClassLoader())));
            in.readList(instance.data,(SearchData.class.getClassLoader()));
            return instance;
            //com.example.EventPracticeDetialData.class.getClassLoader()
        }

        public SearchResponse[] newArray(int size) {
            return (new SearchResponse[size]);
        }

    }
    ;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<SearchData> getData() {
        return data;
    }

    public void setData(List<SearchData> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeList(data);
    }

    public int describeContents() {
        return  0;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
