
package com.cynexis.motoguide.network.response.getsettingsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Equipment implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("EquipmentName")
    @Expose
    private String equipmentName;
    @SerializedName("EquipmentFile")
    @Expose
    private String equipmentFile;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentFile() {
        return equipmentFile;
    }

    public void setEquipmentFile(String equipmentFile) {
        this.equipmentFile = equipmentFile;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}
