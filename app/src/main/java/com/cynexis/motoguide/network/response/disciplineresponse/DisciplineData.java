
package com.cynexis.motoguide.network.response.disciplineresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class DisciplineData implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("DisciplineName")
    @Expose
    private String disciplineName;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName;
    }

}
