
package com.cynexis.motoguide.network.response.facilitydataresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FacilityDetailData implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;

   /* @SerializedName("Coverphoto")
    @Expose
    private String coverphoto;*/

    @SerializedName("Coverphoto")
    @Expose
    private List<String> coverphoto = null;


    @SerializedName("Logo")
    @Expose
    private String logo;
    @SerializedName("FacilityName")
    @Expose
    private String facilityName;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("WebsiteURL")
    @Expose
    private String websiteURL;
    @SerializedName("FacebookPageURL")
    @Expose
    private String facebookPageURL;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("SanctionType")
    @Expose
    private List<String> sanctionType = null;
    @SerializedName("DisciplineType")
    @Expose
    private List<String> disciplineType = null;
    @SerializedName("EquipmentType")
    @Expose
    private List<EquipmentType> equipmentType = null;
    @SerializedName("AdditionalInfo")
    @Expose
    private String additionalInfo;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("already_added")
    @Expose
    private Integer alreadyAdded;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

   /* public String getCoverphoto() {
        return coverphoto;
    }

    public void setCoverphoto(String coverphoto) {
        this.coverphoto = coverphoto;
    }*/

    public List<String> getCoverphoto() {
        return coverphoto;
    }

    public void setCoverphoto(List<String> coverphoto) {
        this.coverphoto = coverphoto;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getFacilityName() {
        return facilityName;
    }

    public void setFacilityName(String facilityName) {
        this.facilityName = facilityName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getFacebookPageURL() {
        return facebookPageURL;
    }

    public void setFacebookPageURL(String facebookPageURL) {
        this.facebookPageURL = facebookPageURL;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getSanctionType() {
        return sanctionType;
    }

    public void setSanctionType(List<String> sanctionType) {
        this.sanctionType = sanctionType;
    }

    public List<String> getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(List<String> disciplineType) {
        this.disciplineType = disciplineType;
    }

    public List<EquipmentType> getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(List<EquipmentType> equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public Integer getAlreadyAdded() {
        return alreadyAdded;
    }

    public void setAlreadyAdded(Integer alreadyAdded) {
        this.alreadyAdded = alreadyAdded;
    }


}
