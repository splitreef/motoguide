
package com.cynexis.motoguide.network.response.facilitydataresponse;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FacilityDetailResponse implements Serializable {
    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<FacilityDetailData> data = null;
    @SerializedName("upcoming_event")
    @Expose
    private List<UpcomingEvent> upcomingEvent = null;
    @SerializedName("media")
    @Expose
    private Media media;
    @SerializedName("message")
    @Expose
    private String message;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<FacilityDetailData> getData() {
        return data;
    }

    public void setData(List<FacilityDetailData> data) {
        this.data = data;
    }

    public List<UpcomingEvent> getUpcomingEvent() {
        return upcomingEvent;
    }

    public void setUpcomingEvent(List<UpcomingEvent> upcomingEvent) {
        this.upcomingEvent = upcomingEvent;
    }

    public Media getMedia() {
        return media;
    }

    public void setMedia(Media media) {
        this.media = media;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
