package com.cynexis.motoguide.network;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.cynexis.motoguide.FacilityEventDetailActivity;
import com.cynexis.motoguide.Utility.AdvertiseResponse;
import com.cynexis.motoguide.network.response.disciplineresponse.DisciplineResponse;
import com.cynexis.motoguide.network.response.equipmentresponse.EquipmentResponse;
import com.cynexis.motoguide.network.response.eventpracticedetailresponse.EventDetailResponse;
import com.cynexis.motoguide.network.response.facilitydataresponse.FacilityDetailResponse;
import com.cynexis.motoguide.network.response.factorycontingencyresponse.FactoryContingencyResponse;
import com.cynexis.motoguide.network.response.getsettingsresponse.GetSettingsResopnse;
import com.cynexis.motoguide.network.response.login.LoginResponse;
import com.cynexis.motoguide.network.response.mycalendarresponse.MycalendarResponse;
import com.cynexis.motoguide.network.response.nearbyracesresponse.NearbyEventResponse;
import com.cynexis.motoguide.network.response.sanctionresponce.SanctionResponse;
import com.cynexis.motoguide.network.response.searchresponse.SearchResponse;
import com.cynexis.motoguide.network.response.signupresponse.SignUpResonse;
import com.cynexis.motoguide.network.response.wheretorideresponse.WheretoRideResponse;
import com.cynexis.motoguide.searchallraces.SearchActivity;
import com.cynexis.motoguide.startingactivities.ForgotPasswordResponse;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by Admin on 06-12-2016.
 */

public class AndroidFastNetworkingWebServiceCall {

    AndroidNetworking _networking;
    TypeFactory.RequestType _requestType;
    Gson gson;

    OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
            .connectTimeout(180, TimeUnit.SECONDS)
            .readTimeout(180, TimeUnit.SECONDS)
            .writeTimeout(180, TimeUnit.SECONDS)
            .build();

    private static AndroidFastNetworkingWebServiceCall singleton = new AndroidFastNetworkingWebServiceCall();

    private AndroidFastNetworkingWebServiceCall() {
        // TODO Auto-generated constructor stub
        singleton = this;
    }

    public static AndroidFastNetworkingWebServiceCall getInstance() {
        return singleton;
    }


    public void callServiceWithPOST(TypeFactory.RequestType _requestTypeAction, List<DataEntity> _dataEntityToPost, final WebserviceStartEndEventTracker eventTracker) {
        this._requestType = _requestTypeAction;
        ANRequest.PostRequestBuilder _builder = _networking.post(WebServicePath.GetSoapServicePath(_requestType));
        if (_dataEntityToPost != null && _dataEntityToPost.size() > 0) {
            for (final DataEntity _dataToPost : _dataEntityToPost) {
                // Here your room is available
                _builder.addBodyParameter(_dataToPost.getName(), _dataToPost.getValue());
                Log.i(_dataToPost.getName(), _dataToPost.getValue());
            }
        }
        _builder.setTag("POST");
        _builder.setPriority(Priority.HIGH);
        _builder.build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        Log.i("Anish", response.toString());
                        Gson gson = new Gson();
                        switch (_requestType) {
                            case LOGIN:
                                Log.d("dataMayuri",""+ "LOGIN");
                                Log.d("dataMayuri",""+ response.toString());

                                LoginResponse _loginResponseData = gson.fromJson(response.toString().trim(), LoginResponse.class);
                                eventTracker.hideProgressDialog(_loginResponseData);
                                break;
                            case SEARCH:
                                SearchResponse _searchResponse = gson.fromJson(response.toString(), SearchResponse.class);
                                eventTracker.hideProgressDialog(_searchResponse);
                                break;
                            case REGISTER:
                                SignUpResonse _singupResponse = gson.fromJson(response.toString(), SignUpResonse.class);
                                eventTracker.hideProgressDialog(_singupResponse);
                                break;
                            case NEAR_EVENTRACE:
                                NearbyEventResponse _neareventresponse = gson.fromJson(response.toString().trim(), NearbyEventResponse.class);
                                eventTracker.hideProgressDialog(_neareventresponse);
                                break;
                            case EVENT_DETAIL:
                                EventDetailResponse _eventdetailrespose = gson.fromJson(response.toString().trim(), EventDetailResponse.class);
                                eventTracker.hideProgressDialog(_eventdetailrespose);
                                break;
                            case TRACK_FACILITY:
                                WheretoRideResponse _rideresponse = gson.fromJson(response.toString().trim(), WheretoRideResponse.class);
                                eventTracker.hideProgressDialog(_rideresponse);
                                break;
                            case MY_CALENDAR:
                                MycalendarResponse _calendarresponse = gson.fromJson(response.toString().trim(), MycalendarResponse.class);
                                eventTracker.hideProgressDialog(_calendarresponse);
                                break;
                            case GET_SETTINGS:
                                GetSettingsResopnse _getSettingsresponse = gson.fromJson(response.toString().trim(), GetSettingsResopnse.class);
                                eventTracker.hideProgressDialog(_getSettingsresponse);
                                break;
                            case FACILITY_DETAIL:
                                ActivityManager _activityManagerFacilityDetail = (ActivityManager) eventTracker.getActivityContext().getSystemService(Context.ACTIVITY_SERVICE);
                                List<ActivityManager.RunningTaskInfo> _listOfTaskFacility = _activityManagerFacilityDetail.getRunningTasks(1);
                                for (ActivityManager.RunningTaskInfo _getTask : _listOfTaskFacility) {
                                    if (_getTask.topActivity.getClassName().equals(FacilityEventDetailActivity.class.getName())) {
                                        Log.d("response.toStrin==", response.toString().trim());
                                        FacilityDetailResponse _getfacilitysresponse = gson.fromJson(response.toString().trim(), FacilityDetailResponse.class);
                                        eventTracker.hideProgressDialog(_getfacilitysresponse);
                                    }
                                }
                                break;
                            case DELETE_EVENT:
                                //MycalendarResponse _calendarresponse = gson.fromJson(response.toString().trim(), MycalendarResponse.class);
                                // eventTracker.hideProgressDialog(_calendarresponse);
                                eventTracker.hideProgressDialog(response);

                                break;
                            case FORGOT_PASSWORD:
                                ForgotPasswordResponse forgotPasswordResponse = gson.fromJson(response.toString().trim(), ForgotPasswordResponse.class);
                                eventTracker.hideProgressDialog(forgotPasswordResponse);
                                break;
                            case NEARBY_OPEN_RIDING_AREAS:
                                WheretoRideResponse _rideresponse1 = gson.fromJson(response.toString().trim(), WheretoRideResponse.class);
                                eventTracker.hideProgressDialog(_rideresponse1);
                                break;

                            case ADVERTISEMENT:
                                AdvertiseResponse advertiseResponse = gson.fromJson(response.toString(), AdvertiseResponse.class);
                                eventTracker.hideProgressDialog(advertiseResponse);
                                break;
                           /* case ADD_CALENDAR:
                               *//* AdvertiseResponse advertiseResponse1 = gson.fromJson(response.toString(), AdvertiseResponse.class);
                                eventTracker.hideProgressDialog(advertiseResponse1);*//*
                                break;*/


                            default:
                                eventTracker.hideProgressDialog(response);
                                break;
                        }
                    }

                    @Override
                    public void onError(ANError error) {
                        Log.d("dataMayuri",""+ "ANError");

                        // handle error
                        eventTracker.hideProgressDialog( null);
                        //Log.i("Anish", error.getErrorDetail());
//                        Log.i("Anish",error.getErrorBody());
                    }
                });
    }


    public void callServiceWithJSONPOST(TypeFactory.RequestType _requestTypeAction, JSONObject jsonObject, final WebserviceStartEndEventTracker eventTracker) {
        this._requestType = _requestTypeAction;
        ANRequest.PostRequestBuilder _builder = _networking.post(WebServicePath.GetSoapServicePath(_requestType));
        _builder.addJSONObjectBody(jsonObject); // posting json
        _builder.setTag("test");
        _builder.setContentType("application/json");
        _builder.setUserAgent("application/json;charset=UTF-8");

        _builder.setPriority(Priority.MEDIUM);
        _builder.build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                // do anything with response
                //Log.i("Anish",response.toString());
                switch (_requestType) {

                }
            }

            @Override
            public void onError(ANError error) {
                // handle error
                //Log.i("Anish",error.getErrorBody()+" "+ error.getErrorDetail()+" "+error.getMessage());
                switch (_requestType) {

                    default:
                        eventTracker.hideProgressDialog(null);
                        break;

                }

                       /* Log.i("Anish",error.getErrorBody()+" "+ error.getErrorDetail()+" "+error.getMessage());
                        eventTracker.hideProgressDialog(null);*/
            }
        });
    }

    public void callServiceWithGET(TypeFactory.RequestType _requestTypeAction, List<DataEntity> _dataEntityToPost, final WebserviceStartEndEventTracker eventTracker) {
        this._requestType = _requestTypeAction;
        ANRequest.GetRequestBuilder _getRequestBuilder = AndroidNetworking.get(WebServicePath.GetSoapServicePath(_requestType));
        if (_dataEntityToPost != null && _dataEntityToPost.size() > 0) {
            for (final DataEntity _dataToPost : _dataEntityToPost) {
                // Here your room is available
                _getRequestBuilder.addQueryParameter(_dataToPost.getName(), _dataToPost.getValue());
            }
        }
        // _getRequestBuilder.setTag("test");
        _getRequestBuilder.setPriority(Priority.LOW);
        _getRequestBuilder.build().getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                //Log.i("Anish",response.toString());
                Gson gson = new Gson();
                switch (_requestType) {
                    case SANCTION:
                        SanctionResponse _sanctionResponseData = gson.fromJson(response.toString(), SanctionResponse.class);
                        eventTracker.hideProgressDialog(_sanctionResponseData);
                        break;

                    case DISCIPLINE:
                        DisciplineResponse _disciplineResponseData = gson.fromJson(response.toString(), DisciplineResponse.class);
                        eventTracker.hideProgressDialog(_disciplineResponseData);
                        break;
                    case EQUIPMENT:
                        EquipmentResponse _equipmenResponseData = gson.fromJson(response.toString(), EquipmentResponse.class);
                        eventTracker.hideProgressDialog(_equipmenResponseData);
                        break;
                    case FACTORY_CONTINGENCY:
                        FactoryContingencyResponse factoryContingencyResponse = gson.fromJson(response.toString(), FactoryContingencyResponse.class);
                        eventTracker.hideProgressDialog(factoryContingencyResponse);
                        break;
                    default:
                        eventTracker.hideProgressDialog(response);
                        break;
                }
            }

            @Override
            public void onError(ANError anError) {

            }
        });
    }


    public void callServiceWithPOSTWithPARSER(TypeFactory.RequestType _requestTypeAction, List<DataEntity> _dataEntityToPost, final WebserviceStartEndEventTracker eventTracker) {
        this._requestType = _requestTypeAction;
        ANRequest.PostRequestBuilder _builder = _networking.post(WebServicePath.GetSoapServicePath(_requestType));
        if (_dataEntityToPost != null && _dataEntityToPost.size() > 0) {
            for (final DataEntity _dataToPost : _dataEntityToPost) {
                // Here your room is available
                _builder.addBodyParameter(_dataToPost.getName(), _dataToPost.getValue());
            }
        }
        _builder.setTag("test");
        _builder.setPriority(Priority.HIGH);
        _builder.build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        eventTracker.hideProgressDialog(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                    }
                });
    }


    public TypeFactory.RequestType get_requestType() {
        return _requestType;
    }

}
