
package com.cynexis.motoguide.network.response.equipmentresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EquipmentData implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("EquipmentName")
    @Expose
    private String equipmentName;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

}
