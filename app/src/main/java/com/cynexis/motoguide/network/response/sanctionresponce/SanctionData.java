
package com.cynexis.motoguide.network.response.sanctionresponce;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SanctionData implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("SanctionName")
    @Expose
    private String sanctionName;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getSanctionName() {
        return sanctionName;
    }

    public void setSanctionName(String sanctionName) {
        this.sanctionName = sanctionName;
    }

}
