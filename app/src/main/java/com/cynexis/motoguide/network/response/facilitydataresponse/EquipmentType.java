package com.cynexis.motoguide.network.response.facilitydataresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Admin on 30-06-2017.
 */

public class EquipmentType implements Serializable {
    @SerializedName("EquipmentName")
    @Expose
    private String equipmentName;
    @SerializedName("EquipmentImage")
    @Expose
    private String equipmentImage;

    public String getEquipmentName() {
        return equipmentName;
    }

    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    public String getEquipmentImage() {
        return equipmentImage;
    }

    public void setEquipmentImage(String equipmentImage) {
        this.equipmentImage = equipmentImage;
    }

}
