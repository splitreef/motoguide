
package com.cynexis.motoguide.network.response.disciplineresponse;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DisciplineResponse implements Serializable {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<DisciplineData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<DisciplineData> getData() {
        return data;
    }

    public void setData(List<DisciplineData> data) {
        this.data = data;
    }

}
