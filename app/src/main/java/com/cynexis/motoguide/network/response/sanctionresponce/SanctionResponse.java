
package com.cynexis.motoguide.network.response.sanctionresponce;

import java.io.Serializable;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SanctionResponse implements Serializable {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<SanctionData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<SanctionData> getData() {
        return data;
    }

    public void setData(List<SanctionData> data) {
        this.data = data;
    }

}
