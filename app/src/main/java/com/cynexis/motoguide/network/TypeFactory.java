package com.cynexis.motoguide.network;

public class TypeFactory {
	
	public static enum RequestType
	{
		NONE,SIGN_UP,STATE,LOGIN,EQUIPMENT,DISCIPLINE,SANCTION,SEARCH,FACTORY_CONTINGENCY,REGISTER,NEAR_EVENTRACE,EVENT_DETAIL,TRACK_FACILITY
		,ADD_CALENDAR,MY_CALENDAR,DELETE_EVENT,GET_SETTINGS,FACILITY_DETAIL,UPDATE_ACCOUNT_SETTING,FORGOT_PASSWORD,UPDATE_ADDITIONAL_INFO_SETTING,
		ADVERTISEMENT,NEARBY_OPEN_RIDING_AREAS

	}
	
	public static enum SettingType
	{
		NONE,USER_ID,FIRSTNAME,LASTNAME,LOGIN_SUCCESS,EMAIL,EVENTID,PASSWORD,UPCOMINFEVENTID,
		REMEMBER_PASSWORD,REMEMBER_EMAIL,SELECTED_STATE,SELECTED_FACTORY_CONTIGENCY,SELECTED_STATE_NAME,DEVICE_ID
	}
	
}
