package com.cynexis.motoguide.network;



public class WebServicePath {
	//private static final String DOMAIN_NAME = "http://work.splitreef.com/client/development/motoguide/api/webservice/";


//	private static final String DOMAIN_NAME ="http://192.168.0.82/motoguide/api/webservice/";
//	private static final String DOMAIN_NAME = "http://192.168.0.57/motoguide/api/webservice/";

	//private static final String DOMAIN_NAME = "https://work.cynexis.com/client/development/motoguide/api/webservice/";


	//live url
	//private static final String DOMAIN_NAME = "http://admin.motoguideapp.com/api/webservice/";
	private static final String DOMAIN_NAME = "http://admin.motoguideapp.com/api/v2/webservice/";

	public static String GetSoapServicePath(TypeFactory.RequestType requestType)
	{
		if(requestType.equals(TypeFactory.RequestType.SIGN_UP))
		{
			return DOMAIN_NAME + "registration";

		}

		if(requestType.equals(TypeFactory.RequestType.STATE))
		{
			return DOMAIN_NAME + "state";
		}

		if(requestType.equals(TypeFactory.RequestType.LOGIN))
		{
			return DOMAIN_NAME + "login";
		}

		if(requestType.equals(TypeFactory.RequestType.EQUIPMENT))
		{
			return DOMAIN_NAME + "equipment";
		}

		if(requestType.equals(TypeFactory.RequestType.DISCIPLINE))
		{
			return DOMAIN_NAME + "discipline";
		}

		if(requestType.equals(TypeFactory.RequestType.SANCTION))
		{
			return DOMAIN_NAME + "sanction";
		}

		if(requestType.equals(TypeFactory.RequestType.SEARCH))
		{
			return DOMAIN_NAME + "eventsearch";
		}

		if(requestType.equals(TypeFactory.RequestType.FACTORY_CONTINGENCY))
		{
			return DOMAIN_NAME + "factorycontingency";
		}

		if(requestType.equals(TypeFactory.RequestType.REGISTER))
		{
			return DOMAIN_NAME + "registration";
		}

		if(requestType.equals(TypeFactory.RequestType.NEAR_EVENTRACE))
		{
			return DOMAIN_NAME + "nearby_event";
		}


		if(requestType.equals(TypeFactory.RequestType.NEARBY_OPEN_RIDING_AREAS))
		{
			return DOMAIN_NAME + "nearby_open_riding_areas";
		}

		if(requestType.equals(TypeFactory.RequestType.EVENT_DETAIL))
		{
			return DOMAIN_NAME + "eventdetails";
		}

		if(requestType.equals(TypeFactory.RequestType.TRACK_FACILITY))
		{
			return DOMAIN_NAME + "nearby_facility";
		}

		if(requestType.equals(TypeFactory.RequestType.ADD_CALENDAR))
		{
			return DOMAIN_NAME + "save_user_event";
		}

		if(requestType.equals(TypeFactory.RequestType.MY_CALENDAR))
		{
			return DOMAIN_NAME + "user_event";
		}

		if(requestType.equals(TypeFactory.RequestType.DELETE_EVENT))
		{
			return DOMAIN_NAME + "user_event_delete";
		}

        if(requestType.equals(TypeFactory.RequestType.GET_SETTINGS))
        {
            return DOMAIN_NAME + "user_setting";
        }
		if(requestType.equals(TypeFactory.RequestType.FACILITY_DETAIL))
		{
			return DOMAIN_NAME + "facilitydetails";
		}

		if(requestType.equals(TypeFactory.RequestType.FORGOT_PASSWORD))
		{
			return DOMAIN_NAME + "forgot";
		}

		if(requestType.equals(TypeFactory.RequestType.UPDATE_ACCOUNT_SETTING))
		{
			return DOMAIN_NAME + "update_user_account_setting";
		}

		if(requestType.equals(TypeFactory.RequestType.UPDATE_ADDITIONAL_INFO_SETTING))
		{
			return DOMAIN_NAME + "update_user_additional_info";
		}
		if(requestType.equals(TypeFactory.RequestType.ADVERTISEMENT))
		{
			return DOMAIN_NAME + "advertisement";
		}

		//    //http://work.splitreef.com/client/development/motoguide/api/webservice/advertisement
		return "";
	}
}
