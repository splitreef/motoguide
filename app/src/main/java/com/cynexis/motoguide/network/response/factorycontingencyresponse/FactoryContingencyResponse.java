
package com.cynexis.motoguide.network.response.factorycontingencyresponse;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FactoryContingencyResponse implements Serializable {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<FactoryConigencyData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<FactoryConigencyData> getData() {
        return data;
    }

    public void setData(List<FactoryConigencyData> data) {
        this.data = data;
    }

}
