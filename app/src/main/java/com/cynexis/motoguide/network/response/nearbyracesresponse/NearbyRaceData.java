
package com.cynexis.motoguide.network.response.nearbyracesresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class NearbyRaceData implements Serializable {
    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("Coverphoto")
    @Expose
    private String coverphoto;
    @SerializedName("Logo")
    @Expose
    private String logo;
    @SerializedName("EventName")
    @Expose
    private String eventName;
    @SerializedName("TrackFacility")
    @Expose
    private String trackFacility;
    @SerializedName("TrackFacilityID")
    @Expose
    private String trackFacilityID;
    @SerializedName("StartDate")
    @Expose
    private String startDate;
    @SerializedName("EndDate")
    @Expose
    private String endDate;
    @SerializedName("Address")
    @Expose
    private String address;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("WebsiteURL")
    @Expose
    private String websiteURL;
    @SerializedName("FacebookPageURL")
    @Expose
    private String facebookPageURL;
    @SerializedName("Phone")
    @Expose
    private String phone;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("SanctionType")
    @Expose
    private String sanctionType;
    @SerializedName("DisciplineType")
    @Expose
    private String disciplineType;
    @SerializedName("EquipmentType")
    @Expose
    private String equipmentType;
    @SerializedName("FactoryContingency")
    @Expose
    private String factoryContingency;
    @SerializedName("AdditionalInfo")
    @Expose
    private String additionalInfo;
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("distance")
    @Expose
    private String distance;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;


    public String getTrackFacilityID() {
        return trackFacilityID;
    }

    public void setTrackFacilityID(String trackFacilityID) {
        this.trackFacilityID = trackFacilityID;
    }

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getCoverphoto() {
        return coverphoto;
    }

    public void setCoverphoto(String coverphoto) {
        this.coverphoto = coverphoto;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getTrackFacility() {
        return trackFacility;
    }

    public void setTrackFacility(String trackFacility) {
        this.trackFacility = trackFacility;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getWebsiteURL() {
        return websiteURL;
    }

    public void setWebsiteURL(String websiteURL) {
        this.websiteURL = websiteURL;
    }

    public String getFacebookPageURL() {
        return facebookPageURL;
    }

    public void setFacebookPageURL(String facebookPageURL) {
        this.facebookPageURL = facebookPageURL;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSanctionType() {
        return sanctionType;
    }

    public void setSanctionType(String sanctionType) {
        this.sanctionType = sanctionType;
    }

    public String getDisciplineType() {
        return disciplineType;
    }

    public void setDisciplineType(String disciplineType) {
        this.disciplineType = disciplineType;
    }

    public String getEquipmentType() {
        return equipmentType;
    }

    public void setEquipmentType(String equipmentType) {
        this.equipmentType = equipmentType;
    }

    public String getFactoryContingency() {
        return factoryContingency;
    }

    public void setFactoryContingency(String factoryContingency) {
        this.factoryContingency = factoryContingency;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }
}
