package com.cynexis.motoguide.network;

import android.content.Context;

/**
 * Created by Admin on 23-08-2017.
 */

public interface GettingActivityInstance {

    public Context getActivityContext();
}
