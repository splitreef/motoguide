
package com.cynexis.motoguide.network.response.getsettingsresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Discipline implements Serializable {

    @SerializedName("ID")
    @Expose
    private String iD;
    @SerializedName("DisciplineName")
    @Expose
    private String disciplineName;
    @SerializedName("CreateDate")
    @Expose
    private String createDate;

    public String getID() {
        return iD;
    }

    public void setID(String iD) {
        this.iD = iD;
    }

    public String getDisciplineName() {
        return disciplineName;
    }

    public void setDisciplineName(String disciplineName) {
        this.disciplineName = disciplineName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

}
