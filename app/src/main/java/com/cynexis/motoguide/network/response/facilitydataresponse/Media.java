
package com.cynexis.motoguide.network.response.facilitydataresponse;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Media implements Serializable {

    @SerializedName("image")
    @Expose
    private List<Image> image = null;
    @SerializedName("video")
    @Expose
    private List<Video> video = null;

    public List<Image> getImage() {
        return image;
    }

    public void setImage(List<Image> image) {
        this.image = image;
    }

    public List<Video> getVideo() {
        return video;
    }

    public void setVideo(List<Video> video) {
        this.video = video;
    }


}
