
package com.cynexis.motoguide.network.response.equipmentresponse;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EquipmentResponse implements Serializable {

    @SerializedName("Status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<EquipmentData> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<EquipmentData> getData() {
        return data;
    }

    public void setData(List<EquipmentData> data) {
        this.data = data;
    }

}
