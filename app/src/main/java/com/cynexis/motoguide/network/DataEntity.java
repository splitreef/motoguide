package com.cynexis.motoguide.network;

import java.util.ArrayList;
import java.util.List;

public class DataEntity {
	
	private String name;
	private String value;
	private List<DataEntity> list;
	
	public DataEntity(String _name, String _value)
	{
		this.setName(_name);
		this.setValue(_value);
		list = new ArrayList<DataEntity>();
	}
	
	public DataEntity(String _name)
	{
		this.setName(_name);
		list = new ArrayList<DataEntity>();
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}

	public List<DataEntity> getList() {
		return list;
	}

	public void setList(List<DataEntity> list) {
		this.list = list;
	}
	
	public void appendItem(DataEntity _item)
	{
		list.add(_item);
	}
	
	public void removeItem(DataEntity _item)
	{
		list.remove(_item);
	}
	
	public static String getValueByName(List<DataEntity> _list, String _name, Boolean _isCaseSensitive)
	{		
		for(int i=0;i<_list.size();i++)
		{
			if(_isCaseSensitive.equals(true))
			{
				if(_list.get(i).getName().toLowerCase().equals(_name.toLowerCase()))
				{
					return _list.get(i).getValue(); 
				}
			}
			else
			{
				if(_list.get(i).getName().equals(_name))
				{
					return _list.get(i).getValue(); 
				}
			}
		}
		return null;
	}
	
}
