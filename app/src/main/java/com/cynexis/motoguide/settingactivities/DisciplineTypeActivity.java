package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ListView;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.disciplineresponse.DisciplineData;
import com.cynexis.motoguide.network.response.disciplineresponse.DisciplineResponse;
import com.cynexis.motoguide.settingactivities.adapter.DisciplineAdapter;
import com.cynexis.motoguide.settingactivities.adapter.ItemCheckedUnCheckedCalled;
import com.cynexis.motoguide.startingactivities.permission.EquipmentAndDisceplineSerializableClass;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DisciplineTypeActivity extends AppCompatActivity implements WebserviceStartEndEventTracker,ItemCheckedUnCheckedCalled {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cball)
    CheckBox cball;
    @BindView(R.id.lvdiscipline)
    ListView lvdiscipline;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.cbnone)
    CheckBox cbnone;
    @BindView(R.id.ivback)
    ImageButton ivback;
    Bundle _extra;
    EquipmentAndDisceplineSerializableClass _dataToSend;
    FilterSerializableForRaceEventsClass _arrayOfEvents;

    MainApplication _mainApplication;
    List<DisciplineData> _disciplinitem;
    DisciplineAdapter disciplineAdapter;
    public SparseBooleanArray mChecked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra =  this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        mChecked = new SparseBooleanArray();
        initializeActivity();
        bindControls();
    }


    private void initializeActivity() {
        setContentView(R.layout.activity_discipline_type);
        ButterKnife.bind(this);
        cbnone.setChecked(true);
        if(_extra.getSerializable("DATA") != null)
        {
            _dataToSend = (EquipmentAndDisceplineSerializableClass)_extra.getSerializable("DATA");
        }
        else
        {
            _arrayOfEvents = (FilterSerializableForRaceEventsClass)_extra.getSerializable("FILTER");
        }
    }

    private void bindControls() {
        ivmenu.setVisibility(View.GONE);
        if (_mainApplication.networkManager.CheckNetworkConnection())
        {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("", ""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.DISCIPLINE, _dataToPost, this);
        }
        else
        {
            _mainApplication.messageManager.noInternetConnectionMessage(lvdiscipline);
        }
    }
    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if(_object != null)
        {
            DisciplineResponse _response = (DisciplineResponse) _object[0];
            //Log.i("tanu", String.valueOf(_response));
            if(_response != null)
            {
                if(_response.getStatus() == 1)
                {
                    if(_response.getData()!= null && _response.getData().size() >0)
                    {
                        _disciplinitem = _response.getData();

                        if(_dataToSend.getDiscipline() != null && _dataToSend.getDiscipline().size() >0)
                        {
                            for (int i = 0 ; i < _disciplinitem.size() ; i++)
                            {
                                if(_dataToSend.getDiscipline().contains(Integer.valueOf(_disciplinitem.get(i).getID())))
                                {
                                    mChecked.put(i,true);
                                }
                                else
                                {
                                    mChecked.put(i,false);
                                }
                            }
                        }
                        disciplineAdapter = new DisciplineAdapter(this,_disciplinitem,this,mChecked);
                        lvdiscipline.setAdapter(disciplineAdapter);
                    }
                }
                else
                {
                    // _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                }
            }
        }
        else
        {
            _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
        }
    }


    @OnClick({R.id.ivback, R.id.cball, R.id.cbnone, R.id.btnsave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.cball:
                cbnone.setChecked(false);
                cball.setChecked(true);
                cball.setBackgroundResource(R.drawable.allcheck);
                cbnone.setBackgroundResource(R.drawable.noneuncheck);

                for (int i = 0; i < _disciplinitem.size() ; i++)
                {
                    mChecked.put(i, cball.isChecked());
                }
                disciplineAdapter.notifyDataSetChanged();
                break;

            case R.id.cbnone:
                cbnone.setChecked(true);
                cball.setChecked(false);

                cball.setBackgroundResource(R.drawable.alluncheck);
                cbnone.setBackgroundResource(R.drawable.nonecheck);

                for (int i = 0; i < _disciplinitem.size() ; i++)
                {
                    mChecked.put(i, false);
                }
                disciplineAdapter.notifyDataSetChanged();
                break;

            case R.id.btnsave:
                if(mChecked != null && mChecked.size() > 0)
                {
                    ArrayList<Integer> _allCheckedIDs = new ArrayList<>();
                    for (int i = 0; i < mChecked.size() ; i++)
                    {
                        if(mChecked.get(i))
                        {
                            _allCheckedIDs.add(Integer.valueOf(_disciplinitem.get(i).getID()));
                            //Log.i("Anish",_disciplinitem.get(i).getID());
                        }
                    }
                    _dataToSend.setDiscipline(_allCheckedIDs);
                }
                if(_extra.getString("TYPE") != null)
                {
                    Intent _intent =  new Intent(this, AccountInfoActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("DATA", (Serializable) _dataToSend);
                    _intent.putExtras(bundle);
                    startActivity(_intent);
                    finish();
                }
                else
                {
                    Intent _intent =  new Intent(this, TypeChooserActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("DATA", (Serializable) _dataToSend);
                    _intent.putExtras(bundle);
                    startActivity(_intent);
                    finish();
                }
                break;

        }

    }

    @Override
    public void getItemCheckedUnChecked(int position, boolean ischecked) {
        mChecked.put(position, ischecked);
        if(mChecked.size() == _disciplinitem.size())
        {
            if (isAllValuesChecked())
            {
                cball.setChecked(true);
                cbnone.setChecked(false);
            }
            else if(isAllValuesUnChecked())
            {

                cball.setChecked(false);
                cbnone.setChecked(false);
            }
            else
            {
                cball.setChecked(false);
                cbnone.setChecked(true);
            }
        }
        else
        {
            cball.setChecked(false);
            cbnone.setChecked(false);
        }
    }

    @Override
    public boolean getItemCheckedUnCheckedByPosition(int position) {
        return mChecked.get(position);
    }

    protected boolean isAllValuesChecked()
    {
        for (int i = 0; i < mChecked.size(); i++)
        {
            if (!mChecked.get(i))
            {
                return false;
            }
        }
        return true;
    }

    protected boolean isAllValuesUnChecked() {

        for (int i = 0; i < mChecked.size(); i++)
        {
            if (mChecked.get(i))
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean showChekBox() {
        return false;
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
