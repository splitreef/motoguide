package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.signupresponse.SignUpResonse;
import com.cynexis.motoguide.startingactivities.LoginActivity;
import com.cynexis.motoguide.startingactivities.permission.EquipmentAndDisceplineSerializableClass;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class TypeChooserActivity extends AppCompatActivity implements WebserviceStartEndEventTracker{

    @BindView(R.id.ivquestion)
    ImageView ivquestion;
    @BindView(R.id.rlequipmenttype)
    RelativeLayout rlequipmenttype;
    @BindView(R.id.rldisciplinetype)
    RelativeLayout rldisciplinetype;
    @BindView(R.id.btnregister)
    Button btnregister;

    Intent _intent;
    Bundle _extra;
    String email,first_name,last_name,phone,password,address,user_lat = "39.9612",user_long = "82.9988",city,state,zip;
    EquipmentAndDisceplineSerializableClass _equipmentAndDisceplineType;
    Bundle bundle;
    MainApplication _mainApplication;
    String _jsonDataForEquipmentAndDiscipline = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra = this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        initializeActivity();
        bindControls();
    }

    private void initializeActivity() {
        setContentView(R.layout.activity_type_chooser);
        ButterKnife.bind(this);
    }

    private void bindControls()
    {
        if(_extra != null)
        {
            email =  _extra.getString("Email");
            first_name = _extra.getString("FirstName");
            last_name = _extra.getString("LastName");
            phone = _extra.getString("Phone");
            password = _extra.getString("Password");
            address = _extra.getString("Address");
            city = _extra.getString("City");
            state = _extra.getString("State");
            zip = _extra.getString("Zip");
            user_lat = _extra.getString("LAT");
            user_long = _extra.getString("LONG");
        }
        _equipmentAndDisceplineType =  new EquipmentAndDisceplineSerializableClass();
    }

    @OnClick({R.id.ivquestion, R.id.rlequipmenttype, R.id.rldisciplinetype, R.id.btnregister})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivquestion:
                displayPopupWindow(ivquestion);
                break;

            case R.id.rlequipmenttype:
                _intent = new Intent(TypeChooserActivity.this,EquipmentTypeActivity.class);
                 bundle = new Bundle();
                bundle.putSerializable("DATA", (Serializable) _equipmentAndDisceplineType);
                _intent.putExtras(bundle);
                startActivity(_intent);
                break;
            case R.id.rldisciplinetype:
                _intent = new Intent(TypeChooserActivity.this,DisciplineTypeActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("DATA", (Serializable) _equipmentAndDisceplineType);
                _intent.putExtras(bundle);
                startActivity(_intent);
                break;
            case R.id.btnregister:
                if(_equipmentAndDisceplineType.getDiscipline() == null && _equipmentAndDisceplineType.getEquipment() == null)
                {
                    //_mainApplication.dialogManager.DisplayAlertDialog(this,"","Are you sure to register without Equipment And Discipline Type. If yes all will be selected as your preference.");

                    AlertDialog.Builder _dialogForConfirmation = new AlertDialog.Builder(this);
                    _dialogForConfirmation.setMessage("Are you sure to register without Equipment And Discipline Type. If yes all will be selected as your preference.");
                    _dialogForConfirmation.setIcon(android.R.drawable.ic_dialog_alert);
                    _dialogForConfirmation.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                            Gson _g =  new Gson();
                            _g.toJson(_equipmentAndDisceplineType.toString());
                           // Log.i("Anish",_g.toJson(_equipmentAndDisceplineType).toString());
                            _jsonDataForEquipmentAndDiscipline = _g.toJson(_equipmentAndDisceplineType).toString();

                            if (_mainApplication.networkManager.CheckNetworkConnection())
                            {
                                showProgressDialog();

                                List<DataEntity> _dataToPost = new ArrayList<>();
                                _dataToPost.add(new DataEntity("Email", email));
                                _dataToPost.add(new DataEntity("FirstName", first_name));
                                _dataToPost.add(new DataEntity("LastName", last_name));
                                _dataToPost.add(new DataEntity("Phone", phone));
                                _dataToPost.add(new DataEntity("Password", password));
                                _dataToPost.add(new DataEntity("Address", address));
                                _dataToPost.add(new DataEntity("City", city));
                                _dataToPost.add(new DataEntity("State", state));
                                _dataToPost.add(new DataEntity("Zip", zip));
                                if(user_lat != null)
                                {
                                    _dataToPost.add(new DataEntity("Latitude", user_lat));
                                    _dataToPost.add(new DataEntity("Longitude", user_long));
                                }

                                /*else
                                {
                                    _dataToPost.add(new DataEntity("Latitude", "39.9612"));
                                    _dataToPost.add(new DataEntity("Longitude", "82.9988"));
                                }*/

                                _dataToPost.add(new DataEntity("additionalinfo", _jsonDataForEquipmentAndDiscipline));
                                AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.REGISTER, _dataToPost, TypeChooserActivity.this);
                            }
                            else
                            {
                                _mainApplication.messageManager.noInternetConnectionMessage(btnregister);
                            }

                        }});
                    _dialogForConfirmation.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }});
                    _dialogForConfirmation.show();
                    return;
                }
                else
                {
                    Gson _g =  new Gson();
                    _g.toJson(_equipmentAndDisceplineType.toString());
                    //Log.i("Anish",_g.toJson(_equipmentAndDisceplineType).toString());
                    _jsonDataForEquipmentAndDiscipline = _g.toJson(_equipmentAndDisceplineType).toString();

                    if (_mainApplication.networkManager.CheckNetworkConnection())
                    {
                        showProgressDialog();

                        List<DataEntity> _dataToPost = new ArrayList<>();
                        _dataToPost.add(new DataEntity("Email", email));
                        _dataToPost.add(new DataEntity("FirstName", first_name));
                        _dataToPost.add(new DataEntity("LastName", last_name));
                        _dataToPost.add(new DataEntity("Phone", phone));
                        _dataToPost.add(new DataEntity("Password", password));
                        _dataToPost.add(new DataEntity("Address", address));
                        _dataToPost.add(new DataEntity("City", city));
                        _dataToPost.add(new DataEntity("State", state));
                        _dataToPost.add(new DataEntity("Zip", zip));
                        if(user_lat != null)
                        {
                            _dataToPost.add(new DataEntity("Latitude", user_lat));
                            _dataToPost.add(new DataEntity("Longitude", user_long));
                        }
                        else
                        {
                            _dataToPost.add(new DataEntity("Latitude", "39.9612"));
                            _dataToPost.add(new DataEntity("Longitude", "82.9988"));
                        }
                        _dataToPost.add(new DataEntity("additionalinfo", _jsonDataForEquipmentAndDiscipline));
                        AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.REGISTER, _dataToPost, this);
                    }
                    else
                    {
                        _mainApplication.messageManager.noInternetConnectionMessage(btnregister);
                    }
                }


                break;
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
    _mainApplication.dialogManager.dismissProgressDialog();
        if(_object != null)
        {
            SignUpResonse _response = (SignUpResonse) _object[0];
            if(_response.getStatus() == 1)
            {
                _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                ActivityCompat.finishAffinity(this);
                _intent =  new Intent(this, LoginActivity.class);
                startActivity(_intent);
                finish();
            }
            else
            {
                _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
            }
        }
        else
        {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if(intent.getExtras().getSerializable("DATA") != null)
        {
            _equipmentAndDisceplineType = (EquipmentAndDisceplineSerializableClass)intent.getExtras().getSerializable("DATA");
        }
    }

    private void displayPopupWindow(View anchorView) {
        PopupWindow popup = new PopupWindow(this);
        View layout = getLayoutInflater().inflate(R.layout.popup_content, null);
        popup.setContentView(layout);
        // Set content width and height
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        // Closes the popup window when touch outside of it - when looses focus
        popup.setOutsideTouchable(true);
        popup.setFocusable(true);
        // Show anchored to button
        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAsDropDown(anchorView);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
