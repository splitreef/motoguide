package com.cynexis.motoguide.settingactivities.adapter;

/**
 * Created by user1 on 9/7/2016.
 */
public interface ItemCheckedUnCheckedCalled {

    public void getItemCheckedUnChecked(int position, boolean ischecked);
    public boolean getItemCheckedUnCheckedByPosition(int position);
    public boolean showChekBox();
}
