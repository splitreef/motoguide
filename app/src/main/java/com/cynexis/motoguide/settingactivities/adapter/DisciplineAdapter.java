package com.cynexis.motoguide.settingactivities.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.network.response.disciplineresponse.DisciplineData;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 25-05-2017.
 */

public class DisciplineAdapter extends BaseAdapter {

    Context _ctx;
    List<DisciplineData> _disciplienitemlist;
    private LayoutInflater _inflater = null;
    ItemCheckedUnCheckedCalled _internFace;
    SparseBooleanArray _selctedPositon;

    public DisciplineAdapter(Context _ctx, List<DisciplineData> _disciplienitemlist, ItemCheckedUnCheckedCalled _internFace,SparseBooleanArray selctedPositon) {
        this._ctx = _ctx;
        this._disciplienitemlist = _disciplienitemlist;
        this._internFace = _internFace;
        _inflater = ( LayoutInflater )_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this._selctedPositon = selctedPositon;
    }

    @Override
    public int getCount() {
        return _disciplienitemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHolderOne holder;
        if (view != null)
        {
            holder = (ViewHolderOne) view.getTag();
        }
        else
        {
            view = _inflater.inflate(R.layout.custom_discipline_list, parent, false);
            holder = new ViewHolderOne(view);
            view.setTag(holder);
        }

        /////////////////////////////////////////////// LOGIC FOR CHECK UNCHECK
        if(_selctedPositon != null)
        {
            if(_selctedPositon.get(position))
            {
                _internFace.getItemCheckedUnChecked(position, true);
            }
            else
            {
                _internFace.getItemCheckedUnChecked(position, false);
            }
        }
        else
        {
            _internFace.getItemCheckedUnChecked(position, false);
        }
        ////////////////////////////////////////////////////////////////////////

        holder.tvname.setText(this._disciplienitemlist.get(position).getDisciplineName());
        holder.switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    _internFace.getItemCheckedUnChecked(position, isChecked);
                }
                else
                {
                    _internFace.getItemCheckedUnChecked(position, isChecked);
                }

            }
        });
        holder.switchOnOff.setChecked((_internFace.getItemCheckedUnCheckedByPosition(position) == true ? true : false));
        return view;
    }

    static class ViewHolderOne {
        @BindView(R.id.tvname)
        TextView tvname;
        @BindView(R.id.switchOnOff)
        SwitchCompat switchOnOff;

        public ViewHolderOne(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
