package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.getsettingsresponse.Discipline;
import com.cynexis.motoguide.network.response.getsettingsresponse.Equipment;
import com.cynexis.motoguide.network.response.getsettingsresponse.GetSettingsResopnse;
import com.cynexis.motoguide.startingactivities.permission.EquipmentAndDisceplineSerializableClass;
import com.google.gson.Gson;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProfileSettingActivity extends AppCompatActivity implements WebserviceStartEndEventTracker {


    @BindView(R.id.etphone)
    EditText etphone;
    @BindView(R.id.etpassword)
    ShowHidePasswordEditText etpassword;
    @BindView(R.id.etnewpassword)
    ShowHidePasswordEditText etnewpassword;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.etemail)
    TextView etemail;
    @BindView(R.id.llaccount)
    LinearLayout llaccount;
    @BindView(R.id.activity_account_info)
    LinearLayout activityAccountInfo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.etfirstname)
    EditText etfirstname;
    @BindView(R.id.etlstname)
    EditText etlstname;
    @BindView(R.id.btnsave)
    Button btnsave;


    MainApplication _mainApplication;
    SlideMenueImplimentationClass _slidingMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity();
        ButterKnife.bind(this);
        bindControls();
    }

    private void initializeActivity() {
        _mainApplication = (MainApplication) getApplicationContext();
        setContentView(R.layout.activity_account_info);
        ButterKnife.bind(this);
        etemail.setText(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.EMAIL));
        etpassword.setTransformationMethod(new PasswordTransformationMethod());
        etnewpassword.setTransformationMethod(new PasswordTransformationMethod());
        etemail.setFocusable(false);
    }

    private void bindControls() {
        ivmenu.setVisibility(View.GONE);
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();


        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("UserID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.GET_SETTINGS, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(etemail);
        }
    }


    @OnClick({R.id.ivback, R.id.ivmenu, R.id.btnsave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.btnsave:
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                if (_mainApplication.networkManager.CheckNetworkConnection()) {

                    if (etphone.getText().toString().trim().equals("")) {
                        etphone.setError("Please enter Phone");
                        etphone.requestFocus();
                        return;
                    }
                    if (etphone.getText().toString().trim().length() < 10) {
                        etphone.setError("Please enter valid Phone number");
                        etphone.requestFocus();
                        return;
                    }
                    if (etnewpassword.getText().toString().trim().length() > 0)
                    {
                        if (etpassword.getText().toString().trim().equals(""))
                        {
                            etpassword.setError("Please enter current password");
                            etpassword.requestFocus();
                            return;
                        }
                    }
                    if (etpassword.getText().toString().trim().length() > 0)
                    {
                        if (!etpassword.getText().toString().trim().equals(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.PASSWORD)))
                        {
                            etpassword.setError("Old Password does not match with the entered password.");
                            etpassword.requestFocus();
                            return;
                        }
                    }

                    showProgressDialog();
                    List<DataEntity> _dataToPost = new ArrayList<>();
                    _dataToPost.add(new DataEntity("FirstName", etfirstname.getText().toString().trim()));
                    _dataToPost.add(new DataEntity("LastName",  etlstname.getText().toString().trim()));
                    _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
                    _dataToPost.add(new DataEntity("phone", etphone.getText().toString()));
                    if (etnewpassword.getText().toString().trim().length() > 0) {
                        _dataToPost.add(new DataEntity("password", etnewpassword.getText().toString()));
                    } else {
                        _dataToPost.add(new DataEntity("password", " "));
                    }
                    AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.UPDATE_ACCOUNT_SETTING, _dataToPost, this);


                } else {
                    _mainApplication.messageManager.noInternetConnectionMessage(btnsave);
                }
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.GET_SETTINGS)) {
                GetSettingsResopnse _responce = (GetSettingsResopnse) _object[0];
                List<Integer> _disciplineList = new ArrayList<>();
                List<Integer> _equipmentList = new ArrayList<>();
                if (_responce != null) {
                    for (Discipline _data : _responce.getDiscipline()) {
                        _disciplineList.add(Integer.valueOf(_data.getID()));
                    }

                    for (Equipment _equipment : _responce.getEquipment()) {
                        _equipmentList.add(Integer.valueOf(_equipment.getID()));
                    }

                    /*_equipmentAndDisceplineType.setDiscipline(_disciplineList);
                    _equipmentAndDisceplineType.setEquipment(_equipmentList);*/

                    etphone.setText(_responce.getData().get(0).getPhone());
                    etfirstname.setText(_responce.getData().get(0).getFirstName());
                    etlstname.setText(_responce.getData().get(0).getLastName());
                    //spradius.setSelection(_responce.getData().get(0).getRadius());
                } else {
                    _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
                }
            } else {
                JSONObject _response = (JSONObject) _object[0];
                //Log.i("tanu", String.valueOf(_response));
                try {
                    if (_response.getInt("Status") == 1) {
                        if (etnewpassword.getText().toString().trim().length() > 0) {
                            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.PASSWORD, etnewpassword.getText().toString());
                        }
                        _mainApplication.messageManager.DisplayToastMessage(_response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        } else {
            _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
        }

    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
