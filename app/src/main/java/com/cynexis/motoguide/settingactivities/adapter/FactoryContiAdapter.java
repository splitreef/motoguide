package com.cynexis.motoguide.settingactivities.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.network.response.factorycontingencyresponse.FactoryConigencyData;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 01-06-2017.
 */

public class FactoryContiAdapter extends BaseAdapter {

    Context _ctx;
    List<FactoryConigencyData> _factoryitemlist;
    ItemCheckedUnCheckedCalled _internFace;
    SparseBooleanArray _selctedPositon;
    LayoutInflater _inflater = null;


    public FactoryContiAdapter(Context _ctx, List<FactoryConigencyData> _factoryitemlist,ItemCheckedUnCheckedCalled _internFace, SparseBooleanArray selctedPositon) {
        this._ctx = _ctx;
        this._factoryitemlist = _factoryitemlist;
        this._internFace = _internFace;
        _inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this._selctedPositon = selctedPositon;
    }

    @Override
    public int getCount() {
        return _factoryitemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
      ViewHolderOne holder;
        if (view != null) {
            holder = (ViewHolderOne) view.getTag();
        } else {
            view = _inflater.inflate(R.layout.custom_discipline_list, parent, false);
            holder = new ViewHolderOne(view);
            view.setTag(holder);
        }
        holder.tvname.setText(_factoryitemlist.get(position).getFactoryName());
        /////////////////////////////////////////////// LOGIC FOR CHECK UNCHECK
        if(_factoryitemlist != null)
        {
            if(_selctedPositon.get(position))
            {
                _internFace.getItemCheckedUnChecked(position, true);
            }
            else
            {
                _internFace.getItemCheckedUnChecked(position, false);
            }
        }
        else
        {
            _internFace.getItemCheckedUnChecked(position, false);
        }
        ////////////////////////////////////////////////////////////////////////

        if(_factoryitemlist != null)
        {
            holder.tvname.setText(this._factoryitemlist.get(position).getFactoryName());
        }
        else
        {
            holder.tvname.setText(this._factoryitemlist.get(position).getFactoryName());
        }

        holder.switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    _internFace.getItemCheckedUnChecked(position, isChecked);
                }
                else
                {
                    _internFace.getItemCheckedUnChecked(position, isChecked);
                }

            }
        });
        holder.switchOnOff.setChecked((_internFace.getItemCheckedUnCheckedByPosition(position) == true ? true : false));
        return view;
    }
    static class ViewHolderOne {
        @BindView(R.id.tvname)
        TextView tvname;
        @BindView(R.id.switchOnOff)
        SwitchCompat switchOnOff;

        public ViewHolderOne(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
