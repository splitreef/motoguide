package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.filters.FilterRaceEventsActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.sanctionresponce.SanctionData;
import com.cynexis.motoguide.network.response.sanctionresponce.SanctionResponse;
import com.cynexis.motoguide.settingactivities.adapter.ItemCheckedUnCheckedCalled;
import com.cynexis.motoguide.settingactivities.adapter.SanctionAdapter;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SanctionType extends AppCompatActivity implements WebserviceStartEndEventTracker,ItemCheckedUnCheckedCalled {

    MainApplication _mainApplication;
    List<SanctionData> _sanctiontitem;
    SanctionAdapter sanctionAdapter;
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cball)
    CheckBox cball;
    @BindView(R.id.llallequipment)
    LinearLayout llallequipment;
    @BindView(R.id.cbnone)
    CheckBox cbnone;
    @BindView(R.id.llcontainer)
    LinearLayout llcontainer;
    @BindView(R.id.btnsave)
    Button btnsave;
    @BindView(android.R.id.list)
    ListView list;


    public SparseBooleanArray mChecked;
    Bundle _extra;
    FilterSerializableForRaceEventsClass _arrayOfEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra = this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        mChecked = new SparseBooleanArray();

        initalizeActivity();
        bindControls();
    }

    private void initalizeActivity() {
        setContentView(R.layout.activity_sanction_type);
        ButterKnife.bind(this);
        ivback.setVisibility(View.VISIBLE);
    }

    private void bindControls()
    {
        _arrayOfEvents = (FilterSerializableForRaceEventsClass) _extra.getSerializable("FILTER");
        if (_mainApplication.networkManager.CheckNetworkConnection())
        {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("", ""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.SANCTION, _dataToPost, this);
        }
        else
        {
            _mainApplication.messageManager.noInternetConnectionMessage(list);
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null)
        {
            SanctionResponse _response = (SanctionResponse) _object[0];
            if (_response.getStatus() == 1)
            {
                _sanctiontitem = _response.getData();
                if (_response.getData() != null && _response.getData().size() > 0)
                {
                    if (_arrayOfEvents != null)
                    {
                        if (_arrayOfEvents.getSanction() != null && _arrayOfEvents.getSanction().size() > 0)
                        {
                            for (int i = 0; i < _sanctiontitem.size(); i++)
                            {
                                if (_arrayOfEvents.getSanction().contains(_sanctiontitem.get(i).getSanctionName()))
                                {
                                    mChecked.put(i, true);
                                }
                                else
                                {
                                    mChecked.put(i, false);
                                }
                            }
                        }
                    }
                    sanctionAdapter = new SanctionAdapter(this, _sanctiontitem,this,mChecked);
                    list.setAdapter(sanctionAdapter);
                }

            } else {
                _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
            }
        }
        else
        {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }


    @Override
    public void getItemCheckedUnChecked(int position, boolean ischecked) {
        mChecked.put(position, ischecked);
        if (mChecked.size() == _sanctiontitem.size()) {
            if (isAllValuesChecked()) {
                cball.setChecked(true);
                cbnone.setChecked(false);
            } else if (isAllValuesUnChecked()) {

                cball.setChecked(false);
                cbnone.setChecked(false);
            } else {
                cball.setChecked(false);
                cbnone.setChecked(true);
            }
        } else {
            cball.setChecked(false);
            cbnone.setChecked(false);
        }
    }

    @Override
    public boolean getItemCheckedUnCheckedByPosition(int position) {

        if (mChecked != null && mChecked.size() > 0) {
            ArrayList<String> _allCheckedIDs = new ArrayList<>();
            for (int i = 0; i < mChecked.size(); i++) {
                if (mChecked.get(i))
                {
                    _allCheckedIDs.add(_sanctiontitem.get(i).getSanctionName());
                    //Log.i("Anish", _sanctiontitem.get(i).getSanctionName());
                }
            }
            _arrayOfEvents.setSanction(_allCheckedIDs);
        }

        return mChecked.get(position);
    }

    protected boolean isAllValuesChecked() {

        for (int i = 0; i < mChecked.size(); i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    protected boolean isAllValuesUnChecked() {

        for (int i = 0; i < mChecked.size(); i++) {
            if (mChecked.get(i)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean showChekBox() {
        return false;
    }

    @OnClick({R.id.ivback, R.id.cball, R.id.cbnone, R.id.btnsave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.cball:
                cbnone.setChecked(false);
                cball.setChecked(true);
                cball.setBackgroundResource(R.drawable.allcheck);
                cbnone.setBackgroundResource(R.drawable.noneuncheck);

                for (int i = 0; i < _sanctiontitem.size(); i++) {
                    mChecked.put(i, true);
                }
                sanctionAdapter.notifyDataSetChanged();
                break;

            case R.id.cbnone:
                cbnone.setChecked(true);
                cball.setChecked(false);

                cball.setBackgroundResource(R.drawable.alluncheck);
                cbnone.setBackgroundResource(R.drawable.nonecheck);

                for (int i = 0; i < _sanctiontitem.size(); i++) {
                    mChecked.put(i, false);
                }
                sanctionAdapter.notifyDataSetChanged();
                break;

            case R.id.btnsave:
                if (mChecked != null && mChecked.size() > 0) {
                    ArrayList<String> _allCheckedName = new ArrayList<>();
                    for (int i = 0; i < mChecked.size(); i++) {
                        if (mChecked.get(i)) {
                            _allCheckedName.add(_sanctiontitem.get(i).getSanctionName()); // Containing Name
                            //Log.i("Anish", _sanctiontitem.get(i).getSanctionName());
                        }
                    }
                    _arrayOfEvents.setSanction(_allCheckedName);
                }
                Intent _intent = new Intent(this, FilterRaceEventsActivity.class);
                _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                Bundle bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                finish();

                break;
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
