package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.getsettingsresponse.Discipline;
import com.cynexis.motoguide.network.response.getsettingsresponse.Equipment;
import com.cynexis.motoguide.network.response.getsettingsresponse.GetSettingsResopnse;
import com.cynexis.motoguide.startingactivities.permission.EquipmentAndDisceplineSerializableClass;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AccountInfoActivity extends AppCompatActivity implements WebserviceStartEndEventTracker{

    @BindView(R.id.btnequipment)
    Button btnequipment;
    @BindView(R.id.btndiscipline)
    Button btndiscipline;
    @BindView(R.id.spradius)
    Spinner spradius;
    SlideMenueImplimentationClass _slidingMenu;
    MainApplication _mainApplication;
    Intent _intent;
    Bundle bundle;
    EquipmentAndDisceplineSerializableClass _equipmentAndDisceplineType;
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnsave)
    Button btnsave;
    @BindView(R.id.activity_profile_seting)
    LinearLayout activityProfileSeting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeActivity();
        ButterKnife.bind(this);
        bindControls();

    }

    private void initializeActivity() {
        _mainApplication = (MainApplication) getApplicationContext();
        setContentView(R.layout.activity_profile_seting);
        ButterKnife.bind(this);
        spradius.setSelection(1);
        _equipmentAndDisceplineType = new EquipmentAndDisceplineSerializableClass();
    }

    private void bindControls() {
        ivmenu.setVisibility(View.GONE);
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();


        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("UserID", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.GET_SETTINGS, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(btnsave);
        }
    }

    @OnClick({R.id.btnequipment, R.id.btndiscipline,R.id.ivback, R.id.ivmenu, R.id.btnsave})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnequipment:
                _intent = new Intent(AccountInfoActivity.this, EquipmentTypeActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("DATA", (Serializable) _equipmentAndDisceplineType);
                _intent.putExtras(bundle);
                _intent.putExtra("TYPE", "ACCOUNT_INFO");
                startActivity(_intent);
                break;
            case R.id.btndiscipline:
                _intent = new Intent(AccountInfoActivity.this, DisciplineTypeActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("DATA", (Serializable) _equipmentAndDisceplineType);
                _intent.putExtras(bundle);
                _intent.putExtra("TYPE", "ACCOUNT_INFO");
                startActivity(_intent);
                break;
            case R.id.ivback:
                finish();
                break;
            case R.id.btnsave:
              /*  InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/
                if (_mainApplication.networkManager.CheckNetworkConnection()) {

                    String _jsonDataForEquipmentAndDiscipline = null;
                    if (_equipmentAndDisceplineType.getDiscipline() == null && _equipmentAndDisceplineType.getEquipment() == null) {
                        _mainApplication.dialogManager.DisplayAlertDialog(this, "", "Are you sure to register without Equipment And Discipline Type. If yes all will be selected as your preference.");
                        return;
                    } else {
                        Gson _g = new Gson();
                        _g.toJson(_equipmentAndDisceplineType.toString());
                        // Log.i("Anish1",_g.toJson(_equipmentAndDisceplineType).toString());
                        _jsonDataForEquipmentAndDiscipline = _g.toJson(_equipmentAndDisceplineType).toString();
                    }

                    showProgressDialog();
                    List<DataEntity> _dataToPost = new ArrayList<>();
                    _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
                    _dataToPost.add(new DataEntity("radius",spradius.getSelectedItem().toString()));
                    _dataToPost.add(new DataEntity("additionalinfo",_jsonDataForEquipmentAndDiscipline));
                    AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.UPDATE_ADDITIONAL_INFO_SETTING, _dataToPost, this);


                } else {
                    _mainApplication.messageManager.noInternetConnectionMessage(btnsave);
                }
                break;
        }
    }

 /*   @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }*/

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.getExtras().getSerializable("DATA") != null) {
            _equipmentAndDisceplineType = (EquipmentAndDisceplineSerializableClass) intent.getExtras().getSerializable("DATA");
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.GET_SETTINGS)) {
                GetSettingsResopnse _responce = (GetSettingsResopnse) _object[0];
                List<Integer> _disciplineList = new ArrayList<>();
                List<Integer> _equipmentList = new ArrayList<>();
                if (_responce != null) {
                    for (Discipline _data : _responce.getDiscipline()) {
                        _disciplineList.add(Integer.valueOf(_data.getID()));
                    }

                    for (Equipment _equipment : _responce.getEquipment()) {
                        _equipmentList.add(Integer.valueOf(_equipment.getID()));
                    }

                    _equipmentAndDisceplineType.setDiscipline(_disciplineList);
                    _equipmentAndDisceplineType.setEquipment(_equipmentList);


                    //spradius.setSelection(Integer.valueOf(_responce.getData().get(0).getRadius()));
                } else {
                    _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
                }
            } else {
                JSONObject _response = (JSONObject) _object[0];
                //Log.i("tanu", String.valueOf(_response));
                try {
                    if (_response.getInt("Status") == 1) {
                        _mainApplication.messageManager.DisplayToastMessage(_response.getString("message"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        } else {
            _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
        }

    }

    @Override
    public Context getActivityContext() {
        return this;
    }

}
