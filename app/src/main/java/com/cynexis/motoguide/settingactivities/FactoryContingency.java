package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.filters.FilterRaceEventsActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.factorycontingencyresponse.FactoryConigencyData;
import com.cynexis.motoguide.network.response.factorycontingencyresponse.FactoryContingencyResponse;
import com.cynexis.motoguide.settingactivities.adapter.FactoryContiAdapter;
import com.cynexis.motoguide.settingactivities.adapter.ItemCheckedUnCheckedCalled;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FactoryContingency extends AppCompatActivity implements WebserviceStartEndEventTracker, ItemCheckedUnCheckedCalled {

    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cball)
    CheckBox cball;
    @BindView(R.id.llallequipment)
    LinearLayout llallequipment;
    @BindView(R.id.cbnone)
    CheckBox cbnone;
    @BindView(R.id.llcontainer)
    LinearLayout llcontainer;
    @BindView(R.id.btnsave)
    Button btnsave;
    @BindView(android.R.id.list)
    ListView list;


    public SparseBooleanArray mChecked;
    Bundle _extra;
    FilterSerializableForRaceEventsClass _arrayOfEvents;


    MainApplication _mainApplication;
    List<FactoryConigencyData> _factryitem;
    FactoryContiAdapter factoryContiAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra = this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        mChecked = new SparseBooleanArray();


        initializeActivity();
        bindControls();
    }

    private void initializeActivity()
    {
        _arrayOfEvents = (FilterSerializableForRaceEventsClass) _extra.getSerializable("FILTER");
        setContentView(R.layout.activity_factory_contingency);
        ButterKnife.bind(this);
        ivback.setVisibility(View.VISIBLE);
    }

    private void bindControls() {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("", ""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.FACTORY_CONTINGENCY, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(list);

        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if(_object != null)
        {
            FactoryContingencyResponse _response = (FactoryContingencyResponse) _object[0];
            if (_response != null)
            {

                if (_response.getStatus() == 1)
                {
                    _factryitem = _response.getData();
                    if (_response.getData() != null && _response.getData().size() > 0)
                    {
                        if (_arrayOfEvents != null)
                        {
                            if (_arrayOfEvents.getFactory() != null && _arrayOfEvents.getFactory().size() > 0)
                            {
                                for (int i = 0; i < _factryitem.size(); i++)
                                {
                                    if (_arrayOfEvents.getFactory().contains(_factryitem.get(i).getFactoryName()))
                                    {
                                        mChecked.put(i, true);
                                    }
                                    else
                                    {
                                        mChecked.put(i, false);
                                    }
                                }
                            }
                        }

                        factoryContiAdapter = new FactoryContiAdapter(this, _factryitem,this,mChecked);
                        list.setAdapter(factoryContiAdapter);

                    }
                }
                else
                {
                    //_mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                }

            }
        }
        else
        {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }

    }

    @Override
    public void getItemCheckedUnChecked(int position, boolean ischecked)
    {
        mChecked.put(position, ischecked);
        if (mChecked.size() == _factryitem.size())
        {
            if (isAllValuesChecked())
            {
                cball.setChecked(true);
                cbnone.setChecked(false);
            }
            else if (isAllValuesUnChecked())
            {

                cball.setChecked(false);
                cbnone.setChecked(false);
            }
            else
            {
                cball.setChecked(false);
                cbnone.setChecked(true);
            }
        }
        else
        {
            cball.setChecked(false);
            cbnone.setChecked(false);
        }
    }

    @Override
    public boolean getItemCheckedUnCheckedByPosition(int position) {

        if (mChecked != null && mChecked.size() > 0) {
            ArrayList<String> _allCheckedIDs = new ArrayList<>();
            for (int i = 0; i < mChecked.size(); i++)
            {
                if (mChecked.get(i))
                {
                    _allCheckedIDs.add(_factryitem.get(i).getFactoryName());
                    //Log.i("Anish", _factryitem.get(i).getFactoryName());
                }
            }
            _arrayOfEvents.setFactory(_allCheckedIDs);
        }

        return mChecked.get(position);
    }

    protected boolean isAllValuesChecked() {

        for (int i = 0; i < mChecked.size(); i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    protected boolean isAllValuesUnChecked() {

        for (int i = 0; i < mChecked.size(); i++) {
            if (mChecked.get(i)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean showChekBox() {
        return false;
    }

    @OnClick({R.id.ivback, R.id.cball, R.id.cbnone, R.id.btnsave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.cball:
                cbnone.setChecked(false);
                cball.setChecked(true);
                cball.setBackgroundResource(R.drawable.allcheck);
                cbnone.setBackgroundResource(R.drawable.noneuncheck);

                for (int i = 0; i < _factryitem.size(); i++) {
                    mChecked.put(i, true);
                }
                factoryContiAdapter.notifyDataSetChanged();
                break;

            case R.id.cbnone:
                cbnone.setChecked(true);
                cball.setChecked(false);

                cball.setBackgroundResource(R.drawable.alluncheck);
                cbnone.setBackgroundResource(R.drawable.nonecheck);

                for (int i = 0; i < _factryitem.size(); i++) {
                    mChecked.put(i, false);
                }
                factoryContiAdapter.notifyDataSetChanged();
                break;

            case R.id.btnsave:
                if (mChecked != null && mChecked.size() > 0) {
                    ArrayList<String> _allCheckedName = new ArrayList<>();
                    String _messageToShow = "";
                    for (int i = 0; i < mChecked.size(); i++)
                    {
                        if (mChecked.get(i)) {
                            _allCheckedName.add(_factryitem.get(i).getFactoryName()); // Containing Name
                            //Log.i("Anish", _factryitem.get(i).getFactoryName());
                            _messageToShow =_messageToShow+","+ _factryitem.get(i).getFactoryName();
                        }
                    }
                    if(_messageToShow.trim().length() != 0)
                    {
                        if(_messageToShow.startsWith(","))
                        {
                            _messageToShow = _messageToShow.substring(1,_messageToShow.length());
                        }

                        if(_messageToShow.endsWith(","))
                        {
                            _messageToShow = _messageToShow.substring(0,_messageToShow.length()-1);
                        }

                    }
                   // _mainApplication.messageManager.DisplayToastMessageAtCenter(_messageToShow);
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_FACTORY_CONTIGENCY,_messageToShow);
                    _arrayOfEvents.setFactory(_allCheckedName);
                }
                Intent _intent = new Intent(this, FilterRaceEventsActivity.class);
                _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                Bundle bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                finish();

                break;
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
