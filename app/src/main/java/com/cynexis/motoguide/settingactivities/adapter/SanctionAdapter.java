package com.cynexis.motoguide.settingactivities.adapter;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.network.response.sanctionresponce.SanctionData;


import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 25-05-2017.
 */

public class SanctionAdapter extends BaseAdapter {

    Context _ctx;
    List<SanctionData> _sanctionitemlist;
    ItemCheckedUnCheckedCalled _internFace;
    SparseBooleanArray _selctedPositon;
    LayoutInflater _inflater = null;

    public SanctionAdapter(Context _ctx, List<SanctionData> _sanctionitemlist, ItemCheckedUnCheckedCalled _internFace, SparseBooleanArray selctedPositon) {
        this._ctx = _ctx;
        this._sanctionitemlist = _sanctionitemlist;
        this._internFace = _internFace;
        _inflater = (LayoutInflater)_ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this._selctedPositon = selctedPositon;
    }

    @Override
    public int getCount() {
        return _sanctionitemlist.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        ViewHoldertwo holder;
        if (view != null) {
            holder = (ViewHoldertwo) view.getTag();
        } else {
            view = _inflater.inflate(R.layout.custom_discipline_list, parent, false);
            holder = new ViewHoldertwo(view);
            view.setTag(holder);
        }
        holder.tvname.setText(this._sanctionitemlist.get(position).getSanctionName());

        /////////////////////////////////////////////// LOGIC FOR CHECK UNCHECK
        if(_sanctionitemlist != null)
        {
            if(_selctedPositon.get(position))
            {
                _internFace.getItemCheckedUnChecked(position, true);
            }
            else
            {
                _internFace.getItemCheckedUnChecked(position, false);
            }
        }
        else
        {
            _internFace.getItemCheckedUnChecked(position, false);
        }
        ////////////////////////////////////////////////////////////////////////

        if(_sanctionitemlist != null)
        {
            holder.tvname.setText(this._sanctionitemlist.get(position).getSanctionName());
        }
        else
        {
            holder.tvname.setText(this._sanctionitemlist.get(position).getSanctionName());
        }

        holder.switchOnOff.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    _internFace.getItemCheckedUnChecked(position, isChecked);
                }
                else
                {
                    _internFace.getItemCheckedUnChecked(position, isChecked);
                }

            }
        });
        holder.switchOnOff.setChecked((_internFace.getItemCheckedUnCheckedByPosition(position) == true ? true : false));
        return view;
    }

    static class ViewHoldertwo {
        @BindView(R.id.tvname)
        TextView tvname;
        @BindView(R.id.switchOnOff)
        SwitchCompat switchOnOff;

        public ViewHoldertwo(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
