package com.cynexis.motoguide.settingactivities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.filters.FilterRaceEventsActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.equipmentresponse.EquipmentData;
import com.cynexis.motoguide.network.response.equipmentresponse.EquipmentResponse;
import com.cynexis.motoguide.settingactivities.adapter.EquipmentDisciplineAdapter;
import com.cynexis.motoguide.settingactivities.adapter.ItemCheckedUnCheckedCalled;
import com.cynexis.motoguide.startingactivities.permission.EquipmentAndDisceplineSerializableClass;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EquipmentTypeActivity extends AppCompatActivity implements WebserviceStartEndEventTracker,ItemCheckedUnCheckedCalled {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cball)
    CheckBox cball;
    @BindView(R.id.cbnone)
    CheckBox cbnone;
    @BindView(R.id.lvequipment)
    ListView lvequipment;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.llallequipment)
    LinearLayout llallequipment;
    @BindView(R.id.btnsave)
    Button btnsave;

    MainApplication _mainApplication;
    List<EquipmentData> _equipmentitem;
    EquipmentDisciplineAdapter equipmentAdapter;
    public SparseBooleanArray mChecked;
    Bundle _extra;
    EquipmentAndDisceplineSerializableClass _dataToSend;
    FilterSerializableForRaceEventsClass _arrayOfEvents;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra =  this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        mChecked = new SparseBooleanArray();
        setContentView(R.layout.activity_equipment_type);
        ButterKnife.bind(this);
        initializeActivity();
        bindControls();
    }

    private void initializeActivity() {
        cbnone.setChecked(true);
        if(_extra.getSerializable("DATA") != null)
        {
            _dataToSend = (EquipmentAndDisceplineSerializableClass)_extra.getSerializable("DATA");
        }
        else
        {
            _arrayOfEvents = (FilterSerializableForRaceEventsClass)_extra.getSerializable("FILTER");
            btnsave.setText("APPLY");
        }
    }

    private void bindControls() {
        ivmenu.setVisibility(View.GONE);
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("", ""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.EQUIPMENT, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(lvequipment);

        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();

        if(_object != null)
        {
            EquipmentResponse _response = (EquipmentResponse) _object[0];
            if(_response != null)
            {
                if(_response.getStatus() == 1)
                {
                    if(_response.getData()!= null && _response.getData().size() >0)
                    {
                        _equipmentitem  = _response.getData();
                        if(_dataToSend != null)
                        {
                            if(_dataToSend.getEquipment() != null && _dataToSend.getEquipment().size() >0)
                            {
                                for (int i = 0 ; i < _equipmentitem.size() ; i++)
                                {
                                    if(_dataToSend.getEquipment().contains(Integer.valueOf(_equipmentitem.get(i).getID())))
                                    {
                                        mChecked.put(i,true);
                                    }
                                    else
                                    {
                                        mChecked.put(i,false);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if(_arrayOfEvents.getEquipment() != null && _arrayOfEvents.getEquipment().size() >0)
                            {
                                for (int i = 0 ; i < _equipmentitem.size() ; i++)
                                {
                                    if(_arrayOfEvents.getEquipment().contains(_equipmentitem.get(i).getEquipmentName().trim()))
                                    {
                                        mChecked.put(i,true);
                                    }
                                    else
                                    {
                                        mChecked.put(i,false);
                                    }
                                }
                            }
                            else
                            {
                                for (int i = 0 ; i < _equipmentitem.size() ; i++)
                                {
                                   mChecked.put(i,true);

                                }
                            }
                        }
                        equipmentAdapter = new EquipmentDisciplineAdapter(this,_equipmentitem, this,mChecked);
                        lvequipment.setAdapter(equipmentAdapter);
                    }
                }
                else
                {
                   // _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                }
            }
            else
            {
                _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
            }
        }

        else
        {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }

    }

    @OnClick({R.id.ivback, R.id.cball, R.id.cbnone, R.id.btnsave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.cball:
                cbnone.setChecked(false);
                cball.setChecked(true);
                cball.setBackgroundResource(R.drawable.allcheck);
                cbnone.setBackgroundResource(R.drawable.noneuncheck);

                for (int i = 0; i < _equipmentitem.size() ; i++)
                {
                    mChecked.put(i, true);
                }
                equipmentAdapter.notifyDataSetChanged();
                break;

            case R.id.cbnone:
                cbnone.setChecked(true);
                cball.setChecked(false);

                cball.setBackgroundResource(R.drawable.alluncheck);
                cbnone.setBackgroundResource(R.drawable.nonecheck);

                for (int i = 0; i < _equipmentitem.size() ; i++)
                {
                    mChecked.put(i, false);
                }
                equipmentAdapter.notifyDataSetChanged();
                break;

            case R.id.btnsave:
                if(_extra.getSerializable("DATA") != null)
                {
                    if(mChecked != null && mChecked.size() > 0)
                    {
                        ArrayList<Integer> _allCheckedIDs = new ArrayList<>();
                        for (int i = 0; i < mChecked.size() ; i++)
                        {
                            if(mChecked.get(i))
                            {
                                _allCheckedIDs.add(Integer.valueOf(_equipmentitem.get(i).getID()));
                               //Log.i("Anish",_equipmentitem.get(i).getID());
                            }
                        }
                        _dataToSend.setEquipment(_allCheckedIDs);
                    }
                    if(_extra.getString("TYPE") != null)
                    {
                        Intent _intent =  new Intent(this, AccountInfoActivity.class);
                        _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("DATA", (Serializable) _dataToSend);
                        _intent.putExtras(bundle);
                        startActivity(_intent);
                        finish();
                    }
                    else
                    {
                        Intent _intent =  new Intent(this, TypeChooserActivity.class);
                        _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("DATA", (Serializable) _dataToSend);
                        _intent.putExtras(bundle);
                        startActivity(_intent);
                        finish();
                    }
                }
                else
                {
                    if(mChecked != null && mChecked.size() > 0)
                    {
                        ArrayList<String> _allCheckedName = new ArrayList<>();
                        for (int i = 0; i < mChecked.size() ; i++)
                        {
                            if(mChecked.get(i))
                            {
                                _allCheckedName.add(_equipmentitem.get(i).getEquipmentName().trim()); // Containing Name
                                //Log.i("Anish",_equipmentitem.get(i).getEquipmentName());
                            }
                        }
                        _arrayOfEvents.setEquipment(_allCheckedName);
                    }
                    Intent _intent =  new Intent(this, FilterRaceEventsActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                    _intent.putExtras(bundle);
                    startActivity(_intent);
                    finish();
                }

                break;
        }
    }



    @Override
    public void getItemCheckedUnChecked(int position, boolean ischecked) {
        mChecked.put(position, ischecked);
        if(mChecked.size() == _equipmentitem.size())
        {
            if (isAllValuesChecked())
            {
                cball.setChecked(true);
                cbnone.setChecked(false);
            }
            else if(isAllValuesUnChecked())
            {

                cball.setChecked(false);
                cbnone.setChecked(false);
            }
            else
            {
                cball.setChecked(false);
                cbnone.setChecked(true);
            }
        }
        else
        {
            cball.setChecked(false);
            cbnone.setChecked(false);
        }
    }

    @Override
    public boolean getItemCheckedUnCheckedByPosition(int position) {

        if(mChecked != null && mChecked.size() > 0)
        {
            if(_dataToSend != null)
            {
                ArrayList<Integer> _allCheckedIDs = new ArrayList<>();
                for (int i = 0; i < mChecked.size() ; i++)
                {
                    if(mChecked.get(i))
                    {
                        _allCheckedIDs.add(Integer.valueOf(_equipmentitem.get(i).getID()));
                        //Log.i("Anish",_equipmentitem.get(i).getID());
                    }
                }
                _dataToSend.setEquipment(_allCheckedIDs);
            }
            else
            {
                ArrayList<String> _allCheckedIDs = new ArrayList<>();
                for (int i = 0; i < mChecked.size() ; i++)
                {
                    if(mChecked.get(i))
                    {
                        _allCheckedIDs.add(_equipmentitem.get(i).getEquipmentName());
                        //Log.i("Anish",_equipmentitem.get(i).getEquipmentName());
                    }
                }
                _arrayOfEvents.setEquipment(_allCheckedIDs);
            }
        }

        return mChecked.get(position);
    }

    protected boolean isAllValuesChecked() {

        for (int i = 0; i < mChecked.size(); i++)
        {
            if (!mChecked.get(i))
            {
                return false;
            }
        }

        return true;
    }

    protected boolean isAllValuesUnChecked() {

        for (int i = 0; i < mChecked.size(); i++)
        {
            if (mChecked.get(i))
            {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean showChekBox() {
        return false;
    }

    @Override
    public Context getActivityContext() {
        return null;
    }
}
