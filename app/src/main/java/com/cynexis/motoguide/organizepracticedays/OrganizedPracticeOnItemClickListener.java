package com.cynexis.motoguide.organizepracticedays;

/**
 * Created by  on 02-01-2020.
 */
public interface OrganizedPracticeOnItemClickListener {
    public void organizedPracticeOnItemClick(int position);
}
