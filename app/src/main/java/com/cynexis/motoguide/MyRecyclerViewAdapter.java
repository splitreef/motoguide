package com.cynexis.motoguide;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Anish on 22/06/17.
 */

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private ArrayList<HashMap<String,String>> mData;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;
    Context ctx;

    // data is passed into the constructor
    public
    MyRecyclerViewAdapter(Context context, ArrayList<HashMap<String,String>> data) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        ctx = context;
    }

    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_facility_imagesvideos, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        if (mData.get(position)!= null && mData.get(position).get("ITEM").length() > 0) {
            if(mData.get(position).get("TYPE").equals("VIDEO"))
            {
                holder.ivplay.setVisibility(View.VISIBLE);
                Picasso.with(ctx)
                        .load(getYoutubeThumbnailUrlFromVideoUrl(mData.get(position).get("ITEM")))
                        .placeholder(R.mipmap.logo)
                        .error(android.R.drawable.stat_notify_error)
                        .into(holder.ivCoverpic);
                //holder.ivCoverpic.setImageURI(Uri.parse(getYoutubeThumbnailUrlFromVideoUrl(mData.get(position))));
            }
            else
            {
                holder.ivplay.setVisibility(View.GONE);
                Picasso.with(ctx)
                        .load(mData.get(position).get("ITEM"))
                        .placeholder(R.mipmap.logo)
                        .error(android.R.drawable.stat_notify_error)
                        .into(holder.ivCoverpic);
               // holder.ivCoverpic.setImageURI(Uri.parse(mData.get(position)));
            }
        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return mData.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView ivCoverpic;
        public ImageView ivplay;
        public FrameLayout llframecontainer;
        public ViewHolder(View itemView) {
            super(itemView);
            ivCoverpic = (ImageView) itemView.findViewById(R.id.ivimagefrasco);
            ivplay = (ImageView) itemView.findViewById(R.id.ivplay);
            llframecontainer = (FrameLayout) itemView.findViewById(R.id.llframecontainer);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    public String getItem(int id) {
        return mData.get(id).get("ITEM");
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }


    public static String getYoutubeThumbnailUrlFromVideoUrl(String videoUrl) {
        String imgUrl = "http://img.youtube.com/vi/"+getYoutubeVideoIdFromUrl(videoUrl) + "/0.jpg";
        return imgUrl;
    }

    public static String getYoutubeVideoIdFromUrl(String inUrl) {
        if (inUrl.toLowerCase().contains("youtu.be")) {
            return inUrl.substring(inUrl.lastIndexOf("/") + 1);
        }
        String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(inUrl);
        if (matcher.find()) {
            return matcher.group();
        }
        return null;
    }


}
