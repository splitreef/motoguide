package com.cynexis.motoguide.filters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cynexis.motoguide.DashBoardActivity;
import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonSpinnerAdapter;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.dateManager.DynamicControlsForDate;
import com.cynexis.motoguide.dateManager.IDateConsumer;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;
import com.cynexis.motoguide.mycalendar.MyCalendarActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.organizepracticedays.OrganizedPracticeDaysActivity;
import com.cynexis.motoguide.searchallraces.SearchActivity;
import com.cynexis.motoguide.searchallraces.SearchAllRacesActivity;
import com.cynexis.motoguide.settingactivities.EquipmentTypeActivity;
import com.cynexis.motoguide.settingactivities.FactoryContingency;
import com.cynexis.motoguide.settingactivities.SanctionType;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterRaceEventsActivity extends AppCompatActivity implements WebserviceStartEndEventTracker, IDateConsumer {


    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.spradius)
    Spinner spradius;
    @BindView(R.id.spstate)
    Spinner spstate;
    @BindView(R.id.btnapply)
    Button btnapply;
    @BindView(R.id.btnreset)
    Button btnreset;
    @BindView(R.id.tvenddate)
    TextView tvenddate;
    @BindView(R.id.tvstartdate)
    TextView tvstartdate;

    Bundle _extra;
    Intent _intent;

    @BindView(R.id.rlequipmenttype)
    RelativeLayout rlequipmenttype;
    @BindView(R.id.disciplinetype)
    RelativeLayout disciplinetype;
    @BindView(R.id.rlsanctiontype)
    RelativeLayout rlsanctiontype;
    @BindView(R.id.rlfactorycontigency)
    RelativeLayout rlfactorycontigency;


    ArrayList<String> _Stateitem, _StateID;
    FilterSerializableForRaceEventsClass _arrayOfEvents;
    Bundle bundle;
    MainApplication _mainApplication;
    @BindView(R.id.rlstartdatecontainer)
    RelativeLayout rlstartdatecontainer;
    @BindView(R.id.rlenddatecontainer)
    RelativeLayout rlenddatecontainer;

    DynamicControlsForDate _dateControl;
    @BindView(R.id.bottomnavigation)
    BottomNavigationView bottomnavigation;
    @BindView(R.id.view_sanction)
    View viewSanction;
    @BindView(R.id.view_factory)
    View viewFactory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        _extra = this.getIntent().getExtras();
        initializeActivity();
        bindControls();
    }

    private void initializeActivity() {
        setContentView(R.layout.activity_filter_race_events);
        ButterKnife.bind(this);
        ivmenu.setVisibility(View.GONE);
        bottomnavigation.setVisibility(View.GONE);
        _arrayOfEvents = (FilterSerializableForRaceEventsClass) _extra.getSerializable("FILTER");

      /*  tvstartdate.setText(_arrayOfEvents.getStartdate());
        tvenddate.setText(_arrayOfEvents.getEnddate());*/
        setCurrentDate();
        setEndDate();

        if (_arrayOfEvents.getType().equals("practiceday")) {
            rlsanctiontype.setVisibility(View.GONE);
            rlfactorycontigency.setVisibility(View.GONE);
            viewSanction.setVisibility(View.GONE);
            viewFactory.setVisibility(View.GONE);
        }
    }

    /**
     * set start date as current date
     */
    private void setCurrentDate() {
        String date = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date());
        tvstartdate.setText(date);
        tvstartdate.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
        tvstartdate.setTypeface(null, Typeface.BOLD);

    }

    /**
     * set end date after 30 days from current date
     */
    private void setEndDate() {
         String pattern = "MM-dd-yyyy";
        String strStartDate =new SimpleDateFormat(pattern).format(new Date());
        //        String strStartDate = new SimpleDateFormat("MM-dd-yyyy", Locale.getDefault()).format(new Date());


        Date startDate = null;
        /**
         * String to date converter
         */
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        try {
            startDate = sdf.parse(strStartDate);
            System.out.println(startDate);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Calendar c = Calendar.getInstance();
        c.setTime(startDate);
        c.add(Calendar.DATE, 30);
        Date endDate = c.getTime();
        String strEndDate = new SimpleDateFormat("MM-dd-yyyy").format(endDate);


        endDate = null;
        /**
         * String to date converter
         */
        SimpleDateFormat sdf1 = new SimpleDateFormat("MM-dd-yyyy");
        try {
            endDate = sdf1.parse(strEndDate);
            System.out.println(endDate);
        } catch (Exception e) {
            e.printStackTrace();
        }



        if(startDate.before(endDate)){
            // Your time expired do your logic here.
            tvenddate.setText(""+strEndDate);
            tvenddate.setTextColor(this.getResources().getColor(R.color.colorPrimaryDark));
            tvenddate.setTypeface(null, Typeface.BOLD);

        }


    }

    private void bindControls() {
        // Bottom bar navigation
        bottomnavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        // Write code to perform some actions.
                        switch (item.getItemId()) {
                            case R.id.action_search:
                                //Log.i("Anish", "1");
                                _intent = new Intent(FilterRaceEventsActivity.this, SearchActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_home:
                                //Log.i("Anish", "2");
                                _intent = new Intent(FilterRaceEventsActivity.this, DashBoardActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_calendar:
                                //Log.i("Anish", "3");
                                _intent = new Intent(FilterRaceEventsActivity.this, MyCalendarActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;
                        }

                        return false;
                    }
                });


        List<String> _arrayOfMiles = Arrays.asList(getResources().getStringArray(R.array.array_miles));
        CommonSpinnerAdapter _spinneradapter = new CommonSpinnerAdapter(FilterRaceEventsActivity.this, android.R.id.text1, _arrayOfMiles);
        _spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spradius.setAdapter(_spinneradapter);
        if (_extra != null) {
            spradius.setSelection(_extra.getInt("POSITION"));
        }

        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.STATE, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(btnapply);

        }
    }


    @OnClick({R.id.rlequipmenttype, R.id.disciplinetype, R.id.rlsanctiontype, R.id.rlfactorycontigency, R.id.rlstartdatecontainer, R.id.rlenddatecontainer, R.id.ivback, R.id.btnapply, R.id.btnreset})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlequipmenttype:
                _intent = new Intent(this, EquipmentTypeActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                break;
            case R.id.disciplinetype:
                _intent = new Intent(this, DisciplineFilterActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                break;
            case R.id.rlsanctiontype:
                _intent = new Intent(this, SanctionType.class);
                bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                break;
            case R.id.rlfactorycontigency:
                if (_arrayOfEvents.getFactory() == null || _arrayOfEvents.getFactory().size() < 0) {
                    if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_FACTORY_CONTIGENCY) != null &&
                            _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_FACTORY_CONTIGENCY).length() > 0) {
                        String ARRAY[] = _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_FACTORY_CONTIGENCY).split(",");

                        ArrayList<String> _selectedData = new ArrayList<>();
                        for (int i = 0; i < ARRAY.length; i++) {
                            _selectedData.add(ARRAY[i]);
                        }

                        _arrayOfEvents.setFactory(_selectedData);
                    }
                }
                _intent = new Intent(this, FactoryContingency.class);
                bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                break;
            case R.id.rlstartdatecontainer:
                _dateControl = new DynamicControlsForDate(this, tvstartdate, tvenddate, this);
                _dateControl.invokeDatePicker();
                break;
            case R.id.rlenddatecontainer:
                _dateControl = new DynamicControlsForDate(this, tvenddate, null, this);
                _dateControl.invokeDatePicker();
                break;
            case R.id.ivback:
                finish();
                break;
            case R.id.btnapply:
                _arrayOfEvents.setStartdate(tvstartdate.getText().toString());
                _arrayOfEvents.setEnddate(tvenddate.getText().toString());
                _arrayOfEvents.setState(spstate.getSelectedItem().toString());
                _arrayOfEvents.setMiles(spradius.getSelectedItem().toString());
                if (_arrayOfEvents.getType().equalsIgnoreCase("Event")) {
                    _intent = new Intent(this, SearchAllRacesActivity.class);
                } else {
                    _intent = new Intent(this, OrganizedPracticeDaysActivity.class);
                }

                if (spstate.getSelectedItemPosition() != 0) {
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE, _StateID.get(spstate.getSelectedItemPosition()));
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE_NAME, _Stateitem.get(spstate.getSelectedItemPosition()));
                } else {
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE, "");
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE_NAME, "");
                }

                _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                Bundle bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                startActivity(_intent);
                finish();
                break;
            case R.id.btnreset:
                _arrayOfEvents.setDiscipline(new ArrayList<String>());
                _arrayOfEvents.setEquipment(new ArrayList<String>());
                _arrayOfEvents.setSanction(new ArrayList<String>());
                _arrayOfEvents.setFactory(new ArrayList<String>());
                _arrayOfEvents.setStartdate("");
                _arrayOfEvents.setEnddate("");
                tvstartdate.setText("Start");
                tvenddate.setText("End");
                spstate.setSelection(0);
                spradius.setSelection(1);
                // finish();
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.getExtras().getSerializable("FILTER") != null) {
            _arrayOfEvents = (FilterSerializableForRaceEventsClass) intent.getExtras().getSerializable("FILTER");
        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        JSONObject _data = (JSONObject) _object[0];
        //Log.i("tanvi", String.valueOf(_data));
        if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.STATE)) {
            if (_data != null) {
                try {
                    if (_data.getInt("Status") == 1) {
                        _Stateitem = new ArrayList<>();
                        _Stateitem.add("STATE");
                        _StateID = new ArrayList<>();
                        _StateID.add("");
                        for (int i = 0; i < _data.getJSONArray("data").length(); i++) {
                            _StateID.add(i + 1, _data.getJSONArray("data").getJSONObject(i).getString("state_id"));
                            _Stateitem.add(i + 1, _data.getJSONArray("data").getJSONObject(i).getString("state_name"));
                        }

                        CommonSpinnerAdapter _spinneradapter = new CommonSpinnerAdapter(FilterRaceEventsActivity.this, android.R.id.text1, _Stateitem);
                        _spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spstate.setAdapter(_spinneradapter);

                        if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE) != null && _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE).length() > 0) {
                            int _selectedIndex = _StateID.indexOf(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE));
                            spstate.setSelection(_selectedIndex);
                        }

                        List<String> _disciplineList = new ArrayList<>();
                        List<String> _equipmentList = new ArrayList<>();

                        for (int i = 0; i < _data.getJSONArray("discipline").length(); i++) {
                            _disciplineList.add(_data.getJSONArray("discipline").getJSONObject(i).getString("DisciplineName"));
                        }

                        for (int i = 0; i < _data.getJSONArray("equipment").length(); i++) {
                            _equipmentList.add(_data.getJSONArray("equipment").getJSONObject(i).getString("EquipmentName"));
                        }

                        _arrayOfEvents.setDiscipline(_disciplineList);
                        _arrayOfEvents.setEquipment(_equipmentList);
                    } else {
                        _mainApplication.messageManager.DisplayToastMessage(_data.getString("data"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
            }

        }
    }


    @Override
    public void getDate(String startDate, String endDate) {

        if (endDate != null) {
            tvstartdate.setText(startDate);
            tvenddate.setText(endDate);
        }

    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
