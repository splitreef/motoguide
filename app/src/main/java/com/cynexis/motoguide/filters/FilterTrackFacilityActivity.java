package com.cynexis.motoguide.filters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.cynexis.motoguide.DashBoardActivity;
import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonSpinnerAdapter;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;
import com.cynexis.motoguide.filters.properties.FilterSerializableForTrackFacilityClass;
import com.cynexis.motoguide.mycalendar.MyCalendarActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.organizepracticedays.OrganizedPracticeDaysActivity;
import com.cynexis.motoguide.searchallraces.SearchActivity;
import com.cynexis.motoguide.wheretoride.TrackFacilityActivity;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FilterTrackFacilityActivity extends AppCompatActivity implements WebserviceStartEndEventTracker {


    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.btnapply)
    Button btnapply;
    @BindView(R.id.btnreset)
    Button btnreset;
    @BindView(R.id.spradius)
    Spinner spradius;
    @BindView(R.id.spstate)
    Spinner spstate;
    @BindView(R.id.disciplinetype)
    RelativeLayout disciplinetype;
    @BindView(R.id.tv_filter_track_facitility)
    TextView tv_filter_track_facitility;



    MainApplication _mainApplication;
    ArrayList<String> _Stateitem, _StateID;
    Bundle _extra;
    Intent _intent;
    @BindView(R.id.bottomnavigation)
    BottomNavigationView bottomnavigation;
    FilterSerializableForTrackFacilityClass _arrayOfEvents;
    Bundle bundle;
    private String from = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra = this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        _arrayOfEvents = new FilterSerializableForTrackFacilityClass();
        initalizeActivity();
        bindControls();
    }

    private void initalizeActivity() {
        setContentView(R.layout.activity_filter_track_facility);
        ButterKnife.bind(this);
        ivmenu.setVisibility(View.GONE);
        bottomnavigation.setVisibility(View.GONE);
    }

    private void bindControls() {
        // Bottom bar navigation
        bottomnavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        // Write code to perform some actions.
                        switch (item.getItemId()) {
                            case R.id.action_search:
                                //Log.i("Anish","1");
                                _intent = new Intent(FilterTrackFacilityActivity.this, SearchActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_home:
                                //Log.i("Anish","2");
                                _intent = new Intent(FilterTrackFacilityActivity.this, DashBoardActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_calendar:
                                //Log.i("Anish","3");
                                _intent = new Intent(FilterTrackFacilityActivity.this, MyCalendarActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;
                        }

                        return false;
                    }
                });

        List<String> _arrayOfMiles = Arrays.asList(getResources().getStringArray(R.array.array_miles));
        CommonSpinnerAdapter _spinneradapter = new CommonSpinnerAdapter(FilterTrackFacilityActivity.this, android.R.id.text1, _arrayOfMiles);
        _spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spradius.setAdapter(_spinneradapter);
        if (_extra != null) {
            spradius.setSelection(_extra.getInt("POSITION"));
        }


        if (getIntent().getExtras().getString("from") != null) {
            from = getIntent().getExtras().getString("from");
            tv_filter_track_facitility.setText("FILTER "+getResources().getString(R.string.organized_practice_day));
        }else {
           // tv_filter_track_facitility.setText(getResources().getString(R.string.filter_track_facility));

        }

        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("userid", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.USER_ID)));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.STATE, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(btnapply);

        }


    }


    @OnClick({R.id.ivback, R.id.btnapply, R.id.btnreset, R.id.disciplinetype})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.btnapply:
                Intent _intent;
                if (from.equalsIgnoreCase("practice_day")) {
                    if (_arrayOfEvents != null) {
                        if (_arrayOfEvents.getDiscipline() != null) {
                            //Log.i("Anish",_arrayOfEvents.getDiscipline().toString().replace("[","").replace("]","").trim());
                        }
                    }
                    if (spstate.getSelectedItemPosition() > 0) {
                        _arrayOfEvents.setState(spstate.getSelectedItem().toString());
                    }

                    if (spstate.getSelectedItemPosition() != 0) {
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE, _StateID.get(spstate.getSelectedItemPosition()));
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE_NAME, _Stateitem.get(spstate.getSelectedItemPosition()));
                    } else {
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE, "");
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE_NAME, "");
                    }
                    _arrayOfEvents.setMiles(spradius.getSelectedItem().toString());
                    _intent = new Intent(this, OrganizedPracticeDaysActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    bundle = new Bundle();
                    bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                    _intent.putExtras(bundle);
                    _intent.putExtra("IS_TRACK_FACILITY", true);
                    startActivity(_intent);
                    finish();
                } else {
                    if (_arrayOfEvents != null) {
                        if (_arrayOfEvents.getDiscipline() != null) {
                            //Log.i("Anish",_arrayOfEvents.getDiscipline().toString().replace("[","").replace("]","").trim());
                        }
                    }
                    if (spstate.getSelectedItemPosition() > 0) {
                        _arrayOfEvents.setState(spstate.getSelectedItem().toString());
                    }

                    if (spstate.getSelectedItemPosition() != 0) {
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE, _StateID.get(spstate.getSelectedItemPosition()));
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE_NAME, _Stateitem.get(spstate.getSelectedItemPosition()));
                    } else {
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE, "");
                        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.SELECTED_STATE_NAME, "");
                    }
                    _arrayOfEvents.setMiles(spradius.getSelectedItem().toString());
                    _intent = new Intent(this, TrackFacilityActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    bundle = new Bundle();
                    bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                    _intent.putExtras(bundle);
                    _intent.putExtra("IS_TRACK_FACILITY", true);
                    startActivity(_intent);
                    finish();
                }
                break;
            case R.id.btnreset:
                _arrayOfEvents.setDiscipline(new ArrayList<String>());
                spstate.setSelection(0);
                spradius.setSelection(1);
                break;
            case R.id.disciplinetype:
                _intent = new Intent(this, DisciplineFilterActivity.class);
                bundle = new Bundle();
                bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                _intent.putExtras(bundle);
                _intent.putExtra("IS_TRACK_FACILITY", true);
                startActivity(_intent);
                break;
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.getExtras().getSerializable("FILTER") != null) {
            _arrayOfEvents = (FilterSerializableForTrackFacilityClass) intent.getExtras().getSerializable("FILTER");
        }



    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        JSONObject _data = (JSONObject) _object[0];
        if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.STATE)) {
            if (_data != null) {
                try {
                    if (_data.getInt("Status") == 1) {
                        _Stateitem = new ArrayList<>();
                        _Stateitem.add("STATE");
                        _StateID = new ArrayList<>();
                        _StateID.add("");
                        for (int i = 0; i < _data.getJSONArray("data").length(); i++) {
                            _StateID.add(i + 1, _data.getJSONArray("data").getJSONObject(i).getString("state_id"));
                            _Stateitem.add(i + 1, _data.getJSONArray("data").getJSONObject(i).getString("state_name"));
                        }
                        CommonSpinnerAdapter _spinneradapter = new CommonSpinnerAdapter(FilterTrackFacilityActivity.this, android.R.id.text1, _Stateitem);
                        _spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spstate.setAdapter(_spinneradapter);

                        if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE) != null && _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE).length() > 0) {
                            int _selectedIndex = _StateID.indexOf(_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE));
                            spstate.setSelection(_selectedIndex);
                        }
                        List<String> _disciplineList = new ArrayList<>();
                        for (int i = 0; i < _data.getJSONArray("discipline").length(); i++) {
                            _disciplineList.add(_data.getJSONArray("discipline").getJSONObject(i).getString("DisciplineName"));
                        }
                        _arrayOfEvents.setDiscipline(_disciplineList);

                    } else {
                        _mainApplication.messageManager.DisplayToastMessage(_data.getString("data"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                _mainApplication.messageManager.DisplayToastMessageAtCenter(CommonUtility.ERROR_OCCURED);
            }
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
