package com.cynexis.motoguide.filters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;
import com.cynexis.motoguide.filters.properties.FilterSerializableForTrackFacilityClass;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.disciplineresponse.DisciplineData;
import com.cynexis.motoguide.network.response.disciplineresponse.DisciplineResponse;
import com.cynexis.motoguide.settingactivities.adapter.EquipmentDisciplineAdapter;
import com.cynexis.motoguide.settingactivities.adapter.ItemCheckedUnCheckedCalled;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DisciplineFilterActivity extends AppCompatActivity implements WebserviceStartEndEventTracker, ItemCheckedUnCheckedCalled {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.cball)
    CheckBox cball;
    @BindView(R.id.cbnone)
    CheckBox cbnone;
    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.llallequipment)
    LinearLayout llallequipment;
    @BindView(R.id.btnsave)
    Button btnsave;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(android.R.id.list)
    ListView list;

    MainApplication _mainApplication;
    List<DisciplineData> _disciplineitem;
    EquipmentDisciplineAdapter equipmentAdapter;
    public SparseBooleanArray mChecked;
    Bundle _extra;
    FilterSerializableForRaceEventsClass _arrayOfEvents;
    FilterSerializableForTrackFacilityClass _arrayOfDiscipline;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _extra = this.getIntent().getExtras();
        _mainApplication = (MainApplication) getApplicationContext();
        mChecked = new SparseBooleanArray();

        initalizeActivity();
        bindControls();
    }

    private void initalizeActivity() {
        setContentView(R.layout.activity_discipline_filter);
        ButterKnife.bind(this);
        ivmenu.setVisibility(View.GONE);
    }

    private void bindControls() {
        if(_extra.getSerializable("FILTER") != null)
        {
            if(_extra.getBoolean("IS_TRACK_FACILITY"))
            {
                _arrayOfDiscipline = (FilterSerializableForTrackFacilityClass) _extra.getSerializable("FILTER");
            }
            else
            {
                _arrayOfEvents = (FilterSerializableForRaceEventsClass) _extra.getSerializable("FILTER");
            }
        }
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("", ""));
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithGET(TypeFactory.RequestType.DISCIPLINE, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(list);

        }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        _mainApplication.dialogManager.dismissProgressDialog();
        if(_object != null)
        {
            DisciplineResponse _response = (DisciplineResponse) _object[0];
            if(_response != null)
            {
                if(_response.getStatus() == 1)
                {
                    if(_response.getData()!= null && _response.getData().size() >0)
                    {
                        _disciplineitem  = _response.getData();
                        if(_arrayOfEvents != null)
                        {
                            if(_arrayOfEvents.getDiscipline() != null && _arrayOfEvents.getDiscipline().size() >0)
                            {
                                for (int i = 0 ; i < _disciplineitem.size() ; i++)
                                {
                                    if(_arrayOfEvents.getDiscipline().contains(_disciplineitem.get(i).getDisciplineName()))
                                    {
                                        mChecked.put(i,true);
                                    }
                                    else
                                    {
                                        mChecked.put(i,false);
                                    }
                                }
                            }
                        }
                        else
                        {
                            if(_arrayOfDiscipline != null)
                            {
                                if(_arrayOfDiscipline.getDiscipline() != null && _arrayOfDiscipline.getDiscipline().size() >0)
                                {
                                    for (int i = 0 ; i < _disciplineitem.size() ; i++)
                                    {
                                        if(_arrayOfDiscipline.getDiscipline().contains(_disciplineitem.get(i).getDisciplineName()))
                                        {
                                            mChecked.put(i,true);
                                        }
                                        else
                                        {
                                            mChecked.put(i,false);
                                        }
                                    }
                                }
                            }
                        }
                        equipmentAdapter = new EquipmentDisciplineAdapter(this,_disciplineitem, this,mChecked,1);
                        list.setAdapter(equipmentAdapter);
                    }
                }
                else
                {
                    //_mainApplication.messageManager.DisplayToastMessage(_response.get());
                }
            }
        }

        else
        {
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    @Override
    public void getItemCheckedUnChecked(int position, boolean ischecked) {
        mChecked.put(position, ischecked);
        if (mChecked.size() == _disciplineitem.size()) {
            if (isAllValuesChecked()) {
                cball.setChecked(true);
                cbnone.setChecked(false);
            } else if (isAllValuesUnChecked()) {

                cball.setChecked(false);
                cbnone.setChecked(false);
            } else {
                cball.setChecked(false);
                cbnone.setChecked(true);
            }
        } else {
            cball.setChecked(false);
            cbnone.setChecked(false);
        }
    }

    @Override
    public boolean getItemCheckedUnCheckedByPosition(int position) {

        if (mChecked != null && mChecked.size() > 0) {
            ArrayList<String> _allCheckedIDs = new ArrayList<>();
            for (int i = 0; i < mChecked.size(); i++) {
                if (mChecked.get(i)) {
                    _allCheckedIDs.add(_disciplineitem.get(i).getDisciplineName());
                    //Log.i("Anish", _disciplineitem.get(i).getDisciplineName());
                }
            }
            if(_arrayOfEvents != null)
            {
                _arrayOfEvents.setDiscipline(_allCheckedIDs);
            }

            if(_arrayOfDiscipline != null)
            {
                _arrayOfDiscipline.setDiscipline(_allCheckedIDs);
            }
        }

        return mChecked.get(position);
    }

    protected boolean isAllValuesChecked() {

        for (int i = 0; i < mChecked.size(); i++) {
            if (!mChecked.get(i)) {
                return false;
            }
        }

        return true;
    }

    protected boolean isAllValuesUnChecked() {

        for (int i = 0; i < mChecked.size(); i++) {
            if (mChecked.get(i)) {
                return true;
            }
        }

        return false;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        if (intent.getExtras().getSerializable("FILTER") != null)
        {
            _arrayOfEvents = (FilterSerializableForRaceEventsClass) intent.getExtras().getSerializable("FILTER");
        }
    }

    @Override
    public boolean showChekBox() {
        return false;
    }

    @OnClick({R.id.ivback, R.id.cball, R.id.cbnone, R.id.btnsave})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivback:
                finish();
                break;
            case R.id.cball:
                cbnone.setChecked(false);
                cball.setChecked(true);
                cball.setBackgroundResource(R.drawable.allcheck);
                cbnone.setBackgroundResource(R.drawable.noneuncheck);

                for (int i = 0; i < _disciplineitem.size(); i++) {
                    mChecked.put(i, true);
                }
                equipmentAdapter.notifyDataSetChanged();
                break;

            case R.id.cbnone:
                cbnone.setChecked(true);
                cball.setChecked(false);

                cball.setBackgroundResource(R.drawable.alluncheck);
                cbnone.setBackgroundResource(R.drawable.nonecheck);

                for (int i = 0; i < _disciplineitem.size(); i++) {
                    mChecked.put(i, false);
                }
                equipmentAdapter.notifyDataSetChanged();
                break;

            case R.id.btnsave:
                if (mChecked != null && mChecked.size() > 0) {
                    ArrayList<String> _allCheckedName = new ArrayList<>();
                    for (int i = 0; i < mChecked.size(); i++) {
                        if (mChecked.get(i)) {
                            _allCheckedName.add(_disciplineitem.get(i).getDisciplineName()); // Containing Name
                            //Log.i("Anish", _disciplineitem.get(i).getDisciplineName());
                        }
                    }
                    if(_arrayOfEvents != null)
                    {
                        _arrayOfEvents.setDiscipline(_allCheckedName);
                    }
                    else
                    {
                        _arrayOfDiscipline.setDiscipline(_allCheckedName);
                    }

                }

                if(_arrayOfEvents != null)
                {
                    Intent _intent = new Intent(this, FilterRaceEventsActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                    _intent.putExtras(bundle);
                    startActivity(_intent);
                    finish();
                }
                else
                {
                    Intent _intent = new Intent(this, FilterTrackFacilityActivity.class);
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FILTER", (Serializable) _arrayOfDiscipline);
                    _intent.putExtras(bundle);
                    startActivity(_intent);
                    finish();
                }

                break;
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }
}
