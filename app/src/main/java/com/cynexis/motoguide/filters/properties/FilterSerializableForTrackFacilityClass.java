
package com.cynexis.motoguide.filters.properties;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FilterSerializableForTrackFacilityClass implements Serializable
{

    @SerializedName("disciplinename")
    @Expose
    private List<String> discipline = null;
    @SerializedName("miles")
    @Expose
    private String miles;
    @SerializedName("state")
    @Expose
    private String state;



    private final static long serialVersionUID = -6502305456075256539L;

    public List<String> getDiscipline() {
        return discipline;
    }

    public void setDiscipline(List<String> discipline) {
        this.discipline = discipline;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
