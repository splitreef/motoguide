
package com.cynexis.motoguide.filters.properties;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class FilterSerializableForRaceEventsClass implements Serializable {

    @SerializedName("disciplinename")
    @Expose
    private List<String> discipline = null;
    @SerializedName("equipmentname")
    @Expose
    private List<String> equipment = null;
    @SerializedName("sanctionname")
    @Expose
    private List<String> sanction = null;
    @SerializedName("start_date")
    @Expose
    private String startdate;
    @SerializedName("end_date")
    @Expose
    private String enddate;
    @SerializedName("miles")
    @Expose
    private String miles;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("factoryname")
    @Expose
    private List<String> factory = null;
    @SerializedName("type")
    @Expose
    private String type;

    private final static long serialVersionUID = -6502305456075256539L;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public List<String> getDiscipline() {
        return discipline;
    }

    public void setDiscipline(List<String> discipline) {
        this.discipline = discipline;
    }

    public List<String> getEquipment() {
        return equipment;
    }

    public void setEquipment(List<String> equipment) {
        this.equipment = equipment;
    }

    public List<String> getSanction() {
        return sanction;
    }

    public void setSanction(List<String> sanction) {
        this.sanction = sanction;
    }

    public List<String> getFactory() {
        return factory;
    }

    public void setFactory(List<String> factory) {
        this.factory = factory;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getMiles() {
        return miles;
    }

    public void setMiles(String miles) {
        this.miles = miles;
    }

    public String getEnddate() {
        return enddate;
    }

    public void setEnddate(String enddate) {
        this.enddate = enddate;
    }

    public String getStartdate() {
        return startdate;
    }

    public void setStartdate(String startdate) {
        this.startdate = startdate;
    }
}
