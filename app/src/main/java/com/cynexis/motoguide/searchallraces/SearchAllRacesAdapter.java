package com.cynexis.motoguide.searchallraces;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.network.response.nearbyracesresponse.NearbyRaceData;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Anish on 30/05/17.
 */

public class SearchAllRacesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context _ctx;
    List<NearbyRaceData> _raceseventitem;
    LayoutInflater inflaterone = null;
    private SearchAllRacesOnItemClickListener searchAllRacesOnItemClickListener;
    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    private boolean showLoader;

    public SearchAllRacesAdapter(Context _ctx, List<NearbyRaceData> _raceseventitem, SearchAllRacesOnItemClickListener searchAllRacesOnItemClickListener) {
        this._ctx = _ctx;
        this._raceseventitem = _raceseventitem;
        inflaterone = (LayoutInflater) _ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.searchAllRacesOnItemClickListener = searchAllRacesOnItemClickListener;
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof FooterLoader) {
            FooterLoader loaderViewHolder = (FooterLoader) viewHolder;
            if (showLoader) {
                loaderViewHolder.loadmore_progress.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.loadmore_progress.setVisibility(View.GONE);
            }
            return;
        }
        SearchAllRacesAdapter.ViewHolderOne holder = ((SearchAllRacesAdapter.ViewHolderOne) viewHolder);
        holder.rlfacilitycontainer.setVisibility(View.GONE);
        DecimalFormat df = new DecimalFormat("###.#");
        //Log.i("tanu",_raceseventitem.get(position).getDistance().replace("[^0-9.]", "").replace("[^a-zA-Z]",""));
        // holder.tvadress.setText(_raceseventitem.get(position).getCity()+", "+_raceseventitem.get(position).getState()+" - "+(df.format(Double.parseDouble(_raceseventitem.get(position).getDistance().replaceAll("[^a-zA-Z]", ""))))+" "+_raceseventitem.get(position).getDistance().replaceAll("[0-9]",""));
        holder.tvadress.setText(_raceseventitem.get(position).getCity() + ", " + _raceseventitem.get(position).getState() + " - " + _raceseventitem.get(position).getDistance());
        holder.tvracename.setText(_raceseventitem.get(position).getEventName());
        if (!_raceseventitem.get(position).getTrackFacility().equals("No Facility")) {
            holder.tvfacility.setText(_raceseventitem.get(position).getTrackFacility());
            holder.tvfacility.setVisibility(View.VISIBLE);
        } else {
            holder.tvfacility.setText("");
            holder.tvfacility.setVisibility(View.GONE);
        }
        holder.tvenddate.setText("to " + CommonUtility.convertDateFormat(_raceseventitem.get(position).getEndDate(),
         //       "yyyy-MM-dd", "dd/yy"));
          //      "yyyy-MM-dd", "MM/dd"));
        "yyyy-MM-dd", "dd/MM"));
        holder.tvcalendardate.setText(CommonUtility.convertDateFormat(_raceseventitem.get(position).getStartDate(), "yyyy-MM-dd", "MMM") + "\n" + CommonUtility.convertDateFormat(_raceseventitem.get(position).getStartDate(), "yyyy-MM-dd", "d"));
        if (position == 0) {
            holder.rlroot.setBackgroundResource(R.color.white);
        }
        if (position == 1) {
            holder.rlroot.setBackgroundResource(R.color.listcolor);

        }

        if (position > 1) {
            if (position % 2 == 0) {
                holder.rlroot.setBackgroundResource(R.color.white);
            } else {
                holder.rlroot.setBackgroundResource(R.color.listcolor);

            }
        }

        if (_raceseventitem.get(position).getType().equalsIgnoreCase(_ctx.getResources().getString(R.string.practiceday))) {
            holder.ivflag.setImageResource(R.mipmap.listorganized);
        } else {
            holder.ivflag.setImageResource(R.mipmap.race_adapter_flag);
        }

        holder.rlroot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchAllRacesOnItemClickListener.searchAllRacesItemClick(position,_raceseventitem.get(position).getType());

            }
        });
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_search_all_raceslist, parent, false);
                return new ViewHolderOne(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
                return new FooterLoader(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == _raceseventitem.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }


    @Override
    public int getItemCount() {
        return _raceseventitem.size();
    }

    static class ViewHolderOne extends RecyclerView.ViewHolder {
        @BindView(R.id.tvcalendardate)
        public TextView tvcalendardate;
        @BindView(R.id.tvadress)
        public TextView tvadress;
        @BindView(R.id.tvtype)
        public TextView tvtype;
        @BindView(R.id.tvfacility)
        public TextView tvfacility;
        @BindView(R.id.tvracename)
        public TextView tvracename;
        @BindView(R.id.tvenddate)
        public TextView tvenddate;
        @BindView(R.id.rlroot)
        public LinearLayout rlroot;
        @BindView(R.id.rlfacilitycontainer)
        public RelativeLayout rlfacilitycontainer;
        @BindView(R.id.ivflag)
        public ImageView ivflag;


        public ViewHolderOne(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected class FooterLoader extends RecyclerView.ViewHolder {
        @BindView(R.id.loadmore_progress)
        public ProgressBar loadmore_progress;

        public FooterLoader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
