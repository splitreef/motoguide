package com.cynexis.motoguide.searchallraces;

/**
 * Created by  on 06-01-2020.
 */
public interface SearchOnItemClickListener {
    public void onSearchItemClick(int position, String searchType,String eventID);
}
