package com.cynexis.motoguide.searchallraces;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.DashBoardActivity;
import com.cynexis.motoguide.FacilityEventDetailActivity;
import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.RaceEventDetailActivity;
import com.cynexis.motoguide.Utility.CommonSpinnerAdapter;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.Utility.EndlessRecyclerViewScrollListener;
import com.cynexis.motoguide.dateManager.DynamicControlsForDate;
import com.cynexis.motoguide.dateManager.IDateConsumer;
import com.cynexis.motoguide.filters.FilterRaceEventsActivity;
import com.cynexis.motoguide.filters.properties.FilterSerializableForRaceEventsClass;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.mycalendar.MyCalendarActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.nearbyracesresponse.NearbyEventResponse;
import com.cynexis.motoguide.network.response.nearbyracesresponse.NearbyRaceData;
import com.cynexis.motoguide.startingactivities.permission.GetLatitudeAndLongitude;
import com.cynexis.motoguide.startingactivities.permission.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class SearchAllRacesActivity extends FragmentActivity implements OnMapReadyCallback, IDateConsumer,
        GetLatitudeAndLongitude, WebserviceStartEndEventTracker, GoogleMap.OnMarkerClickListener, SearchAllRacesOnItemClickListener {


    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.spinnermiles)
    AppCompatSpinner spinnermiles;
    @BindView(R.id.calendarone)
    ImageView calendarone;
    @BindView(R.id.rlstartdatecontainer)
    RelativeLayout rlstartdatecontainer;
    @BindView(R.id.calendarenddate)
    ImageView calendarenddate;
    @BindView(R.id.tvenddate)
    TextView tvenddate;
    @BindView(R.id.tvstartdate)
    TextView tvstartdate;
    @BindView(R.id.rlenddatecontainer)
    RelativeLayout rlenddatecontainer;
    @BindView(R.id.btnmorefilter)
    Button btnmorefilter;

/*    @BindView(android.R.id.list)
    ListView list;*/

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;

    @BindView(R.id.ivscrollupscrolldown)
    ImageView ivscrollupscrolldown;
    @BindView(R.id.dragView)
    RelativeLayout dragView;
    @BindView(R.id.sliding_layout)
    SlidingUpPanelLayout slidingLayout;
    @BindView(R.id.bottomnavigation)
    BottomNavigationView bottomnavigation;
    FilterSerializableForRaceEventsClass _arrayOfEvents;
    Bundle _extra;

    private GoogleMap mMap;
    DynamicControlsForDate _dateControl;
    SearchAllRacesAdapter searchAllRacesAdapter;
    Intent _intent;
    String _latitude = "", _longitude = "";
    LocationService _locationService;
    public static final int PERMISSION_REQUEST_CODE = 120;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    MainApplication _mainApplication;
    List<NearbyRaceData> _raceEventsList;
    SlideMenueImplimentationClass _slidingMenu;
    SupportMapFragment mapFragment;
    private LinearLayoutManager linearLayoutManager;
    private int offset = 0;
    private Boolean isApiCalling = false;
    private int dataSize = 0, totalDataCount = 0, myTotalCount = 0;
    @BindView(R.id.tv_swipe_up_for_more_info)
    TextView tv_swipe_up_for_more_info;
    @BindView(R.id.tv_show_all)
    TextView tv_show_all;
    private Boolean isShowAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        _mainApplication = (MainApplication) getApplicationContext();
        super.onCreate(savedInstanceState);
        initalizeActivity();
        checkPermissionForMarshmallow();
        bindControls();

    }

    private void initalizeActivity() {
        setContentView(R.layout.activity_search_all_races);
        ButterKnife.bind(this);
        //dragView.setVisibility(View.VISIBLE);
        ivmenu.setVisibility(View.VISIBLE);
        _arrayOfEvents = new FilterSerializableForRaceEventsClass();
        // Spinner array list with custom adapter...
        List<String> _arrayOfMiles = Arrays.asList(getResources().getStringArray(R.array.array_miles));
        CommonSpinnerAdapter _spinneradapter = new CommonSpinnerAdapter(this, android.R.id.text1, _arrayOfMiles);
        _spinneradapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnermiles.setAdapter(_spinneradapter);
        spinnermiles.setSelection(1);

        linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_view.getContext(),
                linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.item_divider));
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView

        _raceEventsList = new ArrayList<>();
        searchAllRacesAdapter = new SearchAllRacesAdapter(this, _raceEventsList, this);
        recycler_view.setAdapter(searchAllRacesAdapter);


        slidingLayout.setScrollableView(recycler_view);

        RecyclerView.OnScrollListener mScrollListener = new
                RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                        if (!isShowAll) {
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                if (!isApiCalling) {
                                    searchAllRacesAdapter.showLoading(true);
                                    searchAllRacesAdapter.notifyDataSetChanged();
                                    offset = offset + 10;
                                    callWebAPIs(offset, false);
                                }


                            }
                        }
                    }
                };
        recycler_view.addOnScrollListener(mScrollListener);


    }


    private void bindControls() {
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();

        // Sliding Up Down Listner
        slidingLayout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

                if (newState.equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    ivscrollupscrolldown.setImageResource(R.mipmap.sliderdown);
                } else {
                    ivscrollupscrolldown.setImageResource(R.mipmap.sliderup);
                }
            }
        });

        // Bottom bar navigation
        bottomnavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        // Write code to perform some actions.
                        switch (item.getItemId()) {
                            case R.id.action_search:
                                //Log.i("Anish","1");
                                _intent = new Intent(SearchAllRacesActivity.this, SearchActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_home:
                                //Log.i("Anish","2");
                                _intent = new Intent(SearchAllRacesActivity.this, DashBoardActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_calendar:
                                //Log.i("Anish","3");
                                _intent = new Intent(SearchAllRacesActivity.this, MyCalendarActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;
                        }

                        return false;
                    }
                });


        recycler_view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        break;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        v.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }

                // Handle ListView touch events.
                v.onTouchEvent(event);
                return true;
            }
        });


    }


    private void checkPermissionForMarshmallow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);
                }

            } else {
                if (checkGooglePlayServices()) {
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (_raceEventsList != null && _raceEventsList.size() > 0) {
            for (int i = 0; i < _raceEventsList.size(); i++) {
                if (_raceEventsList.get(i).getLatitude().trim().length() != 0) {
                    Marker _plottedMarkerObject = mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(_raceEventsList.get(i).getLatitude()), Double.valueOf(_raceEventsList.get(i).getLongitude()))).icon(BitmapDescriptorFactory.fromResource(R.mipmap.map_marker)));
                    _plottedMarkerObject.setTag(i);
                }
            }


            if (_latitude.trim().length() != 0 && !_latitude.trim().equals("0.0")) {
                LatLng myLocation = new LatLng(Double.valueOf(_latitude), Double.valueOf(_longitude));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10));
            } else {
                _mainApplication.messageManager.DisplayToastMessage("Current location not found.");
            }

        } else {
            if (_latitude.trim().length() != 0 && !_latitude.trim().equals("0.0")) {
                LatLng myLocation = new LatLng(Double.valueOf(_latitude), Double.valueOf(_longitude));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10));
            } else {
                _mainApplication.messageManager.DisplayToastMessage("Current location not found.");
            }
        }

        mMap.setOnMarkerClickListener(this);
    }

    @OnClick({R.id.rlstartdatecontainer, R.id.rlenddatecontainer, R.id.btnmorefilter, R.id.ivscrollupscrolldown, R.id.ivback, R.id.ivmenu, R.id.tv_show_all})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rlstartdatecontainer:
                _dateControl = new DynamicControlsForDate(this, tvstartdate, tvenddate, this);
                _dateControl.invokeDatePicker();
                break;
            case R.id.rlenddatecontainer:
                _dateControl = new DynamicControlsForDate(this, tvenddate, null, this);
                _dateControl.invokeDatePicker();
                break;
            case R.id.btnmorefilter:

                if (!isApiCalling) {
                    if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                        slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED); //to close
                    }
                    //MM-dd-yyyy
                    _intent = new Intent(this, FilterRaceEventsActivity.class);
                    _intent.putExtra("POSITION", spinnermiles.getSelectedItemPosition());
                    _arrayOfEvents.setEnddate(tvenddate.getText().toString());
                    _arrayOfEvents.setStartdate(tvstartdate.getText().toString());
                    _arrayOfEvents.setMiles(spinnermiles.getSelectedItem().toString());
                    _arrayOfEvents.setType("Event");
                    _intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                    Bundle bundle = new Bundle();
                    bundle.putSerializable("FILTER", (Serializable) _arrayOfEvents);
                    _intent.putExtras(bundle);
                    startActivity(_intent);
                }
                break;
            case R.id.ivscrollupscrolldown:
                if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED); //to close
                } else {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED); //to open
                }
                break;
            case R.id.ivback:
                if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                    slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                } else {
                    finish();
                }
                break;
            case R.id.ivmenu:
                _slidingMenu.ShowMenueOnActionBarClick();
                break;

            case R.id.tv_show_all:
                if (!isApiCalling) {
                    if (!isShowAll) {
                        isShowAll = true;
                        // tv_show_all.setBackgroundResource(R.drawable.bg_oval_red);
                        // tv_show_all.setAlpha(.5f);
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        _raceEventsList.clear();
                        apicallShowAllData(0);
                    } else {
                        isShowAll = false;
                        // tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
                        //tv_show_all.setAlpha(1f);
                        tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        offset = 0;
                        _raceEventsList.clear();
                        callWebAPIs(offset, true);
                    }
                }
                break;
        }
    }

    private void apicallShowAllData(int offset) {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            //isApiCalling = true;
            //if (progressFlag) {
            showProgressDialog();
            //}
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                _dataToPost.add(new DataEntity("distance", spinnermiles.getSelectedItem().toString().replace("Miles", "").trim()));

            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }

            if (tvstartdate.getText().equals("START")) {
                _dataToPost.add(new DataEntity("startdate", ""));
            } else {
                _dataToPost.add(new DataEntity("startdate", tvstartdate.getText().toString()));
            }

            if (tvenddate.getText().equals("END")) {
                _dataToPost.add(new DataEntity("enddate", ""));
            } else {
                _dataToPost.add(new DataEntity("enddate", tvenddate.getText().toString()));
            }

            _dataToPost.add(new DataEntity("type", "Event"));

            if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME) != null) {
                _dataToPost.add(new DataEntity("state", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME)));
            } else {
                _dataToPost.add(new DataEntity("state", ""));
            }


            _dataToPost.add(new DataEntity("disciplinename", ""));
            _dataToPost.add(new DataEntity("equipmentname", ""));
            _dataToPost.add(new DataEntity("sanctionname", ""));
            _dataToPost.add(new DataEntity("factoryname", ""));
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("show_all", "Yes"));

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.NEAR_EVENTRACE, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(recycler_view);
        }
    }

    @Override
    public void getDate(String startDate, String endDate) {

        if (endDate != null) {
            tvstartdate.setText(startDate);
            tvenddate.setText(endDate);
        }
        _raceEventsList.clear();
        offset = 0;
        dataSize = 0;
        totalDataCount = 0;
        myTotalCount = 0;
        isShowAll = false;
        //tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
        callWebAPIs(offset, true);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        isShowAll = false;
        // tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
        offset = 0;
        _arrayOfEvents = (FilterSerializableForRaceEventsClass) intent.getExtras().getSerializable("FILTER");
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            showProgressDialog();
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            if (_arrayOfEvents.getMiles() != null) {
                _dataToPost.add(new DataEntity("distance", _arrayOfEvents.getMiles().replace("Miles", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }
            if (_arrayOfEvents.getStartdate().equalsIgnoreCase("START")) {
                _dataToPost.add(new DataEntity("startdate", ""));
                tvstartdate.setText("START");

            } else {
                //_dataToPost.add(new DataEntity("startdate", _arrayOfEvents.getStartdate()));
                _dataToPost.add(new DataEntity("startdate",
                        (CommonUtility.convertDateFormat(_arrayOfEvents.getStartdate(), "MM-dd-yyyy", "yyyy-MM-dd"))));
                tvstartdate.setText(_arrayOfEvents.getStartdate());
            }
            if (_arrayOfEvents.getEnddate().equalsIgnoreCase("END")) {
                _dataToPost.add(new DataEntity("enddate", ""));
                tvenddate.setText("END");
            } else {
                //  _dataToPost.add(new DataEntity("enddate", _arrayOfEvents.getEnddate()));
                _dataToPost.add(new DataEntity("enddate",
                        (CommonUtility.convertDateFormat(_arrayOfEvents.getEnddate(), "MM-dd-yyyy", "yyyy-MM-dd"))));

                tvenddate.setText(_arrayOfEvents.getEnddate());
            }
            _dataToPost.add(new DataEntity("type", _arrayOfEvents.getType()));

            if (_arrayOfEvents.getState() == null || _arrayOfEvents.getState().trim().equals("STATE")) {
                _dataToPost.add(new DataEntity("state", ""));
            } else {
                _dataToPost.add(new DataEntity("state", _arrayOfEvents.getState().trim()));
            }

            if (_arrayOfEvents.getDiscipline() != null && _arrayOfEvents.getDiscipline().size() > 0) {
                _dataToPost.add(new DataEntity("disciplinename", _arrayOfEvents.getDiscipline().toString().trim().replace("[", "").replace("]", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("disciplinename", ""));
            }

            if (_arrayOfEvents.getEquipment() != null && _arrayOfEvents.getEquipment().size() > 0) {
                _dataToPost.add(new DataEntity("equipmentname", _arrayOfEvents.getEquipment().toString().trim().replace("[", "").replace("]", "").trim()));
            } else {
                _dataToPost.add(new DataEntity("equipmentname", ""));
            }

            if (_arrayOfEvents.getType().equals("Event")) {
                if (_arrayOfEvents.getSanction() != null && _arrayOfEvents.getSanction().size() > 0) {
                    _dataToPost.add(new DataEntity("sanctionname", _arrayOfEvents.getSanction().toString().trim().replace("[", "").replace("]", "").trim()));
                } else {
                    _dataToPost.add(new DataEntity("sanctionname", ""));
                }

                if (_arrayOfEvents.getFactory() != null && _arrayOfEvents.getFactory().size() > 0) {
                    _dataToPost.add(new DataEntity("factoryname", _arrayOfEvents.getFactory().toString().trim().replace("[", "").replace("]", "").trim()));
                } else {
                    _dataToPost.add(new DataEntity("factoryname", ""));
                }
            } else {
                _dataToPost.add(new DataEntity("sanctionname", ""));
                _dataToPost.add(new DataEntity("factoryname", ""));
            }

            if (mMap != null) {
                mMap.clear();
            }
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("show_all", "" + "No"));
            _raceEventsList.clear();
            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.NEAR_EVENTRACE, _dataToPost, this);
            _mainApplication.messageManager.DisplayToastMessageAtCenter("Filter have been applied");
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(ivback);
        }

    }

    @Override
    public void getLatLong(String locationBY, String latitude, String longitude) {
        if (latitude.equals("0.0")) {
            _latitude = "";
            _longitude = "";
        } else {
            _latitude = latitude;
            _longitude = longitude;
        }
        //Log.i("Tanvi", _latitude + " " + _longitude);
        _raceEventsList.clear();
        offset = 0;
        dataSize = 0;
        totalDataCount = 0;
        myTotalCount = 0;
        isShowAll = false;
        // tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
        callWebAPIs(offset, true);
    }

    @OnItemSelected(R.id.spinnermiles)
    public void spinnerItemSelected(Spinner spinner, int position) {
        //if (spinner.getId() == R.id.spinnermiles) {
        if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
            _raceEventsList.clear();
            offset = 0;
            dataSize = 0;
            totalDataCount = 0;
            myTotalCount = 0;
            isShowAll = false;
            //tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
            callWebAPIs(offset, true);
        }
        // }
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }


    @Override
    public void hideProgressDialog(Object... _object) {
        if (!isShowAll) {
            handalResponseWithPagination(_object);
        } else {
            handalResponseShowAll(_object);
        }
    }

    private void handalResponseWithPagination(Object[] _object) {
        isApiCalling = false;
        searchAllRacesAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.NEAR_EVENTRACE)) {
                NearbyEventResponse _response = (NearbyEventResponse) _object[0];
                if (_response != null) {
                    if (_response.getStatus() == 1) {
                        if (_response.getData() != null && _response.getData().size() > 0) {
                            dataSize = _response.getData().size();
                            if (dataSize >= 10) {
                                tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                            } else {
                                tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            }
                            myTotalCount = myTotalCount + dataSize;
                            //_raceEventsList.clear();
                            //  _raceEventsList = _response.getData();
                           /* searchAllRacesAdapter = new SearchAllRacesAdapter(this, _raceEventsList, this);
                            recycler_view.setAdapter(searchAllRacesAdapter);*/
                            _raceEventsList.addAll(_response.getData());
                            searchAllRacesAdapter.notifyDataSetChanged();
                            if (mapFragment == null) {
                                // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                                mapFragment.getMapAsync(SearchAllRacesActivity.this);
                            } else {
                                mapFragment.getMapAsync(SearchAllRacesActivity.this);
                            }

                        }

                    } else {
                        if (offset == 0) {
                            // if (_raceEventsList != null && _raceEventsList.size() > 0) {
                            if (_raceEventsList != null) {
                                tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                                _raceEventsList.clear();
                                dataSize = 0;
                                totalDataCount = 0;
                                searchAllRacesAdapter = new SearchAllRacesAdapter(this, _raceEventsList, this);
                                recycler_view.setAdapter(searchAllRacesAdapter);
                                searchAllRacesAdapter.notifyDataSetChanged();
                                if (mMap != null) {
                                    mMap.clear();
                                }
                            }

                            _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());
                        } else {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            searchAllRacesAdapter.showLoading(false);
                            searchAllRacesAdapter.notifyDataSetChanged();
                        }

                    }
                }
            } else {
                tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                //Log.i("tanvi", String.valueOf(_object[0]));
            }
        } else {
            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    private void handalResponseShowAll(Object[] _object) {
        //isApiCalling = false;
        searchAllRacesAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            if (AndroidFastNetworkingWebServiceCall.getInstance().get_requestType().equals(TypeFactory.RequestType.NEAR_EVENTRACE)) {
                NearbyEventResponse _response = (NearbyEventResponse) _object[0];
                if (_response != null) {
                    if (_response.getStatus() == 1) {
                        if (_response.getData() != null && _response.getData().size() > 0) {


                            _raceEventsList.addAll(_response.getData());
                            searchAllRacesAdapter.notifyDataSetChanged();
                            if (mapFragment == null) {
                                // Obtain the SupportMapFragment and get notified when the map is ready to be used.
                                mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                                mapFragment.getMapAsync(SearchAllRacesActivity.this);
                            } else {
                                mapFragment.getMapAsync(SearchAllRacesActivity.this);
                            }

                        }

                    } else {
                        //if (offset == 0) {
                        // if (_raceEventsList != null && _raceEventsList.size() > 0) {
                        if (_raceEventsList != null) {
                            //tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            _raceEventsList.clear();
                            dataSize = 0;
                            totalDataCount = 0;
                            searchAllRacesAdapter = new SearchAllRacesAdapter(this, _raceEventsList, this);
                            recycler_view.setAdapter(searchAllRacesAdapter);
                            searchAllRacesAdapter.notifyDataSetChanged();
                            if (mMap != null) {
                                mMap.clear();
                            }
                        }

                        _mainApplication.messageManager.DisplayToastMessage(_response.getMessage());


                    }
                }
            } else {
                //tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                //Log.i("tanvi", String.valueOf(_object[0]));
            }
        } else {
            //  tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (locationAccepted) {
                        Snackbar.make(recycler_view, "Permission Granted, Thanks.", Snackbar.LENGTH_LONG).show();
                        if (_locationService == null) {
                            _locationService = new LocationService(this, this);
                            _locationService.makeConnectionIfRequired();
                        }
                    } else {
                        Snackbar.make(recycler_view, "Permission Denied. Please allow and try loading this screen.", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    @Override
    public boolean onMarkerClick(final Marker marker) {
        //dragView.setVisibility(View.INVISIBLE);

        // Create the Snackbar
        final Snackbar snackbar = Snackbar.make(ivback, "", Snackbar.LENGTH_INDEFINITE);
        // Get the Snackbar's layout view

        Snackbar.SnackbarLayout layout = (Snackbar.SnackbarLayout) snackbar.getView();
        layout.setBackgroundResource(R.mipmap.transparent);
        layout.setPadding(0, 0, 0, 0);//set padding to 0
        // Hide the text
        // TextView textView = (TextView) layout.findViewById(android.support.design.R.id.snackbar_text);
        TextView textView = (TextView) layout.findViewById(R.id.snackbar_text);
        textView.setVisibility(View.INVISIBLE);
        LayoutInflater inflater = LayoutInflater.from(this);
        // Inflate our custom view
        View snackView = inflater.inflate(R.layout.snackbar_custom, null);
        // Configure the view
        ImageView ivclose = (ImageView) snackView.findViewById(R.id.ivclose);
        TextView tveventname = (TextView) snackView.findViewById(R.id.tveventname);
        TextView tvFacility = (TextView) snackView.findViewById(R.id.tvdiscipline);
        TextView tvcalendardate = (TextView) snackView.findViewById(R.id.tvcalendardate);
        TextView tvadress = (TextView) snackView.findViewById(R.id.tvadress);
        LinearLayout lldirection = (LinearLayout) snackView.findViewById(R.id.lldirection);
        TextView tv_direction = (TextView) snackView.findViewById(R.id.tv_direction);
        LinearLayout lladdressraceevent = (LinearLayout) snackView.findViewById(R.id.lladdressraceevent);
        LinearLayout lladdressfacility = (LinearLayout) snackView.findViewById(R.id.lladdressfacility);
        TextView tvadresss = (TextView) snackView.findViewById(R.id.tvadresss);
        if (lladdressfacility.getVisibility() == View.VISIBLE) {
            lladdressfacility.setVisibility(View.GONE);
        }
        if (_raceEventsList != null) {
            tveventname.setText(_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getEventName());
            //tvdiscipline.setText("Muddy Creek Raceway");
            if (!_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getTrackFacility().equals("No Facility")) {
                tvFacility.setText(_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getTrackFacility());
            } else {

            }

            tvcalendardate.setText(CommonUtility.convertDateFormat(_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getStartDate(), "yyyy-MM-dd", "MMM") + "\n" + CommonUtility.convertDateFormat(_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getStartDate(), "yyyy-MM-dd", "d"));
            tvadress.setText(_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getAddress() + " " + _raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getCity() + "\n" +
                    " " + _raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getState());
        }

        if (_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getTime().length() != 0) {
            tv_direction.setText(_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getTime());
        } else {
            tv_direction.setText("0h drive");
        }

        lldirection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (_raceEventsList != null && _raceEventsList.size() > 0) {
                    _intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?saddr=" + String.valueOf(_latitude) + "," + String.valueOf(_longitude) + "&daddr=" + _raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getLatitude() + "," + _raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getLongitude()));
                    startActivity(_intent);
                }
            }
        });

        tveventname.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _intent = new Intent(SearchAllRacesActivity.this, RaceEventDetailActivity.class);
                _intent.putExtra("LAT", _latitude);
                _intent.putExtra("LONG", _longitude);
                _intent.putExtra("TYPE", "Event");
                _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getID());
                startActivity(_intent);
            }
        });

        ivclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // dragView.setVisibility(View.VISIBLE);
                snackbar.dismiss();
            }
        });

        tvFacility.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!_raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getTrackFacility().equals("No Facility")) {
                    _intent = new Intent(SearchAllRacesActivity.this, FacilityEventDetailActivity.class);
                    _intent.putExtra("LAT", _latitude);
                    _intent.putExtra("LONG", _longitude);
                    _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _raceEventsList.get(Integer.valueOf(marker.getTag().toString())).getTrackFacilityID());
                    startActivity(_intent);
                }
            }
        });


        // Add the view to the Snackbar's layout
        layout.addView(snackView, 0);
        // Show the Snackbar
        snackbar.show();
        return false;
    }


    public void callWebAPIs(int offset, boolean progressFlag) {
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            isApiCalling = true;
            if (progressFlag) {
                showProgressDialog();
            }
            List<DataEntity> _dataToPost = new ArrayList<>();
            _dataToPost.add(new DataEntity("latitude", _latitude));
            _dataToPost.add(new DataEntity("longitude", _longitude));
            if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
                _dataToPost.add(new DataEntity("distance", spinnermiles.getSelectedItem().toString().replace("Miles", "").trim()));

            } else {
                _dataToPost.add(new DataEntity("distance", "500"));
            }

            if (tvstartdate.getText().equals("START")) {
                _dataToPost.add(new DataEntity("startdate", ""));
            } else {
                _dataToPost.add(new DataEntity("startdate", tvstartdate.getText().toString()));
            }

            if (tvenddate.getText().equals("END")) {
                _dataToPost.add(new DataEntity("enddate", ""));
            } else {
                _dataToPost.add(new DataEntity("enddate", tvenddate.getText().toString()));
            }

            _dataToPost.add(new DataEntity("type", "Event"));

            if (_mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME) != null) {
                _dataToPost.add(new DataEntity("state", _mainApplication.settingManager.getSetting(TypeFactory.SettingType.SELECTED_STATE_NAME)));
            } else {
                _dataToPost.add(new DataEntity("state", ""));
            }


            _dataToPost.add(new DataEntity("disciplinename", ""));
            _dataToPost.add(new DataEntity("equipmentname", ""));
            _dataToPost.add(new DataEntity("sanctionname", ""));
            _dataToPost.add(new DataEntity("factoryname", ""));
            _dataToPost.add(new DataEntity("offset", "" + offset));
            _dataToPost.add(new DataEntity("show_all", "" + "No"));

            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.NEAR_EVENTRACE, _dataToPost, this);
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(recycler_view);
        }
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public void searchAllRacesItemClick(int position, String type) {
        if (slidingLayout.getPanelState().equals(SlidingUpPanelLayout.PanelState.EXPANDED)) {
            slidingLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED); //to close
        }
        _intent = new Intent(SearchAllRacesActivity.this, RaceEventDetailActivity.class);
        _intent.putExtra("LAT", _latitude);
        _intent.putExtra("LONG", _longitude);
        _intent.putExtra("TYPE", type);
        _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, _raceEventsList.get(position).getID());
        startActivity(_intent);
        //finish();
    }
}
