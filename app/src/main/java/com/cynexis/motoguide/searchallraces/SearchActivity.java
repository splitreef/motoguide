package com.cynexis.motoguide.searchallraces;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.DashBoardActivity;
import com.cynexis.motoguide.FacilityEventDetailActivity;
import com.cynexis.motoguide.MainApplication;
import com.cynexis.motoguide.R;
import com.cynexis.motoguide.RaceEventDetailActivity;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.lib.SlideMenueImplimentationClass;
import com.cynexis.motoguide.mycalendar.MyCalendarActivity;
import com.cynexis.motoguide.network.AndroidFastNetworkingWebServiceCall;
import com.cynexis.motoguide.network.DataEntity;
import com.cynexis.motoguide.network.TypeFactory;
import com.cynexis.motoguide.network.WebserviceStartEndEventTracker;
import com.cynexis.motoguide.network.response.searchresponse.SearchData;
import com.cynexis.motoguide.network.response.searchresponse.SearchResponse;
import com.cynexis.motoguide.startingactivities.permission.GetLatitudeAndLongitude;
import com.cynexis.motoguide.startingactivities.permission.LocationService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends AppCompatActivity implements WebserviceStartEndEventTracker,// TextWatcher,
        GetLatitudeAndLongitude, SearchOnItemClickListener {

    @BindView(R.id.ivback)
    ImageButton ivback;
    @BindView(R.id.ivmenu)
    ImageButton ivmenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    SlideMenueImplimentationClass _slidingMenu;
    @BindView(R.id.etsearckeyword)
    EditText etsearckeyword;
   /* @BindView(android.R.id.list)
    ListView list;*/

    @BindView(R.id.recycler_view)
    RecyclerView recycler_view;
    private LinearLayoutManager linearLayoutManager;
    private int offset = 0;
    private Boolean isApiCalling = false;
    private int dataSize = 0;


    MainApplication _mainApplication;
    Intent _intent;
    List<SearchData> _searchitem;
    SearchAdapter searchAdapter;
    String _latitude = "", _longitude = "";
    LocationService _locationService;
    public static final int PERMISSION_REQUEST_CODE = 120;
    public static int REQUEST_CODE_RECOVER_PLAY_SERVICES = 200;
    Intent intent;
    @BindView(R.id.tvnorecordfound)
    TextView tvnorecordfound;
    @BindView(R.id.bottomnavigation)
    BottomNavigationView bottomnavigation;

    @BindView(R.id.tv_swipe_up_for_more_info)
    TextView tv_swipe_up_for_more_info;
    @BindView(R.id.tv_show_all)
    TextView tv_show_all;
    private Boolean isShowAll = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        initializeActivity();
        checkPermissionForMarshmallow();
        bindControls();
    }

    private void initializeActivity() {
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        //etsearckeyword.addTextChangedListener(this);

        linearLayoutManager = new LinearLayoutManager(this);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recycler_view.getContext(),
                linearLayoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getResources().getDrawable(R.drawable.item_divider));
        recycler_view.addItemDecoration(dividerItemDecoration);
        recycler_view.setLayoutManager(linearLayoutManager); // set LayoutManager to RecyclerView

        _searchitem = new ArrayList<>();
        searchAdapter = new SearchAdapter(this, _searchitem, this);
        recycler_view.setAdapter(searchAdapter);

        RecyclerView.OnScrollListener mScrollListener = new
                RecyclerView.OnScrollListener() {
                    @Override
                    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {

                        if (!isShowAll) {
                            int visibleItemCount = linearLayoutManager.getChildCount();
                            int totalItemCount = linearLayoutManager.getItemCount();
                            int pastVisibleItems = linearLayoutManager.findFirstVisibleItemPosition();
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                            if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                                if (!isApiCalling) {
                                    searchAdapter.showLoading(true);
                                    searchAdapter.notifyDataSetChanged();
                                    offset = offset + 10;
                                    callWebAPIs(offset, false);
                                }


                            }
                        }
                    }
                };
        recycler_view.addOnScrollListener(mScrollListener);


    }

    private void checkPermissionForMarshmallow() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if (checkGooglePlayServices()) {
                    _locationService = new LocationService(this, this);
                }

            } else {
                if (checkGooglePlayServices()) {
                    requestPermission();
                }
            }
        } else {
            _locationService = new LocationService(this, this);
        }
    }

    private boolean checkPermission() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSION_REQUEST_CODE);
    }

    private boolean checkGooglePlayServices() {

        int checkGooglePlayServices = GooglePlayServicesUtil
                .isGooglePlayServicesAvailable(this);
        if (checkGooglePlayServices != ConnectionResult.SUCCESS) {
            /*
             * google play services is missing or update is required
             *  return code could be
             * SUCCESS,
             * SERVICE_MISSING, SERVICE_VERSION_UPDATE_REQUIRED,
             * SERVICE_DISABLED, SERVICE_INVALID.
             */
            GooglePlayServicesUtil.getErrorDialog(checkGooglePlayServices,
                    this, REQUEST_CODE_RECOVER_PLAY_SERVICES).show();

            return false;
        }

        return true;

    }

    private void callWebAPIs(int offset, boolean flag) {
        isApiCalling = true;
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            if (checkForBlank()) {
                if (flag) {
                    showProgressDialog();
                }
                List<DataEntity> _dataToPost = new ArrayList<>();
                _dataToPost.add(new DataEntity("keyword", etsearckeyword.getText().toString()));
                _dataToPost.add(new DataEntity("latitude", _latitude));
                _dataToPost.add(new DataEntity("longitude", _longitude));
                _dataToPost.add(new DataEntity("offset", "" + offset));
                _dataToPost.add(new DataEntity("show_all", "" + "No"));

                AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.SEARCH, _dataToPost, SearchActivity.this);
            }
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(etsearckeyword);
        }
    }

    private void bindControls() {
        _slidingMenu = new SlideMenueImplimentationClass(this, true);
        _slidingMenu.AttachSlideMenueToActivity();
        etsearckeyword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                    if (_mainApplication.networkManager.CheckNetworkConnection()) {
                        if (checkForBlank()) {
                            isShowAll = false;
                            // tv_show_all.setBackgroundResource(R.drawable.bg_oval_yellow);
                            isApiCalling = true;
                            offset = 0;
                            if (_searchitem != null) {
                                _searchitem.clear();
                            }
                            showProgressDialog();
                            List<DataEntity> _dataToPost = new ArrayList<>();
                            _dataToPost.add(new DataEntity("keyword", etsearckeyword.getText().toString()));
                            _dataToPost.add(new DataEntity("latitude", _latitude));
                            _dataToPost.add(new DataEntity("longitude", _longitude));
                            _dataToPost.add(new DataEntity("offset", "" + offset));
                            _dataToPost.add(new DataEntity("show_all", "" + "No"));

                            AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.SEARCH, _dataToPost, SearchActivity.this);
                        }
                    } else {
                        _mainApplication.messageManager.noInternetConnectionMessage(etsearckeyword);
                    }
                    return true;
                }
                return false;
            }
        });


        bottomnavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        // Write code to perform some actions.

                        switch (item.getItemId()) {
                            case R.id.action_search:
                                Log.i("Anish", "1");
                                _intent = new Intent(SearchActivity.this, SearchActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_home:
                                Log.i("Anish", "2");
                                _intent = new Intent(SearchActivity.this, DashBoardActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;

                            case R.id.action_calendar:
                                Log.i("Anish", "3");
                                _intent = new Intent(SearchActivity.this, MyCalendarActivity.class);
                                _intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                                startActivity(_intent);
                                finish();
                                break;
                        }

                        return false;
                    }
                });

    }


    @OnClick({R.id.ivback, R.id.ivmenu, R.id.tv_show_all})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivback:
              /*  _intent = new Intent(SearchActivity.this,DashBoardActivity.class);
                startActivity(_intent);*/
                finish();
                break;
            case R.id.ivmenu:
                _slidingMenu.ShowMenueOnActionBarClick();
                break;

            case R.id.tv_show_all:
                if (!isApiCalling) {
                    if (!isShowAll) {
                        isShowAll = true;
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        if (_searchitem != null) {
                            _searchitem.clear();
                        }
                        searchAdapter.notifyDataSetChanged();
                        apicallShowAllData(0);
                    } else {
                        isShowAll = false;
                        tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        offset = 0;
                        if (_searchitem != null) {
                            _searchitem.clear();
                        }
                        searchAdapter.notifyDataSetChanged();
                        callWebAPIs(offset, true);
                    }
                }
                break;
        }
    }

    private void apicallShowAllData(int offset) {
        //isApiCalling = true;
        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            if (checkForBlank()) {
                showProgressDialog();
                List<DataEntity> _dataToPost = new ArrayList<>();
                _dataToPost.add(new DataEntity("keyword", etsearckeyword.getText().toString()));
                _dataToPost.add(new DataEntity("latitude", _latitude));
                _dataToPost.add(new DataEntity("longitude", _longitude));
                _dataToPost.add(new DataEntity("offset", "" + offset));
                _dataToPost.add(new DataEntity("show_all", "Yes"));

                AndroidFastNetworkingWebServiceCall.getInstance().callServiceWithPOST(TypeFactory.RequestType.SEARCH, _dataToPost, SearchActivity.this);
            }
        } else {
            _mainApplication.messageManager.noInternetConnectionMessage(etsearckeyword);
        }
    }

    public boolean checkForBlank() {
        if (etsearckeyword.getText().toString().length() == 0) {
            etsearckeyword.setEnabled(true);
            etsearckeyword.setError("Please enter search keyword.");
            etsearckeyword.requestFocus();
            return false;
        }

        return true;
    }

    @Override
    public void showProgressDialog() {
        _mainApplication.dialogManager.displayProgressDialog(this, "Loading...");
    }

    @Override
    public void hideProgressDialog(Object... _object) {
        if (!isShowAll) {
            handalResponseWithPagination(_object);
        } else {
            handalResponseShowAll(_object);
        }
    }

    private void handalResponseWithPagination(Object[] _object) {
        isApiCalling = false;
        searchAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            SearchResponse _response = (SearchResponse) _object[0];
            if (_response != null) {
                if (_response.getData() != null) {
                    Log.d("_response", _response.getData().toString());
                }
                if (_response.getStatus() == 1) {

                    if (_response.getData() != null && _response.getData().size() > 0) {
                        dataSize = _response.getData().size();
                        //tv_show_all.setVisibility(View.VISIBLE);
                        if (dataSize >= 10) {
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        } else {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        }
                        recycler_view.setVisibility(View.VISIBLE);
                        tvnorecordfound.setVisibility(View.GONE);

                        if (etsearckeyword.getText().toString().trim().length() != 0) {
                            //_searchitem = _response.getData();
                            _searchitem.addAll(_response.getData());
                            searchAdapter.notifyDataSetChanged();
                           /* searchAdapter = new SearchAdapter(this, _searchitem, this);
                            recycler_view.setAdapter(searchAdapter);*/
                        } else {
                            if (_searchitem != null) {
                                _searchitem.clear();
                                offset = 0;
                               /* searchAdapter = new SearchAdapter(this, _searchitem, this);
                                recycler_view.setAdapter(searchAdapter);*/
                                searchAdapter.notifyDataSetChanged();
                            }
                            // tv_show_all.setVisibility(View.GONE);
                            recycler_view.setVisibility(View.GONE);
                            tvnorecordfound.setVisibility(View.VISIBLE);
                            //_mainApplication.messageManager.DisplayToastMessageAtCenter("No record found.");
                        }

                    } else {
                        if (offset == 0) {
                            if (_searchitem != null) {
                                tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                                //tv_show_all.setVisibility(View.GONE);
                                recycler_view.setVisibility(View.GONE);
                                tvnorecordfound.setVisibility(View.VISIBLE);
                                _searchitem.clear();
                                offset = 0;
                              /*  searchAdapter = new SearchAdapter(this, _searchitem, this);
                                recycler_view.setAdapter(searchAdapter);*/
                                searchAdapter.notifyDataSetChanged();
                            }
                        } else {
                            // tv_show_all.setVisibility(View.VISIBLE);
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            searchAdapter.showLoading(false);
                            searchAdapter.notifyDataSetChanged();
                        }
                    }
                } else {
                    if (offset == 0) {
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        //tv_show_all.setVisibility(View.GONE);
                        recycler_view.setVisibility(View.GONE);
                        tvnorecordfound.setVisibility(View.VISIBLE);
                        //_mainApplication.messageManager.DisplayToastMessageAtCenter(_response.getMessage());
                        if (_searchitem != null) {
                            _searchitem.clear();
                            offset = 0;
                           /* searchAdapter = new SearchAdapter(this, _searchitem, this);
                            recycler_view.setAdapter(searchAdapter);*/
                            searchAdapter.notifyDataSetChanged();
                        }
                    } else {
                        //tv_show_all.setVisibility(View.VISIBLE);
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        searchAdapter.showLoading(false);
                        searchAdapter.notifyDataSetChanged();
                    }
                }
            }
        } else {
            if (_searchitem != null) {
                _searchitem.clear();
                offset = 0;
                tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                //tv_show_all.setVisibility(View.GONE);
                showProgressDialog();

              /*  searchAdapter = new SearchAdapter(this, _searchitem, this);
                recycler_view.setAdapter(searchAdapter);*/
                searchAdapter.notifyDataSetChanged();
            }
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }

    private void handalResponseShowAll(Object[] _object) {
        // isApiCalling = false;
        // searchAdapter.showLoading(false);
        _mainApplication.dialogManager.dismissProgressDialog();
        if (_object != null) {
            SearchResponse _response = (SearchResponse) _object[0];
            if (_response != null) {
                if (_response.getData() != null) {
                    Log.d("_response", _response.getData().toString());
                }
                if (_response.getStatus() == 1) {

                    if (_response.getData() != null && _response.getData().size() > 0) {
                        /*dataSize = _response.getData().size();
                        if (dataSize >= 10) {
                            tv_swipe_up_for_more_info.setVisibility(View.VISIBLE);
                        }else {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        }*/
                        recycler_view.setVisibility(View.VISIBLE);
                        tvnorecordfound.setVisibility(View.GONE);

                        if (etsearckeyword.getText().toString().trim().length() != 0) {
                            //_searchitem = _response.getData();
                            _searchitem.addAll(_response.getData());
                            searchAdapter.notifyDataSetChanged();
                           /* searchAdapter = new SearchAdapter(this, _searchitem, this);
                            recycler_view.setAdapter(searchAdapter);*/
                        } else {
                            if (_searchitem != null) {
                                _searchitem.clear();
                                offset = 0;
                               /* searchAdapter = new SearchAdapter(this, _searchitem, this);
                                recycler_view.setAdapter(searchAdapter);*/
                                searchAdapter.notifyDataSetChanged();
                            }
                            recycler_view.setVisibility(View.GONE);
                            tvnorecordfound.setVisibility(View.VISIBLE);
                            //_mainApplication.messageManager.DisplayToastMessageAtCenter("No record found.");
                        }

                    } else {
                        // if (offset == 0) {
                        if (_searchitem != null) {
                            //tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);

                            recycler_view.setVisibility(View.GONE);
                            tvnorecordfound.setVisibility(View.VISIBLE);
                            _searchitem.clear();
                            offset = 0;
                              /*  searchAdapter = new SearchAdapter(this, _searchitem, this);
                                recycler_view.setAdapter(searchAdapter);*/
                            searchAdapter.notifyDataSetChanged();
                        }
                        /*} else {
                            tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                            searchAdapter.showLoading(false);
                            searchAdapter.notifyDataSetChanged();
                        }*/
                    }
                } else {
                    //if (offset == 0) {
                    //tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                    recycler_view.setVisibility(View.GONE);
                    tvnorecordfound.setVisibility(View.VISIBLE);
                    //_mainApplication.messageManager.DisplayToastMessageAtCenter(_response.getMessage());
                    if (_searchitem != null) {
                        _searchitem.clear();
                        offset = 0;
                           /* searchAdapter = new SearchAdapter(this, _searchitem, this);
                            recycler_view.setAdapter(searchAdapter);*/
                        searchAdapter.notifyDataSetChanged();
                    }
                   /* } else {
                        tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);
                        searchAdapter.showLoading(false);
                        searchAdapter.notifyDataSetChanged();
                    }*/
                }
            }
        } else {
            if (_searchitem != null) {
                _searchitem.clear();
                offset = 0;
                // tv_swipe_up_for_more_info.setVisibility(View.INVISIBLE);

                showProgressDialog();

              /*  searchAdapter = new SearchAdapter(this, _searchitem, this);
                recycler_view.setAdapter(searchAdapter);*/
                searchAdapter.notifyDataSetChanged();
            }
            _mainApplication.messageManager.DisplayToastMessage(CommonUtility.ERROR_OCCURED);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean locationAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;

                    if (locationAccepted) {
                        Snackbar.make(recycler_view, "Permission Granted, Thanks.", Snackbar.LENGTH_LONG).show();
                        if (_locationService == null) {
                            _locationService = new LocationService(this, this);
                            _locationService.makeConnectionIfRequired();
                        }
                    } else {
                        Snackbar.make(recycler_view, "Permission Denied. Please allow.", Snackbar.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }


    @Override
    public void getLatLong(String locationBY, String latitude, String longitude) {
        if (latitude.equals("0.0")) {
            _latitude = "";
            _longitude = "";
        } else {
            _latitude = latitude;
            _longitude = longitude;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    public Context getActivityContext() {
        return null;
    }


    @Override
    public void onSearchItemClick(int position, String searchType, String EVENTID) {
        //For Future when Api Contain facility also
        if (searchType.equalsIgnoreCase("Practiceday") || searchType.equalsIgnoreCase("Event")) {
            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, EVENTID);
            intent = new Intent(SearchActivity.this, RaceEventDetailActivity.class);
            intent.putExtra("LAT", _latitude);
            intent.putExtra("LONG", _longitude);
            intent.putExtra("TYPE", searchType);
            startActivity(intent);
        } else {
            _mainApplication.settingManager.setSetting(TypeFactory.SettingType.EVENTID, EVENTID);
            intent = new Intent(SearchActivity.this, FacilityEventDetailActivity.class);
            intent.putExtra("LAT", _latitude);
            intent.putExtra("LONG", _longitude);
            intent.putExtra("TYPE", searchType);
            startActivity(intent);
        }
    }
}
