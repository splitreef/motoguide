package com.cynexis.motoguide.searchallraces;

/**
 * Created by  on 02-01-2020.
 */
public interface SearchAllRacesOnItemClickListener {
    public void searchAllRacesItemClick (int position,String type);

}
