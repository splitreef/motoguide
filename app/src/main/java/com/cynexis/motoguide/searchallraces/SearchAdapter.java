package com.cynexis.motoguide.searchallraces;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cynexis.motoguide.R;
import com.cynexis.motoguide.Utility.CommonUtility;
import com.cynexis.motoguide.network.response.facilitydataresponse.Image;
import com.cynexis.motoguide.network.response.searchresponse.SearchData;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Admin on 01-06-2017.
 */

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context _ctx;
    List<SearchData> _searchlist;
    private LayoutInflater inflaterone = null;
    private SearchOnItemClickListener searchOnItemClickListener;
    private final int VIEWTYPE_ITEM = 1;
    private final int VIEWTYPE_LOADER = 2;
    private boolean showLoader;

    public SearchAdapter(Context _ctx, List<SearchData> _searchlist, SearchOnItemClickListener searchOnItemClickListener) {
        this._ctx = _ctx;
        this._searchlist = _searchlist;
        this.searchOnItemClickListener = searchOnItemClickListener;
        inflaterone = (LayoutInflater) _ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void showLoading(boolean status) {
        showLoader = status;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case VIEWTYPE_ITEM:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_searchlist, parent, false);
                return new SearchAdapter.MyViewHolder(view);

            case VIEWTYPE_LOADER:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false);
                return new SearchAdapter.FooterLoader(view);
        }
        return null;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == _searchlist.size() - 1) {
            return showLoader ? VIEWTYPE_LOADER : VIEWTYPE_ITEM;
        }
        return VIEWTYPE_ITEM;
    }

    /*@Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {*/
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof SearchAdapter.FooterLoader) {
            SearchAdapter.FooterLoader loaderViewHolder = (SearchAdapter.FooterLoader) viewHolder;
            if (showLoader) {
                loaderViewHolder.loadmore_progress.setVisibility(View.VISIBLE);
            } else {
                loaderViewHolder.loadmore_progress.setVisibility(View.GONE);
            }
            return;
        }
        SearchAdapter.MyViewHolder holder = ((SearchAdapter.MyViewHolder) viewHolder);

        if (_searchlist.get(position).getType().equals("Facility")) {
            if (holder.rleventcontainer.getVisibility() == View.VISIBLE) {
                holder.rleventcontainer.setVisibility(View.GONE);

            }
            holder.rlfacilitycontainer.setVisibility(View.VISIBLE);
            holder.ivright_watch.setVisibility(View.GONE);
            holder.ivright.setVisibility(View.VISIBLE);
            holder.llcalendar.setVisibility(View.GONE);
            holder.tveventname.setText(_searchlist.get(position).getEventName());
            holder.tvadresss.setText(_searchlist.get(position).getCity() + ", " + _searchlist.get(position).getState() + " - " + _searchlist.get(position).getDistance().trim());
            holder.tvsearchtype.setText(_searchlist.get(position).getType());
            holder.tvid.setText(_searchlist.get(position).getID());
        } else if (_searchlist.get(position).getType().equals("OpenRidingAreas")) {
            if (holder.rleventcontainer.getVisibility() == View.VISIBLE) {
                holder.rleventcontainer.setVisibility(View.GONE);

            }
            holder.rlfacilitycontainer.setVisibility(View.VISIBLE);
            holder.ivright_watch.setVisibility(View.VISIBLE);
            holder.ivright.setVisibility(View.GONE);

            holder.llcalendar.setVisibility(View.GONE);
            holder.tveventname.setText(_searchlist.get(position).getEventName());
            holder.tvadresss.setText(_searchlist.get(position).getCity() + ", " + _searchlist.get(position).getState() + " - " + _searchlist.get(position).getDistance().trim());
            holder.tvsearchtype.setText(_searchlist.get(position).getType());
            holder.tvid.setText(_searchlist.get(position).getID());
        } else {
            if (holder.rlfacilitycontainer.getVisibility() == View.VISIBLE) {
                holder.rlfacilitycontainer.setVisibility(View.GONE);
            }
            holder.rleventcontainer.setVisibility(View.VISIBLE);
            holder.llcalendar.setVisibility(View.VISIBLE);

            if (_searchlist.get(position).getType().equals("Event")) {
                holder.ivflag.setImageResource(R.mipmap.race_adapter_flag);
            } else {
                holder.ivflag.setImageResource(R.mipmap.listorganized);
            }
            holder.tvracename.setText(_searchlist.get(position).getEventName());
            holder.tvadress.setText(_searchlist.get(position).getCity() + ", " + _searchlist.get(position).getState() + " - " + _searchlist.get(position).getDistance());
            holder.tvtype.setText(_searchlist.get(position).getDisciplineType());
            holder.tvsearchtype.setText(_searchlist.get(position).getType());
            holder.tvid.setText(_searchlist.get(position).getID());
            holder.tvcalendardate.setText(CommonUtility.convertDateFormat(_searchlist.get(position).getStartDate(), "yyyy-MM-dd", "MMM") + "\n" + CommonUtility.convertDateFormat(_searchlist.get(position).getStartDate(), "yyyy-MM-dd", "d"));
            holder.tvenddate.setText("to " + CommonUtility.convertDateFormat(_searchlist.get(position).getEndDate(), "yyyy-MM-dd", "MM/dd"));
            if (_searchlist.get(position).getFacilityID().equalsIgnoreCase("No Facility")) {
                holder.tvfacility.setText("");
                holder.tvfacility.setVisibility(View.GONE);
            } else {
                holder.tvfacility.setText(_searchlist.get(position).getFacilityID());
                holder.tvfacility.setVisibility(View.VISIBLE);
            }
        }
        holder.rlroot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchOnItemClickListener.onSearchItemClick(position, _searchlist.get(position).getType(), _searchlist.get(position).getID());
            }
        });

    }

    @Override
    public int getItemCount() {
        return _searchlist.size() > 0 ? _searchlist.size() : 0;
    }


    static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvcalendardate)
        TextView tvcalendardate;
        @BindView(R.id.llcalendar)
        LinearLayout llcalendar;
        @BindView(R.id.tvenddate)
        TextView tvenddate;
        @BindView(R.id.tvracename)
        TextView tvracename;
        @BindView(R.id.tveventname)
        TextView tveventname;
        @BindView(R.id.tvadresss)
        TextView tvadresss;
        @BindView(R.id.tvadress)
        TextView tvadress;
        @BindView(R.id.tvtype)
        TextView tvtype;
        @BindView(R.id.tvfacility)
        TextView tvfacility;
        @BindView(R.id.tvid)
        public TextView tvid;
        @BindView(R.id.rleventcontainer)
        RelativeLayout rleventcontainer;
        @BindView(R.id.rlfacilitycontainer)
        RelativeLayout rlfacilitycontainer;
        @BindView(R.id.tvsearchtype)
        public TextView tvsearchtype;
        @BindView(R.id.ivflag)
        ImageView ivflag;
        @BindView(R.id.rlroot)
        LinearLayout rlroot;
        @BindView(R.id.ivright_watch)
        ImageView ivright_watch;
        @BindView(R.id.ivright)
        ImageView ivright;


        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    protected class FooterLoader extends RecyclerView.ViewHolder {
        @BindView(R.id.loadmore_progress)
        public ProgressBar loadmore_progress;

        public FooterLoader(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

        }
    }
}
