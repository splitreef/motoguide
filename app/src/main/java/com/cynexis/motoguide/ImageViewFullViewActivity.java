package com.cynexis.motoguide;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ImageViewFullViewActivity extends Activity {

    Bundle _extra;
    @BindView(R.id.ivfullview)
    PhotoView ivfullview;
    @BindView(R.id.loading_spinner)
    ProgressBar loadingSpinner;
    MainApplication _mainApplication;
    @BindView(R.id.ibcancel)
    ImageButton ibcancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        _mainApplication = (MainApplication) getApplicationContext();
        _extra = this.getIntent().getExtras();
        initializeActivity();
        bindControls();
    }

    private void initializeActivity() {
        setContentView(R.layout.activity_image_view_full_view);
        ButterKnife.bind(this);
        loadingSpinner.setVisibility(View.VISIBLE);
    }

    private void bindControls() {

        if (_mainApplication.networkManager.CheckNetworkConnection()) {
            Picasso.with(this)
                    .load(_extra.getString("IMAGE"))
                    .into(ivfullview, new Callback() {
                        @Override
                        public void onSuccess() {
                            loadingSpinner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {

                        }
                    });
        } else {
            loadingSpinner.setVisibility(View.GONE);
        }

    }

    @OnClick(R.id.ibcancel)
    public void onClick() {

        finish();
    }
}
